--==================================================================================================
-- Company     : NCSR Demokritos / CERN
-- Engineer    : Yiannis Kazas
-- Module Name : IT_Register_Readout_core - RTL
--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- Description : 
--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- Last Changes : 1-Nov-2023
--==================================================================================================

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.user_package.all;
use IEEE.std_logic_misc.all;
library UNISIM;
use UNISIM.VComponents.all;

--==================================================================================================
entity IT_Register_Readout_core is
    Port ( 
           Reset_i                    : in std_logic;
           Clk_160MHz_i               : in std_logic;
           Clk_i                      : in std_logic;
           ipb_clk_i                  : in std_logic;
           ctrl_aurora_rx_i           : in ctrl_aurora_type;
           ctrl_reg_readout_i         : in ctrl_register_readout_type;
           stat_RegRdout_from_ipbus_i : in stat_RegReadout_from_ipbus_type;
           stat_RegReadout_to_ipbus_o : out stat_RegReadout_to_ipbus_type;
--           user_clk_i                 : in rdout_signal_array_type;
           reg_frame_valid_i          : in rdout_signal_array_type;
           reg_frame_i                : in rdout_frame_array_type
           );
end IT_Register_Readout_core;
--==================================================================================================


architecture RTL of IT_Register_Readout_core is

--==================================================================================================
    -- Start of Component Declaration
--==================================================================================================
    -- IPbus fifo
    COMPONENT ipbus_fifo_32x4096
      PORT (
        clk : IN STD_LOGIC;
        srst : IN STD_LOGIC;
        din : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        wr_en : IN STD_LOGIC;
        rd_en : IN STD_LOGIC;
        dout : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        full : OUT STD_LOGIC;
        empty : OUT STD_LOGIC;
        almost_empty : OUT STD_LOGIC
      );
    END COMPONENT;
    
    -- Chip Buffer
    COMPONENT reg_rdout_fifo_64x1024
      PORT (
        rst          : IN STD_LOGIC;
        wr_clk       : IN STD_LOGIC;
        rd_clk       : IN STD_LOGIC;
        din          : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        wr_en        : IN STD_LOGIC;
        rd_en        : IN STD_LOGIC;
        dout         : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
        full         : OUT STD_LOGIC;
        empty        : OUT STD_LOGIC;
        almost_empty : OUT STD_LOGIC
      );
    END COMPONENT;
--==================================================================================================
    -- End of Component Declaration
--==================================================================================================


--==================================================================================================
    -- Start of Signal Declaration
--==================================================================================================
    signal reset_int : std_logic;
    signal Reg_RdBack_error : std_logic;
    
    type Register_readout_state_type  is (IDLE, READ_CHIP_BUFFER);
    signal Register_readout_state : Register_readout_state_type;
    
    -- signals for round-robin arbiter
    signal tx_req_en        : std_logic := '0';
    signal tx_request       : std_logic_vector((NUM_HYBRIDS*NUM_CHIPS)-1 downto 0):=(others=>'0');
    signal previous_request : std_logic_vector((NUM_HYBRIDS*NUM_CHIPS)-1 downto 0):=(others=>'0');
    signal select_granted   : std_logic_vector((NUM_HYBRIDS*NUM_CHIPS)-1 downto 0):=(others=>'0');
    signal isol_lsb         : std_logic_vector((NUM_HYBRIDS*NUM_CHIPS)-1 downto 0):=(others=>'0');
    signal mask_previous    : std_logic_vector((NUM_HYBRIDS*NUM_CHIPS)-1 downto 0):=(others=>'0');
    signal tx_granted       : std_logic_vector((NUM_HYBRIDS*NUM_CHIPS)-1 downto 0):=(others=>'0');
    
    signal word_cntr : natural range 0 to 1:=0;
    signal chip_id   : natural range 0 to ((NUM_HYBRIDS*NUM_CHIPS)-1);
    
    -- signals for chip buffers (2D arrays)
    signal chip_fifo_dout_matrix  : rdout_frame_array_type;
    signal chip_fifo_empty_array  : rdout_signal_array_type;
    signal chip_fifo_full_array   : rdout_signal_array_type;
    signal chip_fifo_aempty_array : rdout_signal_array_type;
    
    -- signals for chip buffers (1D arrays), one fifo for each chip 
    type std_vector64_array is array (0 to ((NUM_HYBRIDS*NUM_CHIPS)-1)) of std_logic_vector(63 downto 0);
    signal chip_fifo_dout         : std_vector64_array;
    signal chip_fifo_rd_en        : std_logic_vector((NUM_HYBRIDS*NUM_CHIPS)-1 downto 0);
    signal chip_fifo_wr_en        : std_logic_vector((NUM_HYBRIDS*NUM_CHIPS)-1 downto 0);
    signal chip_fifo_empty_flags  : std_logic_vector((NUM_HYBRIDS*NUM_CHIPS)-1 downto 0);
    signal chip_fifo_full_flags   : std_logic_vector((NUM_HYBRIDS*NUM_CHIPS)-1 downto 0);
    signal chip_fifo_almost_empty : std_logic_vector((NUM_HYBRIDS*NUM_CHIPS)-1 downto 0);
    
    signal chip_fifo_buf_data : std_logic_vector(63 downto 0);
    
    -- Auto Read Registers, 2 for each chip
    type std_vector32_array is array (0 to ((NUM_HYBRIDS*NUM_CHIPS)-1)) of std_logic_vector(31 downto 0);
    signal AutoRead_a : std_vector32_array;
    signal AutoRead_b : std_vector32_array;
    
    -- signals for IPBus fifo, one common fifo for all chips and modules
    signal ipbus_fifo_wr_en        : std_logic;
    signal ipbus_fifo_almost_empty : std_logic;
    signal ipbus_fifo_din          : std_logic_vector(31 downto 0);
    
    -- signals for the decoding of register read-back frames
    signal btf      : std_logic_vector(7 downto 0);
    signal status   : std_logic_vector(3 downto 0);
    signal addr_1st : std_logic_vector(9 downto 0);
    signal data_1st : std_logic_vector(15 downto 0);
    signal addr_2nd : std_logic_vector( 9 downto 0);
    signal data_2nd : std_logic_vector(15 downto 0);
    
    signal chip_fifo_word1 : std_logic_vector(31 downto 0);
    signal chip_fifo_word2 : std_logic_vector(31 downto 0);
    
    -- additions for data merging
    signal fifo_wr_en        : rdout_signal_array_type;
    signal merged_data_valid : rdout_signal_array_type;
    
    -- Reset for FIFO
    signal fifo_rst      : std_logic;
    signal fifo_rst_cntr : integer:= 0;
    
--==================================================================================================
    -- End of Signal Declaration
--==================================================================================================



--==================================================================================================
    -- Start of Function Declaration
--==================================================================================================
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Function to convert one-hot to natural number --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    function one_hot_to_natural (One_Hot : std_logic_vector) 
      return natural is
      variable sel_chip    : natural range 0 to ((NUM_HYBRIDS*NUM_CHIPS)-1);  
      variable Bin_Vec_Var : std_logic_vector(4 downto 0);
    
    begin
    
      Bin_Vec_Var := (others => '0');
      
      for i in One_Hot'range loop
        if (One_Hot(i) = '1') then
          Bin_Vec_Var := Bin_Vec_Var or std_logic_vector(to_unsigned(i,Bin_Vec_Var'length));
          sel_chip    := to_integer(unsigned(Bin_Vec_Var));
        end if;
      end loop;
      return sel_chip;
    end function;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Function to convert array of std_logic_vector to std_logic_vector  --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    function array_to_vector(array_in : rdout_signal_array_type) 
      return std_logic_vector is
      variable vector_out : std_logic_vector((NUM_HYBRIDS * NUM_CHIPS)-1 downto 0);
    begin
      for h in 0 to NUM_HYBRIDS-1 loop
        for c in 0 to NUM_CHIPS-1 loop
            vector_out((h*NUM_CHIPS)+c):= array_in(h)(c);
        end loop;
      end loop;
      return vector_out;
    end function;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Function to convert 2D array of std_logic_vector  
        -- to 1D array of std_logic_vector  --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    function matrix_to_array(matrix_in : rdout_frame_array_type) 
      return std_vector64_array is
      variable array_out : std_vector64_array;
    begin
      for h in 0 to NUM_HYBRIDS-1 loop
        for c in 0 to NUM_CHIPS-1 loop
            array_out((h*NUM_CHIPS)+c):= matrix_in(h)(c);
          end loop;
      end loop;
      return array_out;
    end function;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--==================================================================================================
    -- End of Function Declaration
--==================================================================================================


    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Debug Core
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
COMPONENT Reg_Rdback_debug
    
    PORT (
        clk : IN STD_LOGIC;
        probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0); 
        probe3 : IN STD_LOGIC_VECTOR(3 DOWNTO 0); 
        probe4 : IN STD_LOGIC_VECTOR(3 DOWNTO 0); 
        probe5 : IN STD_LOGIC_VECTOR(3 DOWNTO 0); 
        probe6 : IN STD_LOGIC_VECTOR(3 DOWNTO 0); 
        probe7 : IN STD_LOGIC_VECTOR(3 DOWNTO 0); 
        probe8 : IN STD_LOGIC_VECTOR(3 DOWNTO 0); 
        probe9 : IN STD_LOGIC_VECTOR(3 DOWNTO 0); 
        probe10 : IN STD_LOGIC_VECTOR(63 DOWNTO 0); 
        probe11 : IN STD_LOGIC_VECTOR(63 DOWNTO 0); 
        probe12 : IN STD_LOGIC_VECTOR(3 DOWNTO 0); 
        probe13 : IN STD_LOGIC_VECTOR(31 DOWNTO 0); 
        probe14 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe15 : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        probe16 : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        probe17 : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        probe18 : IN STD_LOGIC_VECTOR(0 DOWNTO 0) 
    );
    END COMPONENT  ;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    signal state : std_logic_vector(7 downto 0);
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

begin 

    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
--Debug_RegRdBack : Reg_Rdback_debug
--    PORT MAP (
--        clk => clk_160MHz_i,
--        probe0(0)  => reg_frame_valid_i(0)(0), 
--        probe1(0)  => reg_frame_valid_i(0)(1), 
--        probe2     => state, 
--        probe3     => tx_request, 
--        probe4     => mask_previous, 
--        probe5     => select_granted, 
--        probe6     => isol_lsb, 
--        probe7     => tx_granted, 
--        probe8     => previous_request, 
--        probe9     => chip_fifo_full_flags, 
--        probe10    => reg_frame_i(0)(0), 
--        probe11    => reg_frame_i(0)(1), 
--        probe12    => chip_fifo_rd_en, 
--        probe13    => ipbus_fifo_din, 
--        probe14(0) => ipbus_fifo_wr_en, 
--        probe15    => chip_fifo_dout_matrix(0)(0),
--        probe16    => chip_fifo_dout_matrix(0)(1),
--        probe17    => chip_fifo_buf_data,
--        probe18(0) => tx_req_en
--    );
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- END Debug Core
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


--==================================================================================================
    -- Start of Main Body
--==================================================================================================

    reset_int <= reset_i or ctrl_reg_readout_i.ipb_fifo_rst;

    -- Reset signal for FIFOs -> has to be several cycles long -- 
    process(ipb_clk_i)
    begin
        if rising_edge(ipb_clk_i) then
            if (ctrl_reg_readout_i.ipb_fifo_rst = '1') then
                fifo_rst      <= '1';
                fifo_rst_cntr <= 0;
            elsif (fifo_rst_cntr < 10) then
                fifo_rst      <= '1';
                fifo_rst_cntr <= fifo_rst_cntr +1;
            else
                fifo_rst      <= '0';
                fifo_rst_cntr <= fifo_rst_cntr;
            end if;
        end if;
    end process;
                
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Function Conversions
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- Convert signals from 2D to 1D arrays
    chip_fifo_dout         <= matrix_to_array(chip_fifo_dout_matrix);
    chip_fifo_empty_flags  <= array_to_vector(chip_fifo_empty_array); 
    chip_fifo_full_flags   <= array_to_vector(chip_fifo_full_array); 
    chip_fifo_almost_empty <= array_to_vector(chip_fifo_aempty_array); 
    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Register Frame Decoding --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- Decoding of register frames for selected chip, shuffled data from aurora_rx
    btf      <= chip_fifo_buf_data(63 downto 56);
    status   <= chip_fifo_buf_data(7 downto 4);
    addr_2nd <= chip_fifo_buf_data(25 downto 24) & chip_fifo_buf_data(39 downto 32); -- Address field of the 2nd word of the frame 
    data_2nd <= chip_fifo_buf_data(47 downto 44) & chip_fifo_buf_data(43 downto 40) & chip_fifo_buf_data(55 downto 52) & chip_fifo_buf_data(51 downto 48); -- Data field of the 2nd word of the frame
    addr_1st <= chip_fifo_buf_data(3 downto 0) & chip_fifo_buf_data(15 downto 10); -- Address field of the 1st word of the frame
    data_1st <= chip_fifo_buf_data(9 downto 8) & chip_fifo_buf_data(23 downto 22) & chip_fifo_buf_data(21 downto 18) & chip_fifo_buf_data(17 downto 16) & chip_fifo_buf_data(31 downto 30) & chip_fifo_buf_data(29 downto 26); --Data field of 1st word
    -- Resulting 32bit words from the 64bit frame
    chip_fifo_word1 <= std_logic_vector(to_unsigned(chip_id,6)) & addr_1st & data_1st;
    chip_fifo_word2 <= std_logic_vector(to_unsigned(chip_id,6)) & addr_2nd & data_2nd;
    
    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Round-Robin Arbiter to read data from chip-buffers and store them to IPbus-fifo
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- Round-robin signals
    tx_request     <= not chip_fifo_empty_flags when (tx_req_en = '1') else (others => '0');
    mask_previous  <= tx_request and not (std_logic_vector(unsigned(previous_request) - 1) or previous_request); -- Mask off previous winners
    select_granted <= mask_previous and std_logic_vector(unsigned(not(mask_previous)) + 1);                      -- Select new winner
    isol_lsb       <= tx_request and std_logic_vector(unsigned(not(tx_request)) + 1);                            -- Isolate least significant set bit.
    tx_granted     <= select_granted when (mask_previous /= ((NUM_HYBRIDS*NUM_CHIPS)-1 downto 0 => '0')) else isol_lsb; -- Grant access for transmition to one chip
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- Process controlling the data tx from chip buffers to IPbus fifo
    process (ipb_clk_i, reset_int)
    begin
        if (reset_int = '1') then
            previous_request   <= (others => '0');
            chip_fifo_rd_en    <= (others => '0');
            chip_fifo_buf_data <= (others => '0');
            Reg_RdBack_error   <= '0';
        elsif rising_edge(ipb_clk_i) then
            case Register_readout_state is
            
                -- IDLE state, check for available data in the fifos
                when IDLE =>
                state <= x"00";
                
                    word_cntr        <= 0;
                    ipbus_fifo_wr_en <= '0';
                    tx_req_en        <= '1';
                    chip_fifo_rd_en  <= (others=>'0');
                    previous_request <= previous_request;
                    -- if data available in the fifos, select chip to process and go to read chip buffer state
                    if (or_reduce(tx_granted) = '1')then
                        tx_req_en              <= '0';
                        previous_request       <= tx_granted;
                        chip_fifo_rd_en        <= tx_granted;
                        chip_id                <= one_hot_to_natural(tx_granted);
                        chip_fifo_buf_data     <= chip_fifo_dout(one_hot_to_natural(tx_granted));--chip_fifo_dout(chip_id);
                        Register_readout_state <= READ_CHIP_BUFFER;
                    end if;
                
                -- Read next word from chip-fifos and store it to IPbus fifo    
                when READ_CHIP_BUFFER =>
                    chip_fifo_rd_en  <= (others=>'0');
                    ipbus_fifo_wr_en <= '0';
                    tx_req_en        <= '0';
                    if (btf = x"00") then -- both words are command reads x"D2"
    
                        if (word_cntr = 0) then
                        state <= x"01";
                            ipbus_fifo_din   <= chip_fifo_word1;
                            ipbus_fifo_wr_en <= '1';
                            word_cntr        <= 1;
                        else
                        state <= x"02";
                            ipbus_fifo_din         <= chip_fifo_word2;
                            ipbus_fifo_wr_en       <= '1';
                            word_cntr              <= 0;
                            Register_readout_state <= IDLE;
                        end if;
                    else
    
                        Register_readout_state <= IDLE;
                        if (btf = x"01") then    -- 1st word cmd read, 2nd word auto read x"99
                        state <= x"03";
                            ipbus_fifo_din      <= chip_fifo_word1;
                            ipbus_fifo_wr_en    <= '1';
                            AutoRead_b(chip_id) <= chip_fifo_word2;
                        elsif (btf = x"02") then -- 1st word auto read, 2nd word cmd read x"55
                        state <= x"04";
                            ipbus_fifo_din      <= chip_fifo_word2;
                            ipbus_fifo_wr_en    <= '1';
                            AutoRead_a(chip_id) <= chip_fifo_word1;
                        elsif (btf = x"03") then -- both words auto read x"B4
                        state <= x"05";
                            ipbus_fifo_wr_en <= '0';
                            AutoRead_a(chip_id) <= chip_fifo_word1;
                            AutoRead_b(chip_id) <= chip_fifo_word2;
                        else
                        state <= x"06";
                            ipbus_fifo_wr_en <= '0';
                            Reg_RdBack_error <= '1';
                        end if;
                    end if;
                    
                when others => 
                    Register_readout_state <= IDLE;
                    Reg_RdBack_error       <= '1';
            end case;            
                    
        end if;
    end process;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Multiplex AutoRead Registers 
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    process(reset_int, ipb_clk_i)
    begin
        if (reset_int = '1') then
            stat_RegReadout_to_ipbus_o.autoread_reg_a <= (others=>'0');
            stat_RegReadout_to_ipbus_o.autoread_reg_b <= (others=>'0');
        elsif rising_edge(ipb_clk_i) then
            stat_RegReadout_to_ipbus_o.autoread_reg_a <= AutoRead_a(to_integer(unsigned(ctrl_reg_readout_i.autoread_addr_a)));
            stat_RegReadout_to_ipbus_o.autoread_reg_b <= AutoRead_b(to_integer(unsigned(ctrl_reg_readout_i.autoread_addr_b)));
        end if;
    end process;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- IPBus Fifo for Chip-Register Readout
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    IPbus_fifo : ipbus_fifo_32x4096
      PORT MAP (
        srst    => fifo_rst,--reset_int,
        clk     => ipb_clk_i,--clk_i,
--        rd_clk => ipb_clk_i,
        din    => ipbus_fifo_din,
        wr_en  => ipbus_fifo_wr_en,
        rd_en  => stat_RegRdout_from_ipbus_i.ipb_fifo_rd_en,
        dout   => stat_RegReadout_to_ipbus_o.ipb_fifo_dout,
        full   => stat_RegReadout_to_ipbus_o.ipb_fifo_full,
        empty  => stat_RegReadout_to_ipbus_o.ipb_fifo_empty,
        almost_empty => ipbus_fifo_almost_empty
      );
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Chip Buffers with Register Read-Back Data
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Module_buffer: for MODULE in 0 to NUM_HYBRIDS-1 generate
        Chip_buffer: for CHIP in 0 to NUM_CHIPS-1 generate
            merged_data_valid(MODULE)(CHIP) <= '1' when reg_frame_i(MODULE)(CHIP)(7 downto 6) = ctrl_aurora_rx_i.chip_id_assigned(CHIP) else '0';
            fifo_wr_en(MODULE)(CHIP)        <= reg_frame_valid_i(MODULE)(CHIP) when (ctrl_aurora_rx_i.data_merging_en = '0') else (merged_data_valid(MODULE)(CHIP) and reg_frame_valid_i(MODULE)(CHIP)); 
            Reg_buffer : reg_rdout_fifo_64x1024 -- reduced to 64x512
              PORT MAP (
                rst    => fifo_rst,
                wr_clk => clk_i,
                rd_clk => ipb_clk_i,
                din    => reg_frame_i(MODULE)(CHIP),
                wr_en  => fifo_wr_en(MODULE)(CHIP),
                rd_en  => chip_fifo_rd_en((MODULE*NUM_CHIPS)+CHIP),
                dout   => chip_fifo_dout_matrix(MODULE)(CHIP),
                full   => chip_fifo_full_array(MODULE)(CHIP),
                empty  => chip_fifo_empty_array(MODULE)(CHIP),
                almost_empty => chip_fifo_aempty_array(MODULE)(CHIP)
              );
        end generate;
    end generate;          
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

end RTL;
