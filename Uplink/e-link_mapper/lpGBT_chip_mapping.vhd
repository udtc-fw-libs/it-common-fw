--==================================================================================================
-- Company     : NCSR Demokritos / CERN
-- Engineer    : Yiannis Kazas
-- Module Name : lpGBT_chip_mapping - Behavioral
--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- Description : FE Modules/Chips are mapped to specific lpGBT links & groups 
--             : and lpGBT links are mapped to SFP connectors
--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- Last change : 5-Dec-2020
--==================================================================================================

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;
use work.user_package.all;
use work.lpgbt_map_package.all;

--==================================================================================================
entity lpGBT_chip_mapping is
    Port ( 
        Reset_i                  : in std_logic;
        Clk_ipbus_i              : in std_logic; 
        -- configuration from ipbus
        Ctrl_lpgbt_i             : in ctrl_gbt_type;
        Cnfg_lpgbt_i             : in cnfg_gbt_type;       
        Stat_lpgbt_o             : out stat_gbt_type;
        -- lpgbt-fpga uplink/downlink data
        lpgbt_rx_data_i          : in  lpgbtfpga_uplink_frame_array(0 to NUM_LPGBT_LINKS_TOTAL-1);   -- lpgbt-fpga uplink frame 
        lpgbt_rx_data32_o        : out lpgbtfpga_uplink_chipframe_array;                             -- lpgbt-fpga uplink frame (divided in 32-bit groups per module/chip)
        lpgbt_tx_data4_i         : in  lpgbtfpga_downlink_chipframe;                                 -- lpgbt-fpga downlink frame (divided in 4-bit groups per module)
        lpgbt_tx_data_o          : out lpgbtfpga_downlink_frame_array(0 to NUM_LPGBT_LINKS_TOTAL-1); --NUM_GBT_LINKS_MAX-1);  -- lpgbt-fpga downlink frame 
--        -- lpgbt-fpga slow control data
--        lpgbt_ic_data_upLink_o   : out lpgbtfpga_sc_reg_array(0 to get_num_gbt_links.l12 + get_num_gbt_links.l8);--NUM_GBT_LINKS_MAX-1);          -- lpgbt-fpga slow control uplink
--        lpgbt_ic_data_downLink_o : out lpgbtfpga_sc_reg_array(0 to get_num_gbt_links.l12 + get_num_gbt_links.l8);--NUM_GBT_LINKS_MAX-1);          -- lpgbt-fpga slow control downlink
        -- generic io
        fmc_l12_la_p             : inout std_logic_vector(33 downto 0);
        fmc_l12_la_n             : inout std_logic_vector(33 downto 0);
        fmc_l8_la_p              : inout std_logic_vector(33 downto 0);
        fmc_l8_la_n              : inout std_logic_vector(33 downto 0);
        -- SFP lines
        fmc_sfp_tx_p             : in  std_logic_vector(0 to NUM_LPGBT_LINKS_TOTAL - 1);
        fmc_sfp_tx_n             : in  std_logic_vector(0 to NUM_LPGBT_LINKS_TOTAL - 1);
        fmc_sfp_rx_p             : out std_logic_vector(0 to NUM_LPGBT_LINKS_TOTAL - 1);
        fmc_sfp_rx_n             : out std_logic_vector(0 to NUM_LPGBT_LINKS_TOTAL - 1);
        -- fmc mgt pins
        fmc_l12_dp_c2m_p         : out std_logic_vector(11 downto 0);
        fmc_l12_dp_c2m_n         : out std_logic_vector(11 downto 0);
        fmc_l12_dp_m2c_p         : in std_logic_vector(11 downto 0);
        fmc_l12_dp_m2c_n         : in std_logic_vector(11 downto 0);
        fmc_l8_dp_c2m_p          : out std_logic_vector( 7 downto 0);
        fmc_l8_dp_c2m_n          : out std_logic_vector( 7 downto 0);
        fmc_l8_dp_m2c_p          : in std_logic_vector( 7 downto 0);
        fmc_l8_dp_m2c_n          : in std_logic_vector( 7 downto 0)
    );
end lpGBT_chip_mapping;
--==================================================================================================


architecture Behavioral of lpGBT_chip_mapping is


--==================================================================================================
    -- Start of Signal Declaration --
--==================================================================================================

    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- to set the fmc configuration (rate, leds and so on)
    type fmc_pin_id_type is
    record
        polarity    : string(1 to 1); 
        id            : natural;
    end record;
    type fmc_pin_id_vector_type is array(natural range<>) of fmc_pin_id_type;
    constant fmc_pin_sfp_disable_vector  : fmc_pin_id_vector_type(0 to 7) := (0 => ("n",6), 1 => ("p",10), 2 => ("n",13), 3 => ("p",17), 4 => ("n",22), 5 => ("p",26), 6 => ("n",29), 7 => ("p",33));
    constant fmc_pin_rate_vector         : fmc_pin_id_vector_type(0 to 7) := (0 => ("n",4), 1 => ("p",8),  2 => ("n",11), 3 => ("p",15), 4 => ("n",20), 5 => ("p",24), 6 => ("n",27), 7 => ("p",31));

    -- Polarity settings
    signal tx_polarity_l8  : std_logic_vector(0 to NUM_GBT_LINKS.L8-1);
    signal rx_polarity_l8  : std_logic_vector(0 to NUM_GBT_LINKS.L8-1);
    signal tx_polarity_l12 : std_logic_vector(0 to NUM_GBT_LINKS.L12-1);
    signal rx_polarity_l12 : std_logic_vector(0 to NUM_GBT_LINKS.L12-1);
    
    signal UPLINK_MAPPING_DATA   : lpgbt_uplinkmap_array_type;
    signal DOWNLINK_MAPPING_DATA : lpgbt_downlinkmap_array_type;
    
    signal UPLINK_MAP   : lpgbt_uplinkmap_array_type;
    signal DOWNLINK_MAP : lpgbt_downlinkmap_array_type;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Function to auto- map module/chips to links and groups --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    function chip_to_lpgbt(hybrid_nb: in integer; chip_nb: in integer)
    return lpgbt_groupmap is
        variable result          : lpgbt_groupmap := (link_id => 0, group_id => 0);
        variable groups_per_link : integer range 0 to 7 := 1;
    begin
        if (SPLIT_MODULES = FALSE) then
            -- Set Nb of groups per link according to module type - one group for each chip
            -- all chips within a module must belong to the same link 
            if (NUM_CHIPS = 1) then    -- Single chip, 7 modules/chips per link
                groups_per_link := 7;
            elsif (NUM_CHIPS = 2) then -- Double module, 3 modules/6 chips per link
                groups_per_link := 3;  
            elsif (NUM_CHIPS = 4) then -- Quad module, 1 module/4 chips per link
                groups_per_link := 1;
            end if;
        else
            -- modules can be split to different links
            groups_per_link := 7;
        end if;
        -- Calculate how many links are needed to fit all the chips
        result.link_id  := 0;
        result.group_id := 0;
        for hybrid in 0 to hybrid_nb loop
            for chip in 0 to chip_nb loop
                if (( (hybrid*NUM_CHIPS + chip) rem groups_per_link) = 0) then
                    result.link_id := result.link_id + 1;
                end if;
            end loop;
        end loop;  
        result.group_id := (hybrid_nb*NUM_CHIPS + chip_nb) - result.link_id*groups_per_link;                  
        return result;
    end function chip_to_lpgbt;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
--==================================================================================================
    -- END of Signal Declaration --
--==================================================================================================


begin


--==================================================================================================
-- START OF MAIN BODY --
--==================================================================================================

    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Select Default Mapping-data depending on module type --    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    scc_map_gen: if (NUM_CHIPS = 1) generate
        UPLINK_MAPPING_DATA   <= SCC_UPLINK_MAP;
        DOWNLINK_MAPPING_DATA <= SCC_DOWNLINK_MAP;
    end generate;
    
    quad_map_gen: if (NUM_CHIPS = 4) generate
        UPLINK_MAPPING_DATA   <= QUAD_UPLINK_MAP;
        DOWNLINK_MAPPING_DATA <= QUAD_DOWNLINK_MAP;
    end generate;
    
    -- pass mapping info to status registers
    stat_lpgbt_o.downlink_map_info <= std_logic_vector(to_unsigned((DOWNLINK_MAP((cnfg_lpgbt_i.downlink_map_id),(cnfg_lpgbt_i.downgroup_map_id)).module_id),5));
    
    stat_lpgbt_o.uplink_id    <= std_logic_vector(to_unsigned((UPLINK_MAP((cnfg_lpgbt_i.prbs_module_addr),(cnfg_lpgbt_i.prbs_chip_addr)).link_id),3));
    stat_lpgbt_o.upgroup_id   <= std_logic_vector(to_unsigned((UPLINK_MAP((cnfg_lpgbt_i.prbs_module_addr),(cnfg_lpgbt_i.prbs_chip_addr)).group_id),3));    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Map modules/chips to lpgbt links and groups --    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- Auto-map modules/chips to link-groups (sequential)--
    Auto_map_lpgbt: if (CUSTOM_MAPPING = FALSE) generate
        hybrid_gen: for h in 0 to NUM_HYBRIDS-1 generate
            chip_gen: for c in 0 to NUM_CHIPS-1 generate
                -- downlink data --> array of 4-bit frames per hybrid to array of 32-bit frames per link
                lpgbt_tx_data_o(chip_to_lpgbt(h,0).link_id) ((chip_to_lpgbt(h,0).group_id)*4 +3 downto (chip_to_lpgbt(h,0).group_id*4)) <= lpgbt_tx_data4_i(h);
                -- uplink data, -->  array of 224-bit frames per link to array of arrays of 32-bit frames per module/chip
                lpgbt_rx_data32_o(h)(c) <= lpgbt_rx_data_i(chip_to_lpgbt(h,c).link_id) ((chip_to_lpgbt(h,c).group_id)*32 +31 downto (chip_to_lpgbt(h,c).group_id*32));
            end generate;
        end generate;
    end generate;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- Custom mapping of modules/chips to link groups according to the provided constant
    Custom_map_lpgbt: if (CUSTOM_MAPPING = TRUE) generate
        -- Downlink mapping generation --> Iterate through links and groups 
        downlink_map_gen: for link in 0 to NUM_LPGBT_LINKS_TOTAL-1 generate
            downgroup_map_gen: for group_id in 0 to 7 generate
                lpgbt_tx_data_o(link)(group_id*4+3 downto group_id*4) <= lpgbt_tx_data4_i(DOWNLINK_MAP(link,group_id).module_id) when ((DOWNLINK_MAP(link,group_id).module_id) < NUM_HYBRIDS) else "0000";
            end generate downgroup_map_gen;
        end generate downlink_map_gen;
        
        -- Uplink mapping generation --> Iterate through hybrids and chips
        module_map_gen: for hybrid in 0 to NUM_HYBRIDS-1 generate
            chip_map_gen: for chip in 0 to NUM_CHIPS-1 generate
                lpgbt_rx_data32_o(hybrid)(chip) <= lpgbt_rx_data_i(UPLINK_MAP(hybrid,chip).link_id)((UPLINK_MAP(hybrid,chip).group_id)*32+31 downto (UPLINK_MAP(hybrid,chip).group_id*32));
            end generate chip_map_gen;
        end generate module_map_gen;
        
    end generate Custom_map_lpgbt;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    process(Reset_i,clk_ipbus_i)
    begin
        if (Reset_i = '1') then
            DOWNLINK_MAP <= downlink_mapping_data;
            UPLINK_MAP   <= uplink_mapping_data;
        elsif rising_edge(clk_ipbus_i) then
            if (ctrl_lpgbt_i.update_downlink_mapping = '1') then
                -- downlink mapping data
                DOWNLINK_MAP(cnfg_lpgbt_i.downlink_map_id,cnfg_lpgbt_i.downgroup_map_id).module_id <= cnfg_lpgbt_i.module_map_id; 
            end if;
            
            if (ctrl_lpgbt_i.update_uplink_mapping = '1') then
                -- uplink mapping data
                UPLINK_MAP(cnfg_lpgbt_i.module_map_id,cnfg_lpgbt_i.chip_map_id).link_id  <= cnfg_lpgbt_i.uplink_map_id;
                UPLINK_MAP(cnfg_lpgbt_i.module_map_id,cnfg_lpgbt_i.chip_map_id).group_id <= cnfg_lpgbt_i.upgroup_map_id;
            end if;
        end if;
    end process;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Map lpgbt optical links to SFP Transceivers --    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- FMC L8
    gen_l8: if FMC_L8 = FMC_OPTO generate
        -- note that l8 has swapped polarity - p to n        
        -- generate sfp connections
        multi: for link in 0 to NUM_GBT_LINKS.L8-1 generate
            -- tx
            fmc_l8_dp_c2m_n(link) <= fmc_sfp_tx_p(link);
            fmc_l8_dp_c2m_p(link) <= fmc_sfp_tx_n(link);
            -- rx
            fmc_sfp_rx_p(link) <= fmc_l8_dp_m2c_n(link);
            fmc_sfp_rx_n(link) <= fmc_l8_dp_m2c_p(link);
            -- settings sfp disable
            dis_p: if fmc_pin_sfp_disable_vector(link).polarity = "p" generate
                fmc_l8_la_p(fmc_pin_sfp_disable_vector(link).id) <= Cnfg_lpgbt_i.sfp_disable_l8(link);
            end generate;  
            dis_n: if fmc_pin_sfp_disable_vector(link).polarity = "n" generate
                fmc_l8_la_n(fmc_pin_sfp_disable_vector(link).id) <= Cnfg_lpgbt_i.sfp_disable_l8(link);
            end generate;     
            -- settings rate
            rate_p: if fmc_pin_rate_vector(link).polarity = "p" generate
                fmc_l8_la_p(fmc_pin_rate_vector(link).id) <= '0';
            end generate;  
            rate_n: if fmc_pin_rate_vector(link).polarity = "n" generate
                fmc_l8_la_n(fmc_pin_rate_vector(link).id) <= '0';
            end generate;     
            -- polarity
            tx_polarity_l8(link) <= Cnfg_lpgbt_i.tx_polarity_l8(link);
            rx_polarity_l8(link) <= Cnfg_lpgbt_i.rx_polarity_l8(link);
        end generate multi;   
        
    end generate gen_l8;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- FMC L12
    gen_l12: if (FMC_L12 = FMC_OPTO) generate
       -- generate sfp connections
        multi: for link in 0 to NUM_GBT_LINKS.L12-1 generate
            -- tx
            fmc_l12_dp_c2m_p(link) <= fmc_sfp_tx_p(NUM_GBT_LINKS.L8 + link);
            fmc_l12_dp_c2m_n(link) <= fmc_sfp_tx_n(NUM_GBT_LINKS.L8 + link);
            -- rx
            fmc_sfp_rx_p(NUM_GBT_LINKS.L8 + link) <= fmc_l12_dp_m2c_p(link);
            fmc_sfp_rx_n(NUM_GBT_LINKS.L8 + link) <= fmc_l12_dp_m2c_n(link);
            -- settings sfp disable
            dis_p: if fmc_pin_sfp_disable_vector(link).polarity = "p" generate
                fmc_l12_la_p(fmc_pin_sfp_disable_vector(link).id) <= Cnfg_lpgbt_i.sfp_disable_l12(link);
            end generate;  
            dis_n: if fmc_pin_sfp_disable_vector(link).polarity = "n" generate
                fmc_l12_la_n(fmc_pin_sfp_disable_vector(link).id) <= Cnfg_lpgbt_i.sfp_disable_l12(link);
            end generate;     
            -- settings rate
            rate_p: if fmc_pin_rate_vector(link).polarity = "p" generate
                fmc_l12_la_p(fmc_pin_rate_vector(link).id) <= '0';
            end generate;  
            rate_n: if fmc_pin_rate_vector(link).polarity = "n" generate
                fmc_l12_la_n(fmc_pin_rate_vector(link).id) <= '0';
            end generate;     
            -- polarity
            tx_polarity_l12(link) <= Cnfg_lpgbt_i.tx_polarity_l12(link);
            rx_polarity_l12(link) <= Cnfg_lpgbt_i.rx_polarity_l12(link);
        end generate multi;  
                        
    end generate gen_l12;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
              
--==================================================================================================
--==================================================================================================
end Behavioral;


