--==================================================================================================
-- Company     : NCSR Demokritos / CERN
-- Engineer    : Yiannis Kazas
-- Module Name : lpgbt_map_package
--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- Description : Package to map modules and chips to lpGBT links/groups
--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- Last change : 3-Dec-2020
--==================================================================================================

library ieee;
use ieee.std_logic_1164.all; 
use ieee.std_logic_misc.all;  
use IEEE.NUMERIC_STD.ALL;
use work.user_package.all;

--==================================================================================================
package lpgbt_map_package is

    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- Custom Mapping of Modules/chips to lpgbt uplinks/groups --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    constant MAX_NUM_HYBRIDS : natural := 48; -- for lpgbt portcard v0 = 8 links * 3 available groups per link --> to be revised!                                                                       
    type lpgbt_groupmap is
    record
        Link_id  : natural range 0 to NUM_LPGBT_LINKS_TOTAL;
        Group_id : natural range 0 to 7;
    end record;
    
    type lpgbt_downlink_map_type is
    record
        module_id : natural range 0 to 23;
    end record;
    
--    type lpgbt_downlinkmap_type is array (0 to 7) of natural;

    type lpgbt_downlinkmap_array_type is array (0 to NUM_GBT_LINKS_MAX-1,0 to 7) of lpgbt_downlink_map_type;
    type lpgbt_uplinkmap_array_type is array (0 to MAX_NUM_HYBRIDS-1,0 to 3) of lpgbt_groupmap;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- ************************ ASSIGN MODULES/CHIPS TO LPGBT LINKS/GROUPS ***********************
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- Only the Hybrids and Chips which are present in the system are taken into account,the rest are ignored. 
    -- (Number of Hybrids and Number of Chips are specified in the "IT_user_package_basic.vhd" file 
    
    --====================================================
    --============= Mapping for Quad Modules =============
    --====================================================
    constant QUAD_UPLINK_MAP : lpgbt_uplinkmap_array_type := (((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(0) !!--
                                                              ,(Link_id => 0, Group_id => 1) -- Chip(1)
                                                              ,(Link_id => 0, Group_id => 2) -- Chip(2)
                                                              ,(Link_id => 0, Group_id => 3) -- Chip(3)
                                                               )
                                                             ,((Link_id => 1, Group_id => 0) -- Chip(0)    --!! Hybrid(1) !!--
                                                              ,(Link_id => 1, Group_id => 1) -- Chip(1) 
                                                              ,(Link_id => 1, Group_id => 2) -- Chip(2) 
                                                              ,(Link_id => 1, Group_id => 3) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 2, Group_id => 0) -- Chip(0)    --!! Hybrid(2) !!--
                                                              ,(Link_id => 2, Group_id => 1) -- Chip(1) 
                                                              ,(Link_id => 2, Group_id => 2) -- Chip(2) 
                                                              ,(Link_id => 2, Group_id => 3) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 3, Group_id => 0) -- Chip(0)    --!! Hybrid(3) !!--
                                                              ,(Link_id => 3, Group_id => 1) -- Chip(1) 
                                                              ,(Link_id => 3, Group_id => 2) -- Chip(2) 
                                                              ,(Link_id => 3, Group_id => 3) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 4, Group_id => 0) -- Chip(0)    --!! Hybrid(4) !!--
                                                              ,(Link_id => 4, Group_id => 1) -- Chip(1) 
                                                              ,(Link_id => 4, Group_id => 2) -- Chip(2) 
                                                              ,(Link_id => 4, Group_id => 3) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 5, Group_id => 0) -- Chip(0)    --!! Hybrid(5) !!--
                                                              ,(Link_id => 5, Group_id => 1) -- Chip(1) 
                                                              ,(Link_id => 5, Group_id => 2) -- Chip(2) 
                                                              ,(Link_id => 5, Group_id => 3) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 6, Group_id => 0) -- Chip(0)    --!! Hybrid(6) !!--
                                                              ,(Link_id => 6, Group_id => 1) -- Chip(1) 
                                                              ,(Link_id => 6, Group_id => 2) -- Chip(2) 
                                                              ,(Link_id => 6, Group_id => 3) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 7, Group_id => 0) -- Chip(0)    --!! Hybrid(7) !!--
                                                              ,(Link_id => 7, Group_id => 1) -- Chip(1) 
                                                              ,(Link_id => 7, Group_id => 2) -- Chip(2) 
                                                              ,(Link_id => 7, Group_id => 3) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 8, Group_id => 0) -- Chip(0)    --!! Hybrid(8) !!--
                                                              ,(Link_id => 8, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 8, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 8, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 9, Group_id => 0) -- Chip(0)    --!! Hybrid(9) !!--
                                                              ,(Link_id => 9, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 9, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 9, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 10, Group_id => 0) -- Chip(0)    --!! Hybrid(10) !!--
                                                              ,(Link_id => 10, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 10, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 10, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 11, Group_id => 0) -- Chip(0)    --!! Hybrid(11) !!--
                                                              ,(Link_id => 11, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 11, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 11, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(12) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(13) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(14) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(15) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(16) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(17) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(18) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(19) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(20) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(21) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(22) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(23) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                               
                                                             ,((Link_id => 1, Group_id => 0) -- Chip(0)    --!! Hybrid(1) !!--
                                                                ,(Link_id => 1, Group_id => 1) -- Chip(1) 
                                                                ,(Link_id => 1, Group_id => 2) -- Chip(2) 
                                                                ,(Link_id => 1, Group_id => 3) -- Chip(3) 
                                                                 )
                                                               ,((Link_id => 2, Group_id => 0) -- Chip(0)    --!! Hybrid(2) !!--
                                                                ,(Link_id => 2, Group_id => 1) -- Chip(1) 
                                                                ,(Link_id => 2, Group_id => 2) -- Chip(2) 
                                                                ,(Link_id => 2, Group_id => 3) -- Chip(3) 
                                                                 )
                                                               ,((Link_id => 3, Group_id => 0) -- Chip(0)    --!! Hybrid(3) !!--
                                                                ,(Link_id => 3, Group_id => 1) -- Chip(1) 
                                                                ,(Link_id => 3, Group_id => 2) -- Chip(2) 
                                                                ,(Link_id => 3, Group_id => 3) -- Chip(3) 
                                                                 )
                                                               ,((Link_id => 4, Group_id => 0) -- Chip(0)    --!! Hybrid(4) !!--
                                                                ,(Link_id => 4, Group_id => 1) -- Chip(1) 
                                                                ,(Link_id => 4, Group_id => 2) -- Chip(2) 
                                                                ,(Link_id => 4, Group_id => 3) -- Chip(3) 
                                                                 )
                                                               ,((Link_id => 5, Group_id => 0) -- Chip(0)    --!! Hybrid(5) !!--
                                                                ,(Link_id => 5, Group_id => 1) -- Chip(1) 
                                                                ,(Link_id => 5, Group_id => 2) -- Chip(2) 
                                                                ,(Link_id => 5, Group_id => 3) -- Chip(3) 
                                                                 )
                                                               ,((Link_id => 6, Group_id => 0) -- Chip(0)    --!! Hybrid(6) !!--
                                                                ,(Link_id => 6, Group_id => 1) -- Chip(1) 
                                                                ,(Link_id => 6, Group_id => 2) -- Chip(2) 
                                                                ,(Link_id => 6, Group_id => 3) -- Chip(3) 
                                                                 )
                                                               ,((Link_id => 7, Group_id => 0) -- Chip(0)    --!! Hybrid(7) !!--
                                                                ,(Link_id => 7, Group_id => 1) -- Chip(1) 
                                                                ,(Link_id => 7, Group_id => 2) -- Chip(2) 
                                                                ,(Link_id => 7, Group_id => 3) -- Chip(3) 
                                                                 )
                                                               ,((Link_id => 8, Group_id => 0) -- Chip(0)    --!! Hybrid(8) !!--
                                                                ,(Link_id => 8, Group_id => 0) -- Chip(1) 
                                                                ,(Link_id => 8, Group_id => 0) -- Chip(2) 
                                                                ,(Link_id => 8, Group_id => 0) -- Chip(3) 
                                                                 )
                                                               ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(9) !!--
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                                 )
                                                               ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(10) !!--
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                                 )
                                                               ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(11) !!--
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                                 )
                                                               ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(12) !!--
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                                 )
                                                               ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(13) !!--
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                                 )
                                                               ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(14) !!--
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                                 )
                                                               ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(15) !!--
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                                 )
                                                               ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(16) !!--
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                                 )
                                                               ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(17) !!--
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                                 )
                                                               ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(18) !!--
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                                 )
                                                               ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(19) !!--
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                                 )
                                                               ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(20) !!--
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                                 )
                                                               ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(21) !!--
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                                 )
                                                               ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(22) !!--
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                                 )
                                                               ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(23) !!--
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                                ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                                 )
                                                               ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(23) !!--
                                                                  ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                                  ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                                  ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                                   )
                                                                 
                                                                 
                                                               );
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- Up to 1 Quad Module per Link
    -- Custom Mapping of Modules to lpgbt downlinks/groups --       Group_0         Group_1         Group_2         Group_3         Group_4         Group_5         Group_6         Group_7
    constant QUAD_DOWNLINK_MAP : lpgbt_downlinkmap_array_type := (((module_id=>0 ),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24))  --!! Link_0 !!--
                                                                 ,((module_id=>1 ),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24))  --!! Link_1 !!--
                                                                 ,((module_id=>2 ),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24))  --!! Link_2 !!--
                                                                 ,((module_id=>3 ),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24))  --!! Link_3 !!--
                                                                 ,((module_id=>4 ),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24))  --!! Link_4 !!--
                                                                 ,((module_id=>5 ),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24))  --!! Link_5 !!--
                                                                 ,((module_id=>6 ),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24))  --!! Link_6 !!--
                                                                 ,((module_id=>7 ),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24))  --!! Link_7 !!--
                                                                 ,((module_id=>8 ),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24))  --!! Link_8 !!--
                                                                 ,((module_id=>9 ),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24))  --!! Link_9 !!--
--                                                                 ,((module_id=>10 ),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24))  --!! Link_10 !!--
--                                                                 ,((module_id=>11 ),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24))  --!! Link_11 !!--
                                                                 );
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    --====================================================
    --============= Mapping for SCC Modules =============
    --====================================================
    -- Only Chip(0) from every hybrid is used, the rest are ignored 
    constant SCC_UPLINK_MAP : lpgbt_uplinkmap_array_type  := (((Link_id => 0, Group_id => 6) -- Chip(0)    --!! Hybrid(0) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1)
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2)
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3)
                                                               )
                                                             ,((Link_id => 0, Group_id => 5) -- Chip(0)    --!! Hybrid(1) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 0, Group_id => 6) -- Chip(0)    --!! Hybrid(2) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 1, Group_id => 4) -- Chip(0)    --!! Hybrid(3) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 1, Group_id => 5) -- Chip(0)    --!! Hybrid(4) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 1, Group_id => 6) -- Chip(0)    --!! Hybrid(5) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 2, Group_id => 4) -- Chip(0)    --!! Hybrid(6) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 2, Group_id => 5) -- Chip(0)    --!! Hybrid(7) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 2, Group_id => 6) -- Chip(0)    --!! Hybrid(8) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 3, Group_id => 4) -- Chip(0)    --!! Hybrid(9) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 3, Group_id => 5) -- Chip(0)    --!! Hybrid(10) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 3, Group_id => 6) -- Chip(0)    --!! Hybrid(11) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(12) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(13) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(14) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(15) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(16) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(17) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(18) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(19) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(20) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(21) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(22) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                             ,((Link_id => 0, Group_id => 0) -- Chip(0)    --!! Hybrid(23) !!--
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(1) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(2) 
                                                              ,(Link_id => 0, Group_id => 0) -- Chip(3) 
                                                               )
                                                               );
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- Up to 3 SCC per Link
    -- Custom Mapping of Modules to lpgbt downlinks/groups --       Group_0         Group_1         Group_2         Group_3         Group_4         Group_5         Group_6         Group_7
    constant SCC_DOWNLINK_MAP : lpgbt_downlinkmap_array_type  := (((module_id=>24),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>1 ),(module_id=>24),(module_id=>0 ),(module_id=>24))  --!! Link_0 !!--
                                                                 ,((module_id=>3 ),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>4 ),(module_id=>24),(module_id=>5 ),(module_id=>24))  --!! Link_1 !!--
                                                                 ,((module_id=>6 ),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>7 ),(module_id=>24),(module_id=>8 ),(module_id=>24))  --!! Link_2 !!--
                                                                 ,((module_id=>9 ),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>10),(module_id=>24),(module_id=>11),(module_id=>24))  --!! Link_3 !!--
                                                                 ,((module_id=>12),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>13),(module_id=>24),(module_id=>14),(module_id=>24))  --!! Link_4 !!--
                                                                 ,((module_id=>15),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>16),(module_id=>24),(module_id=>17),(module_id=>24))  --!! Link_5 !!--
                                                                 ,((module_id=>18),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>19),(module_id=>24),(module_id=>20),(module_id=>24))  --!! Link_6 !!--
                                                                 ,((module_id=>21),(module_id=>24),(module_id=>24),(module_id=>24),(module_id=>22),(module_id=>24),(module_id=>23),(module_id=>24))  --!! Link_7 !!--
                                                                 );
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
end lpgbt_map_package;
--==================================================================================================

package body lpgbt_map_package is

--==================================================================================================
--==================================================================================================
end lpgbt_map_package;
