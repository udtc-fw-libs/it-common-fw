`timescale 1ns / 1ps
//--==================================================================================================
//-- Company     : NCSR Demokritos/CERN
//-- Engineer    : Yiannis Kazas
//-- Module Name : Aurora_to_AXI4_wrapper - RTL
//--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//-- Description : Aurora Decoded Data to AXI Wrapper
//--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//-- Last Changes : 26-Nov-2020
//--==================================================================================================


module Aurora_stream_to_AXI4_wrapper(
     // Data from Descrambler //
     RX_DATA_i,
     RX_HEADER_1_i,
     RX_HEADER_0_i,
     RX_BUF_ERR_i,
     RXDATAVALID_IN_i,
     RX_LANE_UP_i,
     // AXI RX Interface
     M_AXI_RX_TDATA_o,
     M_AXI_RX_TVALID_o,
     M_AXI_RX_TKEEP_o,
     M_AXI_RX_TLAST_o,
 
     // AXI RX User K Interface
     M_AXI_RX_USER_K_DATA_o,
     M_AXI_RX_USER_K_TVALID_o,
                                                                                 
     // Status Interface
     RX_HARD_ERR_o,
     RX_SOFT_ERR_o,
     RX_CHANNEL_UP_o,
     RX_LANE_UP_o,
 
     // System Interface
     USER_CLK_i,
     RESET2FC_o,
     SYSRESET_TO_CORE_i,

     LINK_RESET_OUT_o
 );
 
 `define DLY #1
 localparam INTER_CB_GAP = 5'd15;
 localparam SEQ_COUNT = 4;
 localparam BACKWARD_COMP_MODE1 = 1'b0; //disable check for interCB gap
 localparam BACKWARD_COMP_MODE2 = 1'b0; //reduce RXCDR lock time, Block Sync SH max count, disable CDR FSM in wrapper
 localparam BACKWARD_COMP_MODE3 = 1'b0; //clear hot-plug counter with any valid btf detected

 
 //***********************************Port Declarations*******************************
     // Data from Descrambler //
       input  [63:0]     RX_DATA_i;
       input             RX_HEADER_1_i;
       input             RX_HEADER_0_i;
       input             RX_BUF_ERR_i;
       input             RXDATAVALID_IN_i; 
       input             RX_LANE_UP_i;
     // RX AXI Interface 
       output [0:63]     M_AXI_RX_TDATA_o; 
       output [0:7]      M_AXI_RX_TKEEP_o; 
       output            M_AXI_RX_TLAST_o; 
       output            M_AXI_RX_TVALID_o; 
     // RX User K interface
       output   [0:63]   M_AXI_RX_USER_K_DATA_o; 
       output            M_AXI_RX_USER_K_TVALID_o; 
     // Status Interface
       output            RX_HARD_ERR_o; 
       output            RX_SOFT_ERR_o; 
       output            RX_CHANNEL_UP_o; 
       output            RX_LANE_UP_o; 
     // System Interface
       input               USER_CLK_i; 
       output              RESET2FC_o; 
       input               SYSRESET_TO_CORE_i; 
       output              LINK_RESET_OUT_o; 
 
 //*********************************Wire Declarations**********************************
 
       wire                drp_clk;
       wire                rst_drp;
       wire    [0:63]     tx_d_i2; 
       wire               tx_src_rdy_n_i2; 
       wire               tx_dst_rdy_n_i2; 
       wire    [0:2]      tx_rem_i2; 
       wire    [0:2]      tx_rem_i3; 
       wire               tx_sof_n_i2; 
       wire               tx_eof_n_i2; 
       wire    [0:63]     rx_d_i2; 
       wire               rx_src_rdy_n_i2; 
       wire    [0:2]      rx_rem_i2; 
       wire    [0:2]      rx_rem_i3; 
       wire               rx_sof_n_i2; 
       wire               rx_eof_n_i2;
 
 
       wire    [0:63]     rx_d_i; 
       wire               rx_src_rdy_n_i; 
       wire    [0:2]      rx_rem_i; 
       wire               rx_sof_n_i; 
       wire               rx_eof_n_i; 
       wire    [0:55]     user_k_rx_data_i; 
       wire    [0:63]     user_k_rx_data_vector_i; 
       wire    [0:3]      user_k_rx_blk_no_i; 
       wire               user_k_rx_src_rdy_n_i; 
 
 
 
 
       wire                system_reset_c; 
       wire               gt_pll_lock_i; 
       wire               gt_pll_lock_ii; 
       wire               raw_tx_out_clk_i; 
       wire               ch_bond_done_i; 
       wire               rx_channel_up_i; 
       wire    [0:63]     rx_pe_data_i; 
       wire               rx_pe_data_v_i; 
       wire               rx_lossofsync_i; 
       wire               check_polarity_i; 
       wire               rx_neg_i; 
       wire               rx_polarity_i; 
       wire               rx_hard_err_i; 
       wire               rx_soft_err_i; 
//       wire               RX_LANE_UP_i; 
//       wire               rx_buf_err_i; 
//       wire               rx_header_1_i; 
//       wire               rx_header_0_i; 
       wire               rx_reset_i; 
       wire               reset_lanes_i; 
       wire               en_chan_sync_rx; 
       wire               chan_bond_reset_i; 
 
 
 
       wire               got_na_idles_i; 
       wire               got_idles_i; 
       wire               got_cc_i; 
       wire               rxdatavalid_to_ll_i; 
       wire               remote_ready_i; 
       wire               got_cb_i; 
 
       wire               rx_sep_i; 
       wire               rx_sep7_i; 
       wire    [0:2]      rx_sep_nb_i; 
 
 
     
       wire               rx_user_k_i; 
       wire    [0:3]      rx_user_k_blk_no_i; 
     //Datavalid signal is routed to Local Link
//       wire               rxdatavalid_i; 
       wire               rxdatavalid_to_lanes_i; 
       wire               txclk_locked_c; 
 
       wire               drp_clk_i;
       wire    [8:0] drpaddr_in_i;
       wire    [15:0]     drpdi_in_i;
       wire    [15:0]     drpdo_out_i; 
       wire               drprdy_out_i; 
       wire               drpen_in_i; 
       wire               drpwe_in_i; 
       wire               fsm_resetdone; 
       wire               link_reset_i; 
       wire               mmcm_not_locked_i; 

 
       reg                RX_SOFT_ERR_o; 
  
       wire               reset; 
 
       wire    RESET2FC_i;
       reg                RESET2FC_r; 
 
      wire sysreset_to_core_sync;
      wire             pma_init_sync; 

       wire [0:0] in_polarity_i; 
       wire [0:0] hld_polarity_i; 
       wire [0:0] polarity_val_i; 
 

 //*********************************Main Body of Code**********************************
 

     // Connect top level logic
     
     assign rx_channel_up_i = RX_LANE_UP_i;
 
     assign          RX_CHANNEL_UP_o  = rx_channel_up_i;
 
     always @(posedge USER_CLK_i)
       if(SYSRESET_TO_CORE_i)
               RX_SOFT_ERR_o  <= `DLY 1'b0;
       else
               RX_SOFT_ERR_o  <= `DLY (|rx_soft_err_i) & rx_channel_up_i;
 
 
     assign  rxdatavalid_to_lanes_i = |RXDATAVALID_IN_i;


   assign LINK_RESET_OUT_o   =  link_reset_i;
 
     //_________________________Instantiate Lane 0______________________________
 
       assign         RX_LANE_UP_o =   RX_LANE_UP_i; 
 
 
 
Aurora_stream_flow_control_decoding Aurora_flow_control
     (
         // RX LL 
         .RX_PE_DATA(rx_pe_data_i[0:63]),
         .RX_PE_DATA_V(rx_pe_data_v_i), 

//         .RX_SEP7(rx_sep7_i), 
//         .RX_SEP(rx_sep_i), 
//         .RX_SEP_NB(rx_sep_nb_i[0:2]),
 
         .RX_USER_K(rx_user_k_i), 
         .RX_USER_K_BLK_NO(rx_user_k_blk_no_i[0:3]),
 
         // GTX Interface
         .RX_DATA(RX_DATA_i[63:0]),
         .RX_HEADER_1(RX_HEADER_1_i), 
         .RX_HEADER_0(RX_HEADER_0_i), 
         .RX_BUF_ERR(|RX_BUF_ERR_i),
         .RXDATAVALID_IN(rxdatavalid_to_lanes_i),         
         // Global Logic Interface
         .RX_LANE_UP(RX_LANE_UP_i), 
         .RX_HARD_ERR(rx_hard_err_i), 
         .RX_SOFT_ERR(rx_soft_err_i), 
         .GOT_NA_IDLE(got_na_idles_i), 
         .RXDATAVALID_TO_LL(rxdatavalid_to_ll_i), 
         .GOT_CC(got_cc_i), 
         .REMOTE_READY(remote_ready_i), 
         .GOT_CB(got_cb_i), 
         .GOT_IDLE(got_idles_i), 

         // System Interface
         .USER_CLK(USER_CLK_i),
         .RESET2FC(RESET2FC_i),
         .RESET(SYSRESET_TO_CORE_i)
     );
 

 
 //_____________________________ RX AXI SHIM _______________________________

aurora_ip_1lane_LL_TO_AXI #
     (
        .DATA_WIDTH(64),
        .STRB_WIDTH(8),
        .REM_WIDTH (3)
     )

     ll_to_axi_data_i
     (
      .LL_IP_DATA(rx_d_i),
      .LL_IP_SOF_N(rx_sof_n_i),
      .LL_IP_EOF_N(rx_eof_n_i),
      .LL_IP_REM(rx_rem_i),
      .LL_IP_SRC_RDY_N(rx_src_rdy_n_i),
      .LL_OP_DST_RDY_N(),

      .AXI4_S_OP_TVALID(M_AXI_RX_TVALID_o),
      .AXI4_S_OP_TDATA(M_AXI_RX_TDATA_o),
      .AXI4_S_OP_TKEEP(M_AXI_RX_TKEEP_o),
      .AXI4_S_OP_TLAST(M_AXI_RX_TLAST_o),
      .AXI4_S_IP_TREADY(1'b0)

     );

     assign user_k_rx_data_vector_i = {4'h0,user_k_rx_blk_no_i[0:3],user_k_rx_data_i[0:55]};

 
aurora_ip_1lane_LL_TO_AXI #
     (
        .DATA_WIDTH(64),
        .STRB_WIDTH(8),
        .REM_WIDTH (3)
     )
 
     ll_to_axi_user_k_i
     (
      .LL_IP_DATA(user_k_rx_data_vector_i),
      .LL_IP_SOF_N(),
      .LL_IP_EOF_N(),
      .LL_IP_REM(),
      .LL_IP_SRC_RDY_N(user_k_rx_src_rdy_n_i),
      .LL_OP_DST_RDY_N(),
 
      .AXI4_S_OP_TVALID(M_AXI_RX_USER_K_TVALID_o),
      .AXI4_S_OP_TDATA(M_AXI_RX_USER_K_DATA_o),
      .AXI4_S_OP_TKEEP(),
      .AXI4_S_OP_TLAST(),
      .AXI4_S_IP_TREADY(1'b0)
 
     );
 
     // RX LOCALLINK
aurora_streaming_mode_SIMPLEX_RX_STREAM simplex_rx_ll_i
    (
         // LocalLink RX Interface
         .RX_D(rx_d_i),
         .RX_SRC_RDY_N(rx_src_rdy_n_i),

         // Aurora Lane Interface
         .RX_PE_DATA(rx_pe_data_i),
         .RX_PE_DATA_V(rx_pe_data_v_i),

         // LocalLink RX USER_K Interface
         .USER_K_RX_DATA(user_k_rx_data_i),
         .USER_K_RX_SRC_RDY_N(user_k_rx_src_rdy_n_i),
         .USER_K_RX_BLK_NO(user_k_rx_blk_no_i),
 
         .RX_USER_K(rx_user_k_i),
         .RX_USER_K_BLK_NO(rx_user_k_blk_no_i),
 
         // Global Logic Interface
         .RX_CHANNEL_UP(rx_channel_up_i),
         // System Interface 
         .USER_CLK(USER_CLK_i),
         .RESET(SYSRESET_TO_CORE_i)
    );
 
      assign rst_drp = pma_init_sync; 


         assign  RESET2FC_ii  = RESET2FC_i ;

    always @(posedge USER_CLK_i)
         RESET2FC_r <= `DLY RESET2FC_ii;

    assign RESET2FC_o = RESET2FC_r;


 endmodule

