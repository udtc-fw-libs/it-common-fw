`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/24/2020 03:57:49 PM
// Design Name: 
// Module Name: Aurora_flow_control_decoding
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Aurora_stream_flow_control_decoding(
 
     RX_PE_DATA,
     RX_PE_DATA_V,
 
     RX_SEP7,
     RX_SEP,
     RX_SEP_NB,
 
     RX_USER_K,
     RX_USER_K_BLK_NO,
     RX_DATA,
     // MGT Interface
     
     RX_HEADER_1,
     RX_HEADER_0,
     RX_BUF_ERR,
     RXDATAVALID_IN,
     
     // Global Logic Interface
     RX_LANE_UP,
     RX_HARD_ERR,
     RX_SOFT_ERR,
     RXDATAVALID_TO_LL,
     GOT_CC,
     REMOTE_READY,
     GOT_CB,
     GOT_NA_IDLE,
     GOT_IDLE,
     
     //System Interface
     USER_CLK,
     RESET2FC,
     RESET
 );
 
 //***********************************Port Declarations*******************************
  
     // RX  Interface
       output    [0:63]     RX_PE_DATA; 
       output               RX_PE_DATA_V; 
 
       output               RX_SEP7; 
       output               RX_SEP; 
       output    [0:2]      RX_SEP_NB; 
 
       output               RX_USER_K; 
       output    [0:3]      RX_USER_K_BLK_NO; 
     // MGT Interface
       input     [63:0]     RX_DATA;               // 8-byte data bus from the MGT. 
       input                RX_HEADER_1 ;           // Bit 0 sync header of block code on RX_DATA. 
       input                RX_HEADER_0 ;          // Bit 1 sync header of block code on RX_DATA. 
       input                RX_BUF_ERR;             // Overflow/Underflow of RX buffer detected. 
       input                RXDATAVALID_IN; 
 
     // Global Logic Interface
       input               RX_LANE_UP;             // Lane is ready for bonding and verification. 
       output               RX_HARD_ERR;          // Hard error detected. 
       output               RX_SOFT_ERR;          // Soft error detected. 
       output               GOT_NA_IDLE;            // Not-Aligned Idle symbols received. 
       output               RXDATAVALID_TO_LL;      // RXDATAVALID_TO_LL received. 
       output               GOT_CC;                 // Not-Aligned Idle symbols received. 
       output               REMOTE_READY;           // Remote Ready signal for channel init sm. 
       output               GOT_CB;                 // Not-Aligned Idle symbols received. 
       output               GOT_IDLE;               // Aligned Idle symbols received. 
 
     // System Interface
       input                USER_CLK;               // System clock for all non-MGT Aurora Logic. 
       output               RESET2FC; 
       input                RESET;                  // Global Reset . 
 
 //*********************************Wire Declarations**********************************
 
       wire                  rx_enable_err_detect_i; 
       wire                  rx_hard_err_reset_i; 
       wire                  illegal_btf_i; 
 
 //*********************************Main Body of Code**********************************
 
        assign rx_enable_err_detect_i = 1'b1;  
        
     // Symbol Decode module
    aurora_streaming_mode_SYM_DEC sym_dec_i
     (
         // Lane Init SM Interface
         .LANE_UP(RX_LANE_UP),
         .RX_NA_IDLE(GOT_NA_IDLE),
         .RX_CC(GOT_CC),
         .REMOTE_READY(REMOTE_READY),
         .RX_CB(GOT_CB),
         .RX_IDLE(GOT_IDLE),
    
         // RX  Interface
         .RX_PE_DATA(RX_PE_DATA),
         .RX_PE_DATA_V(RX_PE_DATA_V),
    
    
         .RX_USER_K(RX_USER_K),
         .RX_USER_K_BLK_NO(RX_USER_K_BLK_NO),
    
         //Error Detect Interface
         .ILLEGAL_BTF(illegal_btf_i),
    
         // MGT Interface
         .RX_DATA(RX_DATA),
    
         .RX_HEADER_1(RX_HEADER_1),
         .RX_HEADER_0(RX_HEADER_0),
         .RXDATAVALID_IN(RXDATAVALID_IN),
    
         // System Interface
         .USER_CLK(USER_CLK),
         .RESET2FC(RESET2FC),
         .RESET(RESET)
     );
 
         // Error Detection module
    aurora_streaming_mode_SIMPLEX_RX_ERR_DETECT simplex_rx_err_detect_i
     (
         // Lane Init SM Interface
         .RX_ENABLE_ERR_DETECT(rx_enable_err_detect_i),
 
         .RX_HARD_ERR_RESET(rx_hard_err_reset_i),
 
         // Global Logic Interface
         .RX_HARD_ERR(RX_HARD_ERR),
         .RX_SOFT_ERR(RX_SOFT_ERR),
 
         //Sym decoder interface
         .ILLEGAL_BTF(illegal_btf_i),
   
         // MGT Interface
         .RX_BUF_ERR(RX_BUF_ERR),
         .RX_HEADER_1(RX_HEADER_1),
         .RX_HEADER_0(RX_HEADER_0),
         .RXDATAVALID_IN(RXDATAVALID_IN),
 
         // System Interface
         .USER_CLK(USER_CLK)
     );
 
 endmodule
