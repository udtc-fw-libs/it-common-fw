--==================================================================================================
-- Company     : NCSR Demokritos / CERN
-- Engineer    : Yiannis Kazas
-- Module Name : Custom_Aurora_rx_wrapper - Behavioral
--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- Description : Wrapper for Custom aurora receiver
--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- Last change : 26-Nov-2020
--==================================================================================================

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;
use work.user_package.all;

--==================================================================================================
entity Custom_Aurora_rx_wrapper is
    Port (
        -- Clock & Reset 
        Reset_i            : in std_logic;
        Clk_i              : in std_logic;
        -- Control & Status to/from IPBus
        ctrl_aurora_rx     : in ctrl_aurora_type;
        stat_aurora_rx     : out stat_aurora_type;
        -- Aurora encoded data in
        lpgbt_frames32_i   : in lpgbtfpga_uplink_chipframe_array;
        -- Decoded data out
        reg_frame_valid_o  : out rdout_signal_array_type;
        reg_frame_o        : out rdout_frame_array_type;
        data_frame_valid_o : out rdout_signal_array_type;
        data_frame_last_o  : out rdout_signal_array_type;
        data_frame_o       : out rdout_frame_array_type 
    );
end Custom_Aurora_rx_wrapper;
--==================================================================================================



architecture Behavioral of Custom_Aurora_rx_wrapper is

--==================================================================================================
    -- Start of Signal Declaration --
--==================================================================================================
    signal rx_lane_up      : rdout_signal_array_type;
    signal rx_channel_up   : rdout_signal_array_type;
    signal gt_locked       : rdout_signal_array_type;
    
    type aurora_error_type is array (0 to (NUM_CHIPS-1)) of std_logic_vector(15 downto 0);
    type aurora_error_array is array (0 to (NUM_HYBRIDS-1)) of aurora_error_type;
    signal rx_hard_error_cntr : aurora_error_array;
    signal rx_soft_error_cntr : aurora_error_array;
    
    signal module_addr : natural range 0 to NUM_HYBRIDS-1;
    signal chip_addr   : natural range 0 to NUM_CHIPS-1;
--==================================================================================================
    -- END of signal Declaration --
--==================================================================================================

begin

--==================================================================================================
-- START OF MAIN BODY --
--==================================================================================================

    module_addr <= to_integer(unsigned(ctrl_aurora_rx.error_cntr_module_addr));
    chip_addr   <= to_integer(unsigned(ctrl_aurora_rx.error_cntr_chip_addr));
    stat_aurora_rx.rx_lane_up       <= rx_lane_up;
    stat_aurora_rx.rx_channel_up    <= rx_channel_up;
    stat_aurora_rx.rx_hard_err_cntr <= rx_hard_error_cntr(module_addr)(chip_addr);
    stat_aurora_rx.rx_soft_err_cntr <= rx_soft_error_cntr(module_addr)(chip_addr);

    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Aurora RX Decoder Instantiation --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Aurora_rx_module: for h in 0 to NUM_HYBRIDS-1 generate
        Aurora_rx_chip: for c in 0 to NUM_CHIPS-1 generate
            Aurora_rx_decoder: entity work.aurora_rx_lane
                Port map (
                -- Reset & Clock
                Rst_i                  => Reset_i,
                Clk_rx_i               => Clk_i,
                -- Input Data          
                RX_data_i              => lpgbt_frames32_i(h)(c),
                -- Output Data         
                Register_frame_valid_o => Reg_frame_valid_o(h)(c),
                Register_frame_o       => Reg_frame_o(h)(c),
                Data_frame_valid_o     => Data_frame_valid_o(h)(c),
                Data_frame_last_o      => Data_frame_last_o(h)(c),
                Data_frame_o           => Data_frame_o(h)(c),
                -- Status signals     
                RX_lane_up_o           => rx_lane_up(h)(c),
                RX_channel_up_o        => rx_channel_up(h)(c),
                Hard_error_cntr_o      => rx_hard_error_cntr(h)(c),
                Soft_error_cntr_o      => rx_soft_error_cntr(h)(c)
            );
        end generate Aurora_rx_chip;
    end generate Aurora_rx_module;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
--==================================================================================================

end Behavioral;
