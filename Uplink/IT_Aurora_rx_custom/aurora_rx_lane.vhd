--==================================================================================================
-- Company     : NCSR Demokritos/CERN
-- Engineer    : Yiannis Kazas
-- Module Name : Aurora_rx_lane - RTL
--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- Description : Custom Aurora Receiver, single lane. Accepts scrambled 32-bit frames as input 
-- and delivers 64-bit descrambled frames as output (AXI-protocol) 
--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- Last Changes : 21-Feb-2022
--==================================================================================================

library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.user_package.all;
library unisim ;
use unisim.vcomponents.all ;

--==================================================================================================
entity aurora_rx_lane is 
    port (
        -- Reset & Clock
        Rst_i       : in std_logic;
        Clk_rx_i    : in std_logic;
        -- Input Data
        RX_data_i   : in std_logic_vector(31 downto 0);
        -- Output Data
        Register_frame_valid_o : out std_logic;
        Register_frame_o       : out std_logic_vector(63 downto 0);
        Data_frame_valid_o     : out std_logic;
        Data_frame_last_o      : out std_logic;
        Data_frame_o           : out std_logic_vector(63 downto 0);
        -- Status signals
        RX_lane_up_o           : out std_logic;
        RX_channel_up_o        : out std_logic;
        Hard_error_cntr_o      : out std_logic_vector(15 downto 0);
        Soft_error_cntr_o      : out std_logic_vector(15 downto 0)
       );
end aurora_rx_lane;
--==================================================================================================


architecture behavioral of aurora_rx_lane is

--==================================================================================================
    -- Start of Component Declaration --
--==================================================================================================
    -- Gearbox 32 to 66 --
    component gearbox_32_66
        port (
            clk       : in std_logic;
            arst      : in std_logic;
            din       : in std_logic_vector(31 downto 0);
            din_valid : in std_logic;
            slip_to_frame : in std_logic;
            dout          : out std_logic_vector(65 downto 0);
            dout_valid    : out std_logic;
            slip_count    : out std_logic_vector(6 downto 0)
            );
    end component gearbox_32_66;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    -- Word Align Control --
    component word_align_control 
        Port (
           clk           : in std_logic;
           arst          : in std_logic;
           din_framed    : in std_logic;
           din_valid     : in std_logic;
           slip_to_frame : out std_logic; 
           word_locked	 : out std_logic);
    end component word_align_control;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    -- Descrambler --
    component descrambler
        port (
            data_in   : in std_logic_vector(0 to 65);
            data_out  : out std_logic_vector(63 downto 0);
            enable    : in std_logic;
            sync_info : out std_logic_vector(1 downto 0);
            clk       : in std_logic;
            rst       : in std_logic
        );
    end component descrambler;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
--    -- Clock Correction
--    component aurora_ip_1lane_CLOCK_CORRECTION_CHANNEL_BONDING 
--        generic(
--            INTER_CB_GAP        : std_logic_vector(4 downto 0);
--            LOW_WATER_MARK      : std_logic_vector(12 downto 0);
--            HIGH_WATER_MARK     : std_logic_vector(12 downto 0);
--            BACKWARD_COMP_MODE3 : std_logic;
--            CH_BOND_MAX_SKEW    : std_logic_vector(1 downto 0);
--            CH_BOND_MODE        : std_logic_vector(1 downto 0)
--        );
--        port (
--            GTX_RX_DATA_IN       : in std_logic_vector(31 downto 0);
--            GTX_RX_DATAVALID_IN  : in std_logic;
--            GTX_RX_HEADER_IN     : in std_logic_vector(1 downto 0);
--            ENCHANSYNC           : in std_logic;
--            CHAN_BOND_RESET      : in std_logic;
--            RXLOSSOFSYNC_IN      : in std_logic;
--            CC_RX_DATA_OUT       : out std_logic_vector(63 downto 0);
--            CC_RX_BUF_STATUS_OUT : out std_logic_vector(1 downto 0);            
--            CC_RX_DATAVALID_OUT  : out std_logic;
--            CC_RX_HEADER_OUT     : out std_logic_vector(1 downto 0);
--            CC_RXLOSSOFSYNC_OUT  : out std_logic;
--            RXCHANISALIGNED      : out std_logic;
            
--            CBCC_FIFO_RESET_WR_CLK          : in std_logic;
--            CBCC_FIFO_RESET_TO_FIFO_WR_CLK  : in std_logic;
--            CBCC_FIFO_RESET_RD_CLK          : in std_logic;
--            CBCC_FIFO_RESET_TO_FIFO_RD_CLK  : in std_logic;
--            CBCC_ONLY_RESET_RD_CLK          : in std_logic;
--            CBCC_RESET_CBSTG2_RD_CLK        : in std_logic;
--            ANY_VLD_BTF_FLAG                : out std_logic;
--            START_CB_WRITES_OUT             : out std_logic;
--            ALL_START_CB_WRITES_IN          : in std_logic;
--            ALL_VLD_BTF_FLAG_IN             : in std_logic;
--            PERLN_DO_RD_EN                  : out std_logic;
--            MASTER_DO_RD_EN                 : in std_logic;
--            FIRST_CB_BITERR_CB_RESET_OUT    : out std_logic;
--            FINAL_GATER_FOR_FIFO_DIN        : out std_logic;
            
--            CHBONDI                         : in std_logic_vector(4 downto 0);
--            CHBONDO                         : out std_logic_vector(4 downto 0);
            
--            RESET                           : in std_logic;
--            RD_CLK                          : in std_logic;
--            INIT_CLK                        : in std_logic;
--            WR_ENABLE                       : in std_logic;
--            HPCNT_RESET_IN                  : in std_logic;
--            GTXRESET_IN                     : in std_logic;
--            USER_CLK                        : in std_logic;
--            LINK_RESET                      : out std_logic_vector(1 downto 0)
--        );
--    end component aurora_ip_1lane_CLOCK_CORRECTION_CHANNEL_BONDING;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    -- Aurora to AXI wrapper (framing mode, RD53A)
    component Aurora_to_AXI4_wrapper
        port (
            -- System Interface 
            USER_CLK_i          : in std_logic;
            SYSRESET_TO_CORE_i  : in std_logic;
            RESET2FC_o          : out std_logic;
            LINK_RESET_OUT_o    : out std_logic;
            RX_LANE_UP_i        : in std_logic;
            -- Data from Descrambler
            RX_DATA_i           : in std_logic_vector (63 downto 0);
            RX_HEADER_1_i       : in std_logic;
            RX_HEADER_0_i       : in std_logic;
            RX_BUF_ERR_i        : in std_logic;
            RXDATAVALID_IN_i    : in std_logic;
            -- RX AXI Interface
            M_AXI_RX_TDATA_o         : out std_logic_vector(0 to 63);
            M_AXI_RX_TKEEP_o         : out std_logic_vector(0 to 7);
            M_AXI_RX_TLAST_o         : out std_logic;
            M_AXI_RX_TVALID_o        : out std_logic;
            -- RX User K Interface
            M_AXI_RX_USER_K_DATA_o   : out std_logic_vector(0 to 63);
            M_AXI_RX_USER_K_TVALID_o : out std_logic;
            --Status Interface 
            RX_HARD_ERR_o            : out std_logic;   
            RX_SOFT_ERR_o            : out std_logic;  
            RX_CHANNEL_UP_o          : out std_logic;
            RX_LANE_UP_o             : out std_logic
            );
    end component Aurora_to_AXI4_wrapper;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- Aurora to AXI wrapper (Streaming mode, RD53B)
    component Aurora_stream_to_AXI4_wrapper 
        port (
            -- System Interface 
            USER_CLK_i          : in std_logic;
            SYSRESET_TO_CORE_i  : in std_logic;
            RESET2FC_o          : out std_logic;
            LINK_RESET_OUT_o    : out std_logic;
            RX_LANE_UP_i        : in std_logic;
            -- Data from Descrambler
            RX_DATA_i           : in std_logic_vector (63 downto 0);
            RX_HEADER_1_i       : in std_logic;
            RX_HEADER_0_i       : in std_logic;
            RX_BUF_ERR_i        : in std_logic;
            RXDATAVALID_IN_i    : in std_logic;
            -- RX AXI Interface
            M_AXI_RX_TDATA_o         : out std_logic_vector(0 to 63);
            M_AXI_RX_TKEEP_o         : out std_logic_vector(0 to 7);
            M_AXI_RX_TLAST_o         : out std_logic;
            M_AXI_RX_TVALID_o        : out std_logic;
            -- RX User K Interface
            M_AXI_RX_USER_K_DATA_o   : out std_logic_vector(0 to 63);
            M_AXI_RX_USER_K_TVALID_o : out std_logic;
            --Status Interface 
            RX_HARD_ERR_o            : out std_logic;   
            RX_SOFT_ERR_o            : out std_logic;  
            RX_CHANNEL_UP_o          : out std_logic;
            RX_LANE_UP_o             : out std_logic
            );
    end component Aurora_stream_to_AXI4_wrapper;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
--==================================================================================================
    -- END of Component Declaration --
--==================================================================================================

    
--==================================================================================================
    -- Start of Signal Declaration --
--==================================================================================================
    signal aurora_rx_data32 : std_logic_vector(31 downto 0);
    -- Gearbox
    signal gearbox_data66 : std_logic_vector(65 downto 0);
    signal gearbox_data66_valid : std_logic;
    signal gearbox_data66_valid_d : std_logic;
    signal gearbox_slip : std_logic;

    -- Descrambler
    signal scrambled_data66 : std_logic_vector(65 downto 0);
    signal scrambled_data_valid : std_logic;
    signal scrambled_data_valid_d : std_logic;
    signal descrambled_data : std_logic_vector(63 downto 0);
    signal descrambled_header : std_logic_vector(1 downto 0);
    signal descrambler_data_valid : std_logic;
    
    signal slip_count : std_logic_vector(6 downto 0);
    signal din_framed : std_logic;
    signal gearbox_data66_reversed : std_logic_vector(65 downto 0);
    
    -- debugging --
    signal slip_count_prev : std_logic_vector(6 downto 0);
    signal slip_stable      : std_logic;
    
    --aurora axi interface
    signal data_frame           : std_logic_vector(0 to 63);  
    signal data_frame_reversed  : std_logic_vector(63 downto 0) := (others=>'0');          
    signal data_frame_keep      : std_logic_vector(0 to 7);
    signal data_frame_last      : std_logic;
    signal data_frame_valid     : std_logic;
    signal register_frame       : std_logic_vector(0 to 63);
    signal register_frame_valid : std_logic;
    signal rx_hard_error        : std_logic;
    signal rx_soft_error        : std_logic;
    signal rx_channel_up        : std_logic;
    signal rx_lane_up           : std_logic;
    -- monitoring counters
    signal rx_hard_err_cnt : unsigned(15 downto 0);
    signal rx_soft_err_cnt : unsigned(15 downto 0);

--==================================================================================================
    -- END of Signal Declaration --
--==================================================================================================

    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- Start of DEBUG CORE --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    COMPONENT Aurora_debug
    PORT (
        clk : IN STD_LOGIC;
        probe0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0); 
        probe1 : IN STD_LOGIC_VECTOR(63 DOWNTO 0); 
        probe2 : IN STD_LOGIC_VECTOR(63 DOWNTO 0); 
        probe3 : IN STD_LOGIC_VECTOR(7 DOWNTO 0); 
        probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe6 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe7 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe8 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe9 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe10 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe11 : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        probe12 : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
        probe13 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
    );
    END COMPONENT  ;
   ----------------------------------------------
   signal probe0 : STD_LOGIC_VECTOR(31 DOWNTO 0); 
   signal probe1 : STD_LOGIC_VECTOR(63 DOWNTO 0); 
   signal probe2 : STD_LOGIC_VECTOR(63 DOWNTO 0); 
   signal probe3 : STD_LOGIC_VECTOR(7 DOWNTO 0); 
   signal probe4 : STD_LOGIC_VECTOR(0 DOWNTO 0); 
   signal probe5 : STD_LOGIC_VECTOR(0 DOWNTO 0); 
   signal probe6 : STD_LOGIC_VECTOR(0 DOWNTO 0); 
   signal probe7 : STD_LOGIC_VECTOR(0 DOWNTO 0);
   signal probe8 : STD_LOGIC_VECTOR(0 DOWNTO 0);
   signal probe9 : STD_LOGIC_VECTOR(0 DOWNTO 0);
   signal probe10 : STD_LOGIC_VECTOR(0 DOWNTO 0);
   signal probe11 : STD_LOGIC_VECTOR(63 DOWNTO 0);
   signal probe12 : STD_LOGIC_VECTOR(1 DOWNTO 0);
   signal probe13 : STD_LOGIC_VECTOR(0 DOWNTO 0);

    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
begin

    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--    DEBUG_AURORA : Aurora_debug
--    PORT MAP (
--        clk => clk_rx_i,
--        probe0 => rx_data_i, 
--        probe1 => data_frame, 
--        probe2 => data_frame_reversed(39 downto 32) & data_frame_reversed(47 downto 40) & data_frame_reversed(55 downto 48) & data_frame_reversed(63 downto 56) & data_frame_reversed(7 downto 0) & data_frame_reversed(15 downto 8) & data_frame_reversed(23 downto 16) & data_frame_reversed(31 downto 24), 
--        probe3 => data_frame_keep, 
--        probe4(0) => data_frame_valid, 
--        probe5(0) => register_frame_valid, 
--        probe6(0) => data_frame_last, 
--        probe7(0) => rx_channel_up,
--        probe8(0) => rx_lane_up,
--        probe9(0) => rx_soft_error,
--        probe10(0) => rx_hard_error,
--        probe11 => descrambled_data,
--        probe12 => descrambled_headerbe12,
--        probe13(0) => scrambled_data_valid and gearbox_data66_valid
--    );
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- END of DEBUG CORE --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    
--==================================================================================================
-- START OF MAIN BODY --
--==================================================================================================

    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Register Outputs --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    process(Clk_rx_i)
    begin
        if rising_edge(Clk_rx_i) then
            Rx_lane_up_o           <= rx_lane_up;
            Rx_channel_up_o        <= rx_channel_up;
            if (rx_soft_error = '1') or (rx_hard_error = '1') then
                Register_frame_valid_o <= '0';
                Register_frame_o       <= (others => '0');
                Data_frame_valid_o     <= '0';
                data_frame_reversed    <= (others => '0');
                Data_frame_last_o      <= '0';
             else
                Register_frame_valid_o <= register_frame_valid;
                Register_frame_o       <= register_frame;
                Data_frame_valid_o     <= data_frame_valid;
                if (data_frame_keep = x"FF") then
                    data_frame_reversed <= data_frame;
                else
                    data_frame_reversed <= data_frame(0 to 31) & x"FFFF0000";
                end if;
                Data_frame_last_o <= data_frame_last;
            end if;
        end if;
    end process;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    
    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Reshuffle bytes in aurora data packet --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Aurora_frame_byte_order: if FRONT_END = RD53A generate
        -- reverse the byte order in each 32bit word
        Data_frame_o <= data_frame_reversed(39 downto 32) & data_frame_reversed(47 downto 40) & data_frame_reversed(55 downto 48) & data_frame_reversed(63 downto 56) & data_frame_reversed(7 downto 0) & data_frame_reversed(15 downto 8) & data_frame_reversed(23 downto 16) & data_frame_reversed(31 downto 24); 
    end generate Aurora_frame_byte_order;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Aurora_stream_byte_order: if FRONT_END = RD53B generate
        Data_frame_o <= data_frame_reversed(7 downto 0) & data_frame_reversed(15 downto 8) & data_frame_reversed(23 downto 16) & data_frame_reversed(31 downto 24) & data_frame_reversed(39 downto 32) & data_frame_reversed(47 downto 40) & data_frame_reversed(55 downto 48) & data_frame_reversed(63 downto 56); 
    end generate Aurora_stream_byte_order;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Monitoring Counters --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Hard_error_cntr_o <= std_logic_vector(rx_hard_err_cnt);
    Soft_error_cntr_o <= std_logic_vector(rx_soft_err_cnt);
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- Count Aurora hard errors
    process(Clk_rx_i)
    begin
        if rising_edge(Clk_rx_i) then
            if (Rst_i = '1') then
                rx_hard_err_cnt <= (others => '0');
            elsif (rx_hard_error = '1') then
                rx_hard_err_cnt <= rx_hard_err_cnt + 1;
            end if;
        end if;
    end process;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- Count Aurora soft errors
    process(Clk_rx_i)
    begin
        if rising_edge(Clk_rx_i) then
            if (Rst_i = '1') then
                rx_soft_err_cnt <= (others => '0');
            elsif (rx_soft_error = '1') then
                rx_soft_err_cnt <= rx_soft_err_cnt + 1;
            end if;
        end if;
    end process;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Gearbox 32 to 66 --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- Reverse bit order (gearbox wants the data lsb to msb)
    process(RX_data_i)
    begin
        for i in RX_data_i'high downto Rx_data_i'low loop
            aurora_rx_data32(i)<= RX_data_i(RX_data_i'high-i);
        end loop;
    end process;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    gearbox_32_66_inst: gearbox_32_66
    port map (
        clk           => clk_rx_i,
        arst          => rst_i,
        din           => aurora_rx_data32,
        din_valid     => '1',--data_valid_i,
        slip_to_frame => gearbox_slip,
        dout          => gearbox_data66,
        dout_valid    => gearbox_data66_valid,
        slip_count    => slip_count
        ); 
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Word Alignment Control --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- Reverse Data order from gearbox to Word Alignment Control --
    process(gearbox_data66)
    begin
        for i in gearbox_data66'high downto gearbox_data66'low loop
            gearbox_data66_reversed(i)<= gearbox_data66(gearbox_data66'high-i);
        end loop;
    end process;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    din_framed <= gearbox_data66_reversed(65) xor gearbox_data66_reversed(64);    
    align_ctrl: word_align_control 
    Port map (
        clk           => clk_rx_i,
        arst          => rst_i,
        din_framed    => din_framed, --gearbox_data66(65 downto 64),
        din_valid     => gearbox_data66_valid,
        slip_to_frame => gearbox_slip,
        word_locked   => scrambled_data_valid
        );
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Descrambler --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
--    process(clk_rx_i)
--    begin
--        if rising_edge(rx_clk_i) then
--            descrambler_data_valid <= pos_rx_data_valid;
--            de
            
--    descrambler_data_valid <= pos_rx_data_
    descrambler_data_valid <= scrambled_data_valid and gearbox_data66_valid;
    
    descrambler_cmp : descrambler 
    port map (
        data_in   => gearbox_data66_reversed,--scrambled_data66,
        data_out  => descrambled_data,
        enable    => descrambler_data_valid,--scrambled_data_valid and gearbox_data66_valid,
        sync_info => descrambled_header,
        clk       => not clk_rx_i,
        rst       => rst_i
        );
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Aurora Clock Correction Channel Bonding --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
--    cbcc_gtx : aurora_ip_1lane_CLOCK_CORRECTION_CHANNEL_BONDING
--        generic map(
--            INTER_CB_GAP        => ,
--            LOW_WATER_MARK      => ,
--            HIGH_WATER_MARK     => ,
--            BACKWARD_COMP_MODE3 => ,
--            CH_BOND_MAX_SKEW    => ,
--            CH_BOND_MODE        => ,
--        )
--        port map (
--            GTX_RX_DATA_IN       => descrambled_data,
--            GTX_RX_DATAVALID_IN  => scrambled_data_valid and gearbox_data66_valid,
--            GTX_RX_HEADER_IN     => descrambled_header,
--            ENCHANSYNC           => '0',
--            CHAN_BOND_RESET      => rst_i,
--            RXLOSSOFSYNC_IN      => '0',
--            CC_RX_DATA_OUT       => rx_data_to_axi,
--            CC_RX_BUF_STATUS_OUT => open,         
--            CC_RX_DATAVALID_OUT  => rx_data_valid_to_axi,
--            CC_RX_HEADER_OUT     => rx_header_to_axi,
--            CC_RXLOSSOFSYNC_OUT  => open,
--            RXCHANISALIGNED      => open,
            
--            CBCC_FIFO_RESET_WR_CLK          => '1',
--            CBCC_FIFO_RESET_TO_FIFO_WR_CLK  => '1',
--            CBCC_FIFO_RESET_RD_CLK          => '1',
--            CBCC_FIFO_RESET_TO_FIFO_RD_CLK  => ,
--            CBCC_ONLY_RESET_RD_CLK          => ,
--            CBCC_RESET_CBSTG2_RD_CLK        => ,
--            ANY_VLD_BTF_FLAG                => ,
--            START_CB_WRITES_OUT             => ,
--            ALL_START_CB_WRITES_IN          => ,
--            ALL_VLD_BTF_FLAG_IN             => ,
--            PERLN_DO_RD_EN                  => ,
--            MASTER_DO_RD_EN                 => ,
--            FIRST_CB_BITERR_CB_RESET_OUT    => ,
--            FINAL_GATER_FOR_FIFO_DIN        => ,
--        );
    
    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Aurora to AXI wrapper --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Gen_Aurora_AXI_wrapper : if FRONT_END = RD53A generate
        Aurora_to_AXI: Aurora_to_AXI4_wrapper
            port map (
                -- System Interface 
                USER_CLK_i          => clk_rx_i,
                SYSRESET_TO_CORE_i  => rst_i,
                RESET2FC_o          => open,
                LINK_RESET_OUT_o    => open,
                RX_LANE_UP_i        => din_framed,
                -- Data from Descrambler
                RX_DATA_i           => descrambled_data,
                RX_HEADER_1_i       => descrambled_header(1),
                RX_HEADER_0_i       => descrambled_header(0),
                RX_BUF_ERR_i        => '0',
                RXDATAVALID_IN_i    => descrambler_data_valid,--scrambled_data_valid and gearbox_data66_valid,
                -- RX AXI Interface
                M_AXI_RX_TDATA_o         => data_frame,
                M_AXI_RX_TKEEP_o         => data_frame_keep,
                M_AXI_RX_TLAST_o         => data_frame_last,
                M_AXI_RX_TVALID_o        => data_frame_valid,
                -- RX User K Interface
                M_AXI_RX_USER_K_DATA_o   => register_frame,
                M_AXI_RX_USER_K_TVALID_o => register_frame_valid,
                --Status Interface 
                RX_HARD_ERR_o            => rx_hard_error,
                RX_SOFT_ERR_o            => rx_soft_error, 
                RX_CHANNEL_UP_o          => rx_channel_up,
                RX_LANE_UP_o             => rx_lane_up
                );
    end generate Gen_Aurora_AXI_wrapper;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    
    Gen_Aurora_stream_AXI_wrapper : if FRONT_END = RD53B generate
        Aurora_to_AXI: Aurora_stream_to_AXI4_wrapper
            port map (
                -- System Interface 
                USER_CLK_i          => clk_rx_i,
                SYSRESET_TO_CORE_i  => rst_i,
                RESET2FC_o          => open,
                LINK_RESET_OUT_o    => open,
                RX_LANE_UP_i        => din_framed,
                -- Data from Descrambler
                RX_DATA_i           => descrambled_data,
                RX_HEADER_1_i       => descrambled_header(1),
                RX_HEADER_0_i       => descrambled_header(0),
                RX_BUF_ERR_i        => '0',
                RXDATAVALID_IN_i    => descrambler_data_valid,--scrambled_data_valid and gearbox_data66_valid,
                -- RX AXI Interface
                M_AXI_RX_TDATA_o         => data_frame,
                M_AXI_RX_TKEEP_o         => data_frame_keep,
                M_AXI_RX_TLAST_o         => data_frame_last,
                M_AXI_RX_TVALID_o        => data_frame_valid,
                -- RX User K Interface
                M_AXI_RX_USER_K_DATA_o   => register_frame,
                M_AXI_RX_USER_K_TVALID_o => register_frame_valid,
                --Status Interface 
                RX_HARD_ERR_o            => rx_hard_error,
                RX_SOFT_ERR_o            => rx_soft_error, 
                RX_CHANNEL_UP_o          => rx_channel_up,
                RX_LANE_UP_o             => rx_lane_up
                );
    end generate Gen_Aurora_stream_AXI_wrapper;
    
--==================================================================================================

end behavioral;
