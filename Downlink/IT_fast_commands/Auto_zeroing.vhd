----------------------------------------------------------------------------------
-- Engineer: Yiannis Kazas
-- Create Date: 07/30/2018 08:03:24 PM
-- Module Name: Auto_zeroing - rtl
-- 
-- This Block Sends global pulse command at a configurable frequency 
-- and vetoes triggers while auto-zero operation is active 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.user_package.all;
------------------------------------------------
entity Auto_zeroing is
    Port ( -- Reset signal
           Reset                : in STD_LOGIC;
           -- Input clk
           clk_10MHz            : in STD_LOGIC;
           clk_40MHz            : in STD_LOGIC;
           -- Control signals
--           Global_pulse_sent    : in STD_LOGIC; -- handshake of the global pulse command
           -- Configuration signals
           AutoZero_Source      : in AutoZero_source_type; -- Enables auto-zero functionality
           Trigger_state_i      : in trigger_state_type;
           cnfg_fastblock_i     : in cnfg_fastblock_type;
           -- Outputs to fast commands block
           AutoZero_trig_veto_o : out STD_LOGIC; -- trigger veto
           Global_pulse_cmd_o   : out STD_LOGIC -- global pulse command
--           Global_pulse_data_o  : out STD_LOGIC_VECTOR (9 downto 0)); -- Data field of the command (address & data)
           );
end Auto_zeroing;
------------------------------------------------


architecture rtl of Auto_zeroing is

--=================================================--
-- BEGIN Signal declaration
--=================================================--
-- Auto-Zero state machine
type AutoZero_state_type is (Idle,Send_glb_pulse, Wait_for_glb_pulse);
signal AutoZero_state : AutoZero_State_type := Idle;

-- Auto-Zero enable
signal autozero_en : std_logic:='0';
-- Trigger veto enable
signal veto_en     : std_logic:='0';

-- Trigger-Veto state machine
type Veto_state_type is (Veto_Idle, Veto_active);
signal Veto_state : Veto_state_type := Veto_Idle;

-- signals setting trigger veto
signal AutoZero_veto_cntr     : integer := 1;
signal glb_pulse_data         : integer range 0 to 15;  -- duration of global pulse as specified by the data field of the command 
signal glb_pulse_duration     : integer range 0 to 160; -- duration of global pulse expressed in multiples of 40MHz clk cycles
signal delay_veto             : integer range 0 to 31:=5;  -- 
signal AutoZero_veto_duration : integer;

-- signals setting AutoZero frequency
signal AutoZero_freq_cntr     : integer := 1;
signal AutoZero_period        : integer;
--=================================================--
-- END Signal declaration
--=================================================--

begin
--======================================================================
-- BEGIN Main Body
--======================================================================
-- Configure Auto-Zero Enable signal
autozero_en <= '1' when (AutoZero_source = FreeRun_AutoZero) else '0';
------------------------------------------------------------------------

------------------------------------------------------------------------
-- Read signals from IPBus registers
------------------------------------------------------------------------
Read_Ipbus_regs_process: process (clk_40MHz)
begin
    if rising_edge(clk_40MHz) then
        if (trigger_state_i = Idle) then
            Veto_en            <= cnfg_fastblock_i.veto_en;
            AutoZero_period    <= to_integer(unsigned(cnfg_fastblock_i.AutoZero_freq));
            delay_veto         <= to_integer(unsigned(cnfg_fastblock_i.Veto_After_AutoZero));
            Glb_pulse_data     <= to_integer(unsigned(cnfg_fastblock_i.global_pulse_data(4 downto 1)));
        end if;            
    end if;
end process;       
------------------------------------------------------------------------
------------------------------------------------------------------------


------------------------------------------------------------------------
-- Set global pulse, trigger-veto duration & auto-zero period  --
------------------------------------------------------------------------
-- Convert global pulse data value to multiples of 40MHz clk cycles 
Conv_Glb_pulse_data_process: process(Glb_pulse_data)
begin
    case Glb_pulse_data is
      when 0 to 1 =>
         glb_pulse_duration <= 1;
      when 2 =>
         glb_pulse_duration <= 1;
      when 3 => 
         glb_pulse_duration <= 2;
      when 4 =>
         glb_pulse_duration <= 4;
      when 5 =>
         glb_pulse_duration <= 8;
      when 6 =>
         glb_pulse_duration <= 16;
      when 7 =>
         glb_pulse_duration <= 32;
      when 8 =>
         glb_pulse_duration <= 64;
      when 9 to 15 =>
         glb_pulse_duration <= 128;
      when others =>
         glb_pulse_duration <= 128;
    end case;
end process;

-- Duration of trigger-veto equals global pulse duration plus user-defined delay after that 
AutoZero_veto_duration <= glb_pulse_duration + delay_veto;
------------------------------------------------------------------------

------------------------------------------------------------------------
-- Auto-Zero FSM --
------------------------------------------------------------------------
AutoZero_FSM_process: process(Reset,clk_10MHz)
begin
    if (Reset = '1') then
        AutoZero_state     <= Idle;
        Global_pulse_cmd_o <= '0';
    elsif rising_edge(clk_10MHz) then
        case AutoZero_state is 
        
        when Idle =>
            Global_pulse_cmd_o   <= '0';
            AutoZero_freq_cntr   <= 1;
            if (AutoZero_en = '1') then
                AutoZero_state <= Send_glb_pulse;
                Global_pulse_cmd_o   <= '1';
            else
                AutoZero_state <= Idle;
            end if;
                 
        when Send_glb_pulse =>
            Global_pulse_cmd_o   <= '0';
            AutoZero_freq_cntr   <= 1;
            AutoZero_state       <= Wait_for_glb_pulse;
            
        when Wait_for_glb_pulse =>
            AutoZero_freq_cntr   <= AutoZero_freq_cntr + 1;
            if (AutoZero_en = '0') then
                AutoZero_state     <= Idle;
            elsif (AutoZero_freq_cntr = (AutoZero_period-1)) then
                AutoZero_state     <= Send_glb_pulse;
                Global_pulse_cmd_o <= '1';
            else
                AutoZero_state     <= Wait_for_glb_pulse;
            end if;
            
        when others =>
            AutoZero_state <= Idle;
        end case;
    end if;
end process;
------------------------------------------------------------------------
            
------------------------------------------------------------------------
-- Trigger-Veto FSM --
------------------------------------------------------------------------    
Trig_veto_FSM: process(reset,clk_40MHz)
begin
    if (reset = '1')then
        Veto_state <= Veto_Idle;
    elsif rising_edge(clk_40MHz) then
        
        case Veto_state is 
            when Veto_Idle =>
                AutoZero_trig_veto_o <= '0';
                AutoZero_Veto_cntr   <= 1;
                if ((veto_en = '1') and (AutoZero_state = Send_Glb_pulse)) then
                    Veto_state           <= Veto_active;
                    AutoZero_trig_veto_o <= '1';
                else
                    Veto_state <= Veto_Idle;
                end if;
                        
            when Veto_Active => -- Veto triggers while global pulse is active
                AutoZero_Veto_cntr  <= AutoZero_veto_cntr +1;
                if (AutoZero_veto_cntr = AutoZero_veto_duration) then
                    Veto_state           <= Veto_Idle;
                    AutoZero_trig_veto_o <= '0';
                else
                    Veto_state           <= Veto_Active;
                    AutoZero_trig_veto_o <= '1';
                end if;
                
            when others => 
                Veto_state <= Veto_Idle;
        end case;
    end if;
end process;
------------------------------------------------------------------------
--======================================================================
-- END Main Body
--======================================================================


end rtl;
