--==================================================================================================
-- Engineer    : Yiannis Kazas
-- Module Name : Fast_commands_core - RTL
--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- Description: 
--  Fast Commands Block is responible for Trigger, ECR, BCR, 
--  Calibration Injection & Global Pulse commands. Fast commands are grouped
--  and sent to the PHY layer into packets of 4 BX cycles. 
--  They are sampled on the rising edge of 40MHz clock and stored in 4-bit buffers
--  on the falling edge of 40MHz clock. 
--  The outputs are set on the falling edge of 10MHz clock and CMD Controller samples
--  them on the rising edge of 10MHz clock.
--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- Last Changes : 30-Apr-2023
--==================================================================================================

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use ieee.std_logic_unsigned.all;
use work.user_package.all;
use IEEE.std_logic_misc.all;
--library UNISIM;
--use UNISIM.VComponents.all;

--==================================================================================================
entity Fast_commands_core is
    Port ( -- clk & reset signals
           reset               : in STD_LOGIC;
           clk_10MHz           : in STD_LOGIC;
           clk_40MHz           : in STD_LOGIC;
           clk_320MHz          : in STD_LOGIC;
           -- ctrl & cnfg signals from IPBus          
           ctrl_fastblock_i    : in ctrl_fastblock_type;
           cnfg_fastblock_i    : in cnfg_fastblock_type;
           -- commands from TTC & TLU
           ttc_sig_i           : in ttc_sig_o_type;
           tlu_data_i          : in tlu_data_type;
           -- backpressure from readout
           backpressure_i      : in STD_LOGIC;
           --external trigger from dio5
           ext_trigger_i       : in STD_LOGIC;
           -- Hit-Or lines
           Self_trigger_i      : in STD_LOGIC;
           HitOr_i             : in STD_LOGIC;--_VECTOR(3 downto 0);
           trigger_duration_o  : out integer range 0 to 128;
           -- Handshakes from PHY layer
--           Cmd_done            : in STD_LOGIC;
           Cal_Done            : in STD_LOGIC;
           Global_pulse_done   : in STD_LOGIC;
           --fast block status
           stat_fastblock_o    : out stat_fastblock_type;
           -- Outputs to PHY layer
           Trigger_cmd_o       : out integer range 0 to 15;
           Trigger_tag_o       : out integer range 0 to 31;
           ECR_cmd_o           : out STD_LOGIC;
           BCR_cmd_o           : out STD_LOGIC;
           Cal_data_o          : out STD_LOGIC_VECTOR(19 downto 0);
           Global_pulse_data_o : out STD_LOGIC_VECTOR(9 downto 0);
           -- Vetoed triggers
           Triggers_lost_o     : out STD_LOGIC_VECTOR(3 downto 0);
           Trigger_40MHz_o     : out STD_LOGIC;
           Tdc_value_o         : out STD_LOGIC_VECTOR(7 downto 0)
           );
end Fast_commands_core;
--==================================================================================================

architecture rtl of Fast_commands_core is

--==================================================================================================
-- Start of Signal Declaration
--==================================================================================================

    -- trigger configs
    signal trigger_source_raw     : std_logic_vector(3 downto 0);  
    signal user_trigger_frequency : integer range 1 to MAX_USER_TRIGGER_FREQUENCY := 1;    
    signal triggers_to_accept     : integer range 0 to MAX_NTRIGGERS_TO_ACCEPT := 0;
    signal trigger_duration       : integer range 0 to 128 :=0;--32
    
    
    signal trigger_state    : trigger_state_type;
    signal trigger_source   : trigger_source_type;
    signal AutoZero_source  : AutoZero_source_type;
    --signal Cal_source       : Cal_source_type;
    
    -- Trigger_cmd sources
    signal ipbus_trigger      : std_logic:='0';
    signal external_trigger   : std_logic:='0';
    signal testFSM_trigger    : std_logic:='0';
    signal HitOR_trigger      : std_logic:='0';
    
    -- ECR_cmd sources
    signal ipbus_ECR          : std_logic:='0';
    signal testFSM_ECR        : std_logic:='0';
    
    -- BCR_cmd sources
    signal ipbus_BCR          : std_logic:='0';
    
    -- Cal_cmd sources
    signal Cal_cmd_ipbus      : std_logic:='0'; -- Cal command (coming from ipbus) 
    signal cal_cmd_TestFSM    : std_logic:='0'; -- Cal command (coming from test-pulse fsm)
    signal Cal_data_ipbus     : std_logic_vector(19 downto 0) := (others=>'0'); -- Data for Cal command (coming from ipbus)
    signal Cal_data_Testfsm   : std_logic_vector(19 downto 0) := (others=>'0'); -- Data for Cal command (coming from test-pulse fsm)
    
    -- Glb_pulse_cmd sources
    signal FreeRun_Glb_pulse  : std_logic:='0';
    signal TestFSM_Glb_pulse  : std_logic:='0';
    signal IPBus_Glb_pulse    : std_logic:='0';
       
    -- tdc values
    signal HitOR_tdc_value    : std_logic_vector(7 downto 0);
    signal external_tdc       : std_logic_vector(7 downto 0);
    
    -- Fast-signals buffers
    signal tag_cntr             : integer range 0 to 31:=0;
    signal trigger_cmd          : std_logic:='0';
    signal Trigger_cmd_40MHz    : std_logic:='0';
    signal Trigger_buf          : std_logic_vector(3 downto 0):=(others=>'0');
    signal Trigger_cmd_10MHz    : std_logic_vector(3 downto 0):=(others=>'0');
    
    signal ECR_cmd_40MHz        : std_logic:='0'; -- command composed @40MHz clock (before buffer)
    signal ECR_cmd_10MHz        : std_logic:='0'; -- command composed @10MHz clock (after buffer)
    signal ECR_buf              : std_logic_vector(3 downto 0):=(others=>'0');
    
    signal BCR_cmd_40MHz        : std_logic:='0'; -- command composed @40MHz clock (before buffer)
    signal BCR_cmd_10MHz        : std_logic:='0'; -- command composed @10MHz clock (after buffer) 
    signal BCR_buf              : std_logic_vector(3 downto 0):=(others=>'0');
    
    signal Cal_cmd_40MHz        : std_logic:='0'; -- command composed @40MHz clock (before buffer)
    signal Cal_cmd_10MHz        : std_logic:='0'; -- command composed @10MHz clock (after buffer)
    signal Cal_data_40MHz       : std_logic_vector(19 downto 0) := (others=>'0'); -- Data for Cal command 
    signal Cal_data_10MHz       : std_logic_vector(19 downto 0):=(others=>'0');
    signal Cal_buf              : std_logic_vector(3 downto 0):=(others=>'0');
    
    signal Glb_pulse_cmd_40MHz  : std_logic:='0'; -- command composed @40MHz clock (before buffer)
    signal Glb_pulse_cmd_10MHz  : std_logic:='0'; -- command composed @10MHz clock (after buffer) 
    signal Glb_pulse_data_10MHz : std_logic_vector(9 downto 0):=(others=>'0'); 
    signal Glb_pulse_buf        : std_logic_vector(3 downto 0):=(others=>'0');  
    
    -- Veto-storage buffer
    signal veto_buf_40MHz       : std_logic_vector(3 downto 0):=(others=>'0');      
    signal veto_buf_10MHz       : std_logic_vector(3 downto 0):=(others=>'0');
          
    -- Auto-Zero signals
    signal AutoZero_en          : std_logic:='0';
    signal AutoZero_trig_veto   : std_logic:='0';
    signal AutoZero_trig_veto_i : std_logic:='0';
    signal Global_pulse_data    : std_logic_vector(9 downto 0);
    
    signal reset_int            : std_logic:='0';
    
    -- from ipbus handler
    signal trigger_start_loc  : std_logic := '0';
    signal trigger_stop_loc   : std_logic := '0';
    signal configured         : std_logic := '0';
    signal ipb_reset          : std_logic := '0';
    
    -- main fsm signals
    signal clock_enable         : std_logic := '0';
    signal main_fsm_reset       : std_logic := '0';
    signal main_fsm_reset_timer : natural := 0;        
    -- 1 - running, 0 - idle
    signal status_state         : std_logic_vector(1 downto 0);
    
    -- accept N triggers related signals
    signal trigger_counter          : std_logic_vector(31 downto 0) := (others => '0');
    signal reset_counter            : std_logic := '0';
    -- received trigger counter (to estimate ammount of triggers coming to the board)
    signal trigger_in_counter       : std_logic_vector(31 downto 0) := (others => '0');
    
    -- backressure - checks for the backressure, if so, stops triggers and waits for readout
    signal backpressure_int         : std_logic := '0';   
      
    -- done stat
    signal test_pulse_done    : std_logic := '0';
    --signal tdc 
    
    signal Trigger_cmd_reversed : std_logic_vector(3 downto 0);
    
    -- trigger multiplicity
    signal multiplicity_counter     : std_logic_vector(6 downto 0) := (others => '0');
    signal trigger_i_multiplied     : std_logic;
    signal tdc_value_multiplied     : std_logic_vector(7 downto 0) := (others => '0');
    signal tdc_value_int            : std_logic_vector(7 downto 0) := (others => '0');
    signal trigger_in_progress      : std_logic;
    
    
    
    --signal ext_trigger_source : std_logic;
--==================================================================================================
-- End of Signal Declaration
--==================================================================================================

    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- DEBUG CORE --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    COMPONENT FAST_CMDS_DEBUG
    
    PORT (
        clk : IN STD_LOGIC;
        probe0 : IN STD_LOGIC_VECTOR(1 DOWNTO 0); 
        probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
        probe5 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        probe6 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
        probe7 : IN STD_LOGIC_VECTOR(3 DOWNTO 0)
--        probe8 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
    );
    END COMPONENT  ;
     
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    begin
    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
--    DEBUG_FAST_CMDS : FAST_CMDS_DEBUG
--    PORT MAP (
--    	clk       => clk_40MHz,
--    	probe0    => status_state, 
--    	probe1(0) => trigger_cmd_40MHz, 
--    	probe2(0) => trigger_cmd, 
--    	probe3(0) => HitOr_trigger, 
--    	probe4(0) => HitOr_i, 
--    	probe5    => counter,
--    	probe6(0) => clock_enable,
--    	probe7    => trigger_cmd_10MHz
----    	probe8(0) => self_trigger_i
--    );
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- END DEBUG CORE --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
--==================================================================================================
-- Start of Main Body
--==================================================================================================
    Trigger_40MHz_o    <= trigger_cmd_40MHz;
    trigger_duration_o <= trigger_duration;
    
    -- status outputs to IPBus
    stat_fastblock_o.if_configured      <= configured;
    stat_fastblock_o.trigger_state      <= status_state;
    stat_fastblock_o.trigger_source     <= trigger_source_raw;
    stat_fastblock_o.trigger_in_counter <= trigger_in_counter;
    -- configure reset signal
    reset_int <= reset or ipb_reset;
    -- configure AutoZero_trig_veto signal
    AutoZero_trig_veto <= AutoZero_trig_veto_i when (AutoZero_source = FreeRun_AutoZero)        else '0';
    -- backpressure
    backpressure_int   <= backpressure_i       when (cnfg_fastblock_i.backpressure_enable = '1') else '0';
    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Configure trigger duration --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    multiplicity_process: process(clk_40MHz) 
        begin
            if rising_edge(clk_40MHz) then
                if reset_int = '1' then
                    multiplicity_counter <= (others => '0');
                    trigger_i_multiplied <= '0';
                    tdc_value_multiplied <= (others => '0');
                    trigger_in_progress  <= '0';
                else           
                    -- if received new trigger
                    if (trigger_cmd = '1') and (trigger_in_progress = '0') then
                        multiplicity_counter <= (others => '0');
                        trigger_i_multiplied <= '1';
                        tdc_value_multiplied <= tdc_value_int;
                        trigger_in_progress <= '1';
                    -- trigger was sent on the previous clock cycle - add multiplicity
                    elsif (trigger_i_multiplied = '1') then
                        if (multiplicity_counter < std_logic_vector(to_unsigned(trigger_duration,multiplicity_counter'length))) then
                            multiplicity_counter <= multiplicity_counter + 1;
                            trigger_i_multiplied <= '1';
                            tdc_value_multiplied <= tdc_value_multiplied; -- dont change it
                        else
                            multiplicity_counter <= (others => '0');
                            trigger_i_multiplied <= '0';
                            tdc_value_multiplied <= (others => '0');
                            trigger_in_progress  <= '0';
                        end if;
                    end if;
                end if;        
            end if;
        end process;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    
    -- configure trigger_cmd sources
    trigger_cmd <= ttc_sig_i.l1a       when (trigger_source = SourceTTC)   else
                   tlu_data_i.trigger  when (trigger_source = SourceTLU)   else
                   ipbus_trigger       when (trigger_source = SourceIPBus) else
                   external_trigger    when (trigger_source = SourceExt)   else
                   testFSM_trigger or HitOr_trigger when (trigger_source = SourceTestPulse) else
                   HitOr_trigger       when (trigger_source = SourceHitOr)  else '0';
    trigger_cmd_40MHZ <= trigger_i_multiplied  when (clock_enable = '1') else '0';
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    
    -- configure tdc source
    Tdc_value_o   <= tdc_value_multiplied when clock_enable = '1' else (others => '0');
    tdc_value_int <= external_tdc    when (trigger_source = SourceExt)   else 
                     tlu_data_i.tdc  when (trigger_source = SourceTLU)   else
                     HitOR_tdc_value when (trigger_source = SourceHitOr) else (others=>'0');
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- configure glb_pulse_cmd source
    Glb_pulse_cmd_40MHz <= FreeRun_Glb_pulse when (AutoZero_source = FreeRun_AutoZero) else
                           TestFSM_Glb_pulse when (AutoZero_source = TestFSM_AutoZero) else
                           IPBus_Glb_pulse   when (AutoZero_source = IPBus_AutoZero)   else
                           '0'; 
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- configure Cal_cmd source
    Cal_cmd_40MHz <= cal_cmd_ipbus      when (trigger_source = SourceIPBus)     else
                     cal_cmd_testFSM    when (trigger_source = SourceTestPulse) else
                     '0'; 
    -- configure Cal_cmd data source
    Cal_data_40MHz <= Cal_data_ipbus    when (trigger_source = SourceIPBus)     else
                      Cal_data_Testfsm  when (trigger_source = SourceTestPulse) else
                      (others=>'0');
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- configure ECR_cmd & BCR_cmd source
    ECR_cmd_40MHz <= ipbus_ECR or testFSM_ECR or tlu_data_i.reset or main_fsm_reset or ttc_sig_i.reset;
    BCR_cmd_40MHz <= ipbus_BCR or ttc_sig_i.bcr;               
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Pack Fast Commands into Groups of 4 BX cycles --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- Sample commands @ rising_edge of clk_40MHz and store them in 4-bit buffers
    commands_at_40MHZ_process: process(reset_int,clk_40MHz)
    begin
        if (reset_int = '1') then
            Trigger_buf    <= (others=>'0');
            ECR_buf        <= (others=>'0');
            BCR_buf        <= (others=>'0');
            Cal_buf        <= (others=>'0');
            Glb_pulse_buf  <= (others=>'0');
            Veto_buf_40MHz <= (others=>'0'); 
        elsif falling_edge(clk_40MHz) then
            Trigger_buf    <= Trigger_buf(2 downto 0) & Trigger_cmd_40MHz;
            ECR_buf        <= ECR_buf(2 downto 0) & ECR_cmd_40MHz;
            BCR_buf        <= BCR_buf(2 downto 0) & BCR_cmd_40MHz;
            Cal_buf        <= Cal_buf(2 downto 0) & Cal_cmd_40MHz;
            Glb_pulse_buf  <= Glb_pulse_buf(2 downto 0) & Glb_pulse_cmd_40MHz;
            Veto_buf_40MHz <= Veto_buf_40MHz(2 downto 0) & trigger_cmd;
        end if;
    end process;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- Send them to PHY layer @ rising edge of clk_10MHz (commands are asserted until handshaked from PHY layer)
    commands_at_10MHZ_process: process(reset_int, clk_10MHz)
    begin
        if (reset_int = '1') then
            Trigger_cmd_10MHz    <= (others=>'0');
            Veto_buf_10MHz       <= (others=>'0');
            ECR_cmd_10MHz        <= '0';
            BCR_cmd_10MHz        <= '0';
            Cal_cmd_10MHz        <= '0';
            Glb_pulse_cmd_10MHz  <= '0';
            Cal_data_10MHz       <= (others=>'0');
            Glb_pulse_data_10MHz <= (others=>'0');
        elsif falling_edge(clk_10MHz) then
            Trigger_cmd_10MHz  <= Trigger_buf; -- no need to handshake, triggers have the highest priority and are always processed by the PHY layer
            Veto_buf_10MHz     <= Veto_buf_40MHz;
            
    --        if (cmd_done = '1') then -- if PHY layer is ready, send the commands currently stored in the buffers
            ECR_cmd_10MHz <= or_reduce(ECR_buf);
            BCR_cmd_10MHz <= or_reduce(BCR_buf);
    --        else                     -- if PHY layer is not ready, buffer commands for next clk cycle
    --            ECR_cmd_10MHz <= or_reduce(ECR_buf)or ECR_cmd_10MHz;
    --            BCR_cmd_10MHz <= or_reduce(BCR_buf)or BCR_cmd_10MHz;
    --        end if;
            
            if (Cal_done = '1') then
                Cal_cmd_10MHz  <= or_reduce(Cal_buf);
                Cal_data_10MHz <= Cal_data_40MHz;
            else
                Cal_cmd_10MHz  <= or_reduce(Cal_buf) or Cal_cmd_10MHz;
                Cal_data_10MHz <= Cal_data_10MHz;
            end if;
            
            if (Global_pulse_done = '1') then
                Glb_pulse_cmd_10MHZ  <= or_reduce(Glb_pulse_buf);
                Glb_pulse_data_10MHZ <= Global_pulse_data;
            else
                Glb_pulse_cmd_10MHZ  <= or_reduce(Glb_pulse_buf) or Glb_pulse_cmd_10MHz;
                Glb_pulse_data_10MHZ <= Glb_pulse_data_10MHz;
            end if;
        end if;
    end process;
    
    Trigger_cmd_reversed <= Trigger_cmd_10MHz(0) & Trigger_cmd_10MHz(1) & Trigger_cmd_10MHz(2) & Trigger_cmd_10MHz(3);
    Trigger_cmd_o        <= to_integer(unsigned(Trigger_cmd_reversed));
    ECR_cmd_o            <= ECR_cmd_10MHz;
    BCR_cmd_o            <= BCR_cmd_10MHz;
    Cal_data_o           <= Cal_data_10MHz       when (Cal_cmd_10MHz = '1')       else (others=>'0');
    Global_pulse_data_o  <= Glb_pulse_data_10MHz when (Glb_pulse_cmd_10MHz = '1') else (others=>'0');
    Triggers_lost_o      <= Trigger_cmd_10MHz xor Veto_buf_10MHz;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Set Trigger tag value -- 
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    trigger_tag_process: process(reset_int,clk_10MHz)
    begin
        if (reset_int = '1') then
            tag_cntr <= 0;
        elsif falling_edge(clk_10MHz) then
            if (or_reduce(ECR_buf)='1') or (or_reduce(BCR_buf)='1') then
                tag_cntr <= 0;
            elsif (tag_cntr = 31) then
                tag_cntr <= 0;
            elsif (or_reduce(Trigger_buf)='1') then
                tag_cntr <= tag_cntr +1;
            end if;
        end if;
    end process;
    --            
    Trigger_tag_o <= tag_cntr;
    

    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Trigger State Handling process --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    STATE_PROCESS: process (reset_int, clk_40MHz)
    begin
        if (reset_int = '1') then
           trigger_state               <= Idle;
           main_fsm_reset              <= '0';
           main_fsm_reset_timer        <= 0;
           stat_fastblock_o.error_code <= x"00";
           
        elsif rising_edge(clk_40MHz) then
            reset_counter  <= '0';
            main_fsm_reset <= '0';
            case trigger_state is
            
                -- Idle
                when Idle =>
                    status_state <= "00";
                    clock_enable <= '0';
                    if (trigger_start_loc = '1') then
                        stat_fastblock_o.error_code <= x"00";  
                        if (cnfg_fastblock_i.initial_fast_reset_enable = '1') then                 
                            main_fsm_reset       <= '1';
                            main_fsm_reset_timer <= 50;
                            trigger_state        <= WaitAfterReset;
                        else
                            reset_counter <= '1';
                            trigger_state <= Running;
                        end if;
                    end if;
                    
                -- Resetting    
                when WaitAfterReset =>
                    status_state <= "00";
                    if (main_fsm_reset_timer = 0) then
                        reset_counter <= '1';
                        trigger_state <= Running;
                    else
                        main_fsm_reset_timer <= main_fsm_reset_timer - 1;
                    end if;
                    
                -- Triggering
                when Running =>                
                    status_state <= "01";
                    
                    clock_enable <= '1';
                    if (trigger_stop_loc = '1') or (test_pulse_done = '1' and trigger_source = SourceTestPulse) then
                        clock_enable  <= '0';
                        trigger_state <= Idle;                
                    elsif ((triggers_to_accept /= 0) and TO_INTEGER(unsigned(trigger_counter)) >= triggers_to_accept) then
                        clock_enable  <= '0';
                        trigger_state <= Idle;
                    elsif ((backpressure_int = '1') or (AutoZero_trig_veto = '1')) then                
                        clock_enable  <= '0';
                        trigger_state <= WaitForReadout;
                    end if;
                    
                when WaitForReadout =>
                    status_state <= "10";                
                    clock_enable <= '0';
                    if (trigger_stop_loc = '1') then
                        trigger_state <= Idle;                
                    elsif ((backpressure_int = '0') and (AutoZero_trig_veto = '0')) then
                        trigger_state <= Running;
                    end if;
                                          
                when others =>
                    clock_enable  <= '0';
                    trigger_state <= Idle;
                    -- unknown state
                    stat_fastblock_o.error_code <= x"01";             
            end case;
        end if;
    end process;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Trigger Counter --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    COUNTER_PROCESS: process(clk_40MHz, reset_int, reset_counter)
    begin
        if ((reset_int = '1') or (reset_counter = '1')) then
            trigger_counter <= (others => '0');
        elsif rising_edge(clk_40MHz) then
            -- this counter counts ammount of triggers sent to the module
            if (clock_enable = '1') and ((trigger_i_multiplied = '1') or (Self_trigger_i = '1')) then
                trigger_counter <= trigger_counter + 1;   
            end if;         
        end if;
    end process;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    TRIGGER_IN_COUNTER_PROCESS: process(clk_40MHz, reset_int, reset_counter)
    begin
        if ((reset_int = '1') or (reset_counter = '1')) then
            trigger_in_counter <= (others => '0');
        elsif rising_edge(clk_40MHz) then
            -- this counter counts the full amount of triggers we get from the trigger source
            if ((trigger_state = Running) or (trigger_state = WaitForReadout)) then
                if (trigger_i_multiplied = '1') or (Self_trigger_i = '1') then 
                    trigger_in_counter <= trigger_in_counter + 1;
                end if;
            end if;
        end if;    
    end process;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    

    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- IPBus command handler (some configs and start stop reset) --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ipb_handler_inst: entity work.ipb_handler
    port map(
        reset_i                         => reset_int,
        clk_40MHz_i                     => clk_40MHz,
        -- ctrl fast block                 
        ctrl_fastblock_i                => ctrl_fastblock_i,
        cnfg_fastblock_i                => cnfg_fastblock_i,
        -- intput satte                    
        trigger_state_i                 => trigger_state,
        -- output configurations           
        triggers_to_accept_o            => triggers_to_accept,
        trigger_duration_o              => trigger_duration,
        trigger_source_o                => trigger_source,
        trigger_source_raw_o            => trigger_source_raw,
        user_trigger_frequency_o        => user_trigger_frequency,
        AutoZero_source_o               => AutoZero_source,
    --    Cal_Source_o                    => Cal_Source,
        Global_pulse_data_o             => Global_pulse_data,
        Cal_data_o                      => Cal_data_ipbus,
        
        -- output control                  
        ipb_reset_o                     => ipb_reset,
        configured_o                    => configured,
        start_trigger_o                 => trigger_start_loc,
        stop_trigger_o                  => trigger_stop_loc
    );
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Sending fast commands from the ipbus(configurable duration) --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    commands_from_ipbus_inst: entity work.commands_from_ipbus
        port map (
            reset_i             => reset_int,
            clk_40MHz_i         => clk_40MHz,
            -- ctrl fast block
            ctrl_fastblock_i    => ctrl_fastblock_i,
            -- output of fast commands from the ipbus
            ipb_fast_reset_o    => ipbus_ECR,  
            ipb_l1a_o           => ipbus_trigger,            
            ipb_test_pulse_o    => Cal_cmd_ipbus,     
            ipb_orbit_reset_o   => ipbus_BCR,
            ipb_glb_pulse_o     => IPBus_Glb_pulse   
        );
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Test Pulse Sending Logics ( for the test pulse fsm, which sends sequence ECR -> Global Pulse -> Cal pulse -> Trigger )
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    commands_from_calpulse_fsm_inst: entity work.commands_from_calpulse_fsm
        port map (
            reset_i                     => reset_int,
            clk_40MHz_i                 => clk_40MHz,
            clk_10MHz_i                 => clk_10MHz,
            -- ctrl fast block
            ctrl_fastblock_i            => ctrl_fastblock_i,
            cnfg_fastblock_i            => cnfg_fastblock_i,    
            -- iput configuration
            AutoZero_Source             => AutoZero_Source,
            TRIGGERS_TO_ACCEPT_I        => triggers_to_accept,
            Trigger_duration_i          => trigger_duration,         
            -- trigger state and sourc
            trigger_state_i             => trigger_state,           
            trigger_source_i            => trigger_source,                
            -- fast signal
            test_pulse_fast_reset_o     => testFSM_ECR,      
            test_pulse_l1a_o            => testFSM_trigger,      
            Cal_cmd_o                   => cal_cmd_testFSM,
            Cal_data_o                  => Cal_data_Testfsm,
            Global_pulse_cmd_o          => TestFSM_Glb_pulse,
            -- out signas
            test_pulse_done_o           => test_pulse_done
        );
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Triggering from external trigger
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    commands_from_ext_trigger_inst: entity work.commands_from_ext_trigger
        port map (
            reset_i                     => reset_int,
            clk_40MHz_i                 => clk_40MHz,
            clk_320MHz_i                => clk_320MHz,
            -- ctrl fast block
            ext_trigger_delay_value_i   => cnfg_fastblock_i.ext_trigger_delay_value,
            -- stubs input
            ext_trigger_i               => ext_trigger_i,      
            --
            trigger_duration_i          => trigger_duration,
            -- trigger state and sourc
            trigger_state_i             => trigger_state,           
            -- fast signal
            tdc_value_o                 => external_tdc,
            external_l1a_o              => external_trigger
        );
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Self-Triggering from Hit-Or functionality
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    commands_from_HitOr_inst: entity work.commands_from_HitOr
        Port map ( 
               reset_i           => reset_int,
               clk_40MHz_i       => clk_40MHz,
               clk_320MHz_i      => clk_320MHz,
               -- ctrl fast block
               ctrl_fastblock_i  => ctrl_fastblock_i,
               cnfg_fastblock_i  => cnfg_fastblock_i,
               -- trigger state and source
               trigger_state_i   => trigger_state,
               trigger_source_i  => trigger_source,
               -- Trigger from RD53A
               HitOr_i           => HitOr_i,
               --
               trigger_duration_i => trigger_duration,
               -- fast signals           
               HitOr_trigger_o   => HitOr_trigger,
               HitOr_tdc_value_o => HitOr_tdc_value
               );
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Auto-Zero process --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    AutoZero_process_inst: entity work.Auto_zeroing
        Port map ( 
               -- Reset signal
               Reset                => reset_int,
               -- Input clk
               clk_10MHz            => clk_10MHz,
               clk_40MHz            => clk_40MHz,
    --           Global_pulse_sent    => Global_pulse_done,
               -- Configuration signal
               AutoZero_Source      => AutoZero_Source,
               Trigger_state_i      => trigger_state, 
               cnfg_fastblock_i     => cnfg_fastblock_i, 
               -- Outputs to fast comm
               AutoZero_trig_veto_o => AutoZero_trig_veto_i, 
               Global_pulse_cmd_o   => FreeRun_glb_pulse 
               );
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        --clk_divider: entity work.clock_divider -- the entity which produces the internal trigger as specified rate
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    --port map
    --(
    --    i_clk           => clk_40MHz,
    --    i_rst           => reset_int,
    --    i_clk_frequency => user_trigger_frequency,
    --    o_clk           => user_trigger
    --);             
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


--==================================================================================================
-- End of Main Body
--==================================================================================================

end rtl;
