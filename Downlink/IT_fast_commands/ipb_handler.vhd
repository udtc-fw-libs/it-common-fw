----------------------------------------------------------------------------------
-- Engineer: Yiannis Kazas (Original file by Mykyta Haranko)
-- Create Date: 02/22/2018 11:28:16 AM
----------------------------------------------------------------------------------
-- handles the ipbus control and config signals
----------------------------------------------------------------------------------
-- Last Changes : 22-May-2019 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use IEEE.NUMERIC_STD.ALL;
use work.user_package.all;

entity ipb_handler is
Port ( 
    reset_i                         : in std_logic;
    clk_40MHz_i                     : in std_logic;
    -- ctrl fast block
    ctrl_fastblock_i                : in ctrl_fastblock_type;
    cnfg_fastblock_i                : in cnfg_fastblock_type;
    -- intput satte
    trigger_state_i                 : in trigger_state_type;
    -- output configurations
    triggers_to_accept_o            : out integer range 0 to MAX_NTRIGGERS_TO_ACCEPT;
    trigger_duration_o              : out integer range 0 to 128;
    trigger_source_o                : out trigger_source_type;
    trigger_source_raw_o            : out std_logic_vector(3 downto 0);
    user_trigger_frequency_o        : out integer range 1 to MAX_USER_TRIGGER_FREQUENCY := 1;
    AutoZero_source_o               : out AutoZero_source_type;
--    Cal_source_o                    : out Cal_source_type; 
    Global_pulse_data_o             : out std_logic_vector(9 downto 0);
    Cal_data_o                      : out std_logic_vector(19 downto 0);
    -- output control
    ipb_reset_o                     : out std_logic;
    configured_o                    : out std_logic;
    start_trigger_o                 : out std_logic;
    stop_trigger_o                  : out std_logic
);
end ipb_handler;

architecture rtl of ipb_handler is

    -- trigger source std logic vector
    signal trigger_source                : std_logic_vector(3 downto 0) := x"1";
    signal AutoZero_source               : std_logic_vector(1 downto 0) := "00";
--    signal cal_source                    : std_logic_vector(1 downto 0) := "00";
    
    -- temporary configuration signals
    signal temp_trigger_source           : std_logic_vector(3 downto 0) := x"1";
    signal temp_autozero_source          : std_logic_vector(1 downto 0) := "00";
--    signal temp_cal_source               : std_logic_vector(1 downto 0) := "00";
    signal temp_user_trigger_frequency   : integer range 1 to MAX_USER_TRIGGER_FREQUENCY := 1;    
    signal temp_triggers_to_accept       : integer range 0 to MAX_NTRIGGERS_TO_ACCEPT := 0;   
    signal temp_global_pulse_data        : std_logic_vector(9 downto 0) := (others=>'0');
    signal temp_cal_data                 : std_logic_vector(19 downto 0) := (others=>'0');
    
    -- john debug --
    signal arm_strobe : std_logic:='1';
--    signal start_trigger : std_logic:='0';
--    signal start_trigger_buf : std_logic:='0';
--    signal stop_trigger  : std_logic:='0';
    
    signal temp_trigger_duration : integer range 0 to 128;
    
    signal cmd_strobe_pre_pre_sync    : std_logic:='0';
    signal cmd_strobe_pre_sync        : std_logic:='0';
    signal cmd_strobe_sync            : std_logic:='0';
    signal cmd_strobe                 : std_logic:='0';
    signal start_trigger_pre_pre_sync : std_logic:='0';
    signal start_trigger_pre_sync     : std_logic:='0';
    signal start_trigger_sync         : std_logic:='0';
    signal start_trigger              : std_logic:='0';
    signal load_config_pre_pre_sync   : std_logic:='0';
    signal load_config_pre_sync       : std_logic:='0';
    signal load_config_sync           : std_logic:='0';
    signal load_config                : std_logic:='0';
    signal stop_trigger_pre_pre_sync  : std_logic:='0';
    signal stop_trigger_pre_sync      : std_logic:='0';
    signal stop_trigger_sync          : std_logic:='0';
    signal stop_trigger               : std_logic:='0';
    signal reset_pre_pre_sync         : std_logic:='0';
    signal reset_pre_sync             : std_logic:='0';
    signal reset_sync                 : std_logic:='0';
    signal reset                      : std_logic:='0';
    
    
    
--------------------------------------------------------------
-- Debug Core --
--------------------------------------------------------------    
--COMPONENT ipb_handler_debug
--    PORT (
--        clk : IN STD_LOGIC;
--        probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
--        probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
--        probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
--        probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
--        probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--        probe5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--        probe6 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
--    );
--    END COMPONENT  ;
    
    
        signal probe0 : STD_LOGIC_VECTOR(0 DOWNTO 0); 
        signal probe1 : STD_LOGIC_VECTOR(0 DOWNTO 0); 
        signal probe2 : STD_LOGIC_VECTOR(0 DOWNTO 0); 
        signal probe3 : STD_LOGIC_VECTOR(0 DOWNTO 0); 
        signal probe4 : STD_LOGIC_VECTOR(0 DOWNTO 0);
        signal probe5 : STD_LOGIC_VECTOR(0 DOWNTO 0);
        signal probe6 : STD_LOGIC_VECTOR(0 DOWNTO 0);
--------------------------------------------------------------
    
begin

--------------------------------------------------------------

    probe0(0) <= start_trigger;
    probe1(0) <= arm_strobe;
    probe2(0) <= ctrl_fastblock_i.start_trigger;
    probe3(0) <= ctrl_fastblock_i.cmd_strobe;
    probe4(0) <= ctrl_fastblock_i.reset;
    probe5(0) <= ctrl_fastblock_i.load_config;
    probe6(0) <= reset_i;
    
--    Debug_ipbhandler : ipb_handler_debug
--    PORT MAP (
--        clk => clk_40MHz_i,
--        probe0 => probe0, 
--        probe1 => probe1, 
--        probe2 => probe2, 
--        probe3 => probe3, 
--        probe4 => probe4,
--        probe5 => probe5,
--        probe6 => probe6
--    );
    --------------------------------------------------------------
    --------------------------------------------------------------    
    
    
    
    --debugging
--    start_trigger_o <= start_trigger or start_trigger_buf;
--    start_trigger_o <= start_trigger;

    --==============================================================--
    -- assign the trigger source
    --==============================================================--
    trigger_source_o <= SourceIPBus         when trigger_source = x"1" else
                        SourceTestPulse     when trigger_source = x"2" else
                        SourceTTC           when trigger_source = x"3" else
                        SourceTLU           when trigger_source = x"4" else
                        SourceExt           when trigger_source = x"5" else
                        SourceHitOr         when trigger_source = x"6" else
                        SourceUser          when trigger_source = x"7" else
                        SourceUndefined;
    trigger_source_raw_o <= trigger_source;
    --==============================================================--
    
    --==============================================================--
    -- assign auto-zero source
    --==============================================================--
    AutoZero_source_o <= IPBus_AutoZero     when AutoZero_source = "01" else
                         TestFSM_AutoZero   when AutoZero_source = "10" else
                         FreeRun_AutoZero   when AutoZero_source = "11" else
                         Undefined_AutoZero;
    --==============================================================--
    
--    --==============================================================--
--    -- assign Cal cmd source
--    --==============================================================--
--    Cal_source_o <= Cal_source_ipbus   when Cal_source = "01" else
--                    Cal_source_testFSM when Cal_source = "10" else
--                    Cal_source_undefined;
--    --==============================================================--

    process(clk_40MHz_i)
    begin
        if rising_edge(clk_40MHz_i) then
            cmd_strobe_pre_pre_sync    <= ctrl_fastblock_i.cmd_strobe;
            cmd_strobe_pre_sync        <= cmd_strobe_pre_pre_sync;
            cmd_strobe_sync            <= cmd_strobe_pre_sync;
            cmd_strobe                 <= cmd_strobe_sync;
            
            start_trigger_pre_pre_sync <= ctrl_fastblock_i.start_trigger;
            start_trigger_pre_sync     <= start_trigger_pre_pre_sync;
            start_trigger_sync         <= start_trigger_pre_sync;
            start_trigger              <= start_trigger_sync;
            
            stop_trigger_pre_pre_sync <= ctrl_fastblock_i.stop_trigger;
            stop_trigger_pre_sync     <= stop_trigger_pre_pre_sync;
            stop_trigger_sync         <= stop_trigger_pre_sync;
            stop_trigger              <= stop_trigger_sync;
            
            reset_pre_pre_sync        <= ctrl_fastblock_i.reset;
            reset_pre_sync            <= reset_pre_pre_sync;
            reset_sync                <= reset_pre_sync;
            reset                     <= reset_sync;
            
            load_config_pre_pre_sync <= ctrl_fastblock_i.load_config;
            load_config_pre_sync     <= load_config_pre_pre_sync;
            load_config_sync         <= load_config_pre_sync;
            load_config              <= load_config_sync;
            
            
            
        end if;
    end process;


    --==============================================================--
    -- IPBus config handler
    --==============================================================--
    COMMAND_HANDLER: process (reset_i, clk_40MHz_i)
    begin
        if (reset_i = '1') then
            ipb_reset_o                 <= '0';
            start_trigger_o             <= '0'; 
            stop_trigger_o              <= '0';
            
            temp_trigger_source         <= x"1";
            temp_autozero_source        <= "00";
--            temp_cal_source             <= "00";
            temp_user_trigger_frequency <= 1;
            temp_trigger_duration       <= 0;
            temp_triggers_to_accept     <= 0;
            temp_global_pulse_data      <= (others=>'0');
            temp_cal_data               <= (others=>'0');

            trigger_source              <= x"1";
            autozero_source             <= "00";
--            cal_source                  <= "00";

            user_trigger_frequency_o    <= 1;
            trigger_duration_o          <= 0;
            triggers_to_accept_o        <= 0;
            Global_pulse_data_o         <= (others=>'0');           
            
            configured_o                <= '0';
            
            arm_strobe <= '1';
            
        elsif rising_edge(clk_40MHz_i) then
            ipb_reset_o     <= '0';
            start_trigger_o <= '0';
            stop_trigger_o  <= '0';
            if (trigger_state_i = Idle) then            
                trigger_source           <= temp_trigger_source;
                autozero_source          <= temp_autozero_source;
--                cal_source               <= temp_cal_source;
                user_trigger_frequency_o <= temp_user_trigger_frequency;
                trigger_duration_o       <= temp_trigger_duration;         
--                triggers_to_accept_o     <= temp_triggers_to_accept;
                Global_pulse_data_o      <= temp_global_pulse_data;
                Cal_data_o               <= temp_cal_data;
                
                -- When Test pulse fsm is selected as trigger source, up to 32 consecutive trigger pulses can be send in one fsm sequence
--                if (temp_trigger_source = x"2") or (temp_trigger_source = x"4") or (temp_trigger_source = x"5") or (temp_trigger_source = x"6") then
                    triggers_to_accept_o <= temp_triggers_to_accept * (temp_trigger_duration + 1);
--                else
--                    triggers_to_accept_o <= temp_triggers_to_accept;
--                end if; 
                
            end if;
            
            -- deactivate cmd strobe after one clk cycle. activate again when it is deasserted
            if (cmd_strobe = '1') then
                arm_strobe <= '0';
            else
                arm_strobe <= '1';
            end if;
            
            if (cmd_strobe = '1') and (arm_strobe = '1') then
                if (reset = '1') then
                    ipb_reset_o <= '1';
                elsif (load_config = '1') then                
                    if (trigger_state_i /= Idle) then
                        stop_trigger_o <= '1';
                    end if;
                    --needs to be buffered and changed after trigger was stopped
                    temp_trigger_source         <= cnfg_fastblock_i.trigger_source;
                    temp_autozero_source        <= cnfg_fastblock_i.autozero_source;
--                    temp_cal_source             <= cnfg_fastblock_i.cal_source;
                    temp_user_trigger_frequency <= cnfg_fastblock_i.user_trigger_frequency;
                    temp_trigger_duration       <= cnfg_fastblock_i.trigger_duration;
                    temp_triggers_to_accept     <= cnfg_fastblock_i.triggers_to_accept; 
                    temp_global_pulse_data      <= cnfg_fastblock_i.global_pulse_data;
                    temp_cal_data               <= cnfg_fastblock_i.cal_data_prime;                  
                    configured_o                <= '1';
                elsif (start_trigger = '1') then
                    start_trigger_o <= '1'; 
                elsif (stop_trigger = '1') then
                    stop_trigger_o <= '1';
                end if;
            end if;
        end if;
    end process;

end rtl;
