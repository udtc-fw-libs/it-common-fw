----------------------------------------------------------------------------------
-- Engineer: Yiannis Kazas (original file by Mykyta Haranko)
-- Create Date: 20/08/2018 11:28:16 AM
----------------------------------------------------------------------------------
-- test pulse fsm which sends the sequence of ecr, auto-zero, cal pulse, trigger and repeats
-- all the signals can be disabled
----------------------------------------------------------------------------------
-- Last Changes: 6-May-2021
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use IEEE.NUMERIC_STD.ALL;
use work.user_package.all;

------------------------------------------------------------------------
entity commands_from_calpulse_fsm is
Port (
    reset_i                 : in std_logic;
    clk_40MHz_i             : in std_logic;
    -- john debug
    clk_10MHz_i             : in std_logic;
    -- ctrl fast block
    ctrl_fastblock_i        : in ctrl_fastblock_type;
    cnfg_fastblock_i        : in cnfg_fastblock_type;
    -- ipbus configuration
    AutoZero_Source         : in AutoZero_Source_type;
    triggers_to_accept_i    : in integer range 0 to MAX_NTRIGGERS_TO_ACCEPT;
    trigger_duration_i      : in integer range 0 to 31;
    -- trigger state and source
    trigger_state_i         : in trigger_state_type;
    trigger_source_i        : in trigger_source_type;
    -- fast signal
    test_pulse_fast_reset_o : out std_logic; --ECR command
    test_pulse_l1a_o        : out std_logic;
    Cal_cmd_o               : out std_logic;
    Cal_data_o              : out std_logic_vector(19 downto 0);
    Global_pulse_cmd_o      : out std_logic;
    -- out signas
    test_pulse_done_o       : out std_logic
);
end commands_from_calpulse_fsm;
-------------------------------------------------------------------

architecture rtl of commands_from_calpulse_fsm is

--======================================================================
-- Start of Signal Declaration
--======================================================================
    -- fsm state
    type test_pulse_fsm_state_type is (IdleState, FAST_RESET_STATE, AUTO_ZERO_STATE, TestPulseInitPrime, TEST_PULSE_PRIME_STATE, TEST_PULSE_INJECT_STATE, L1_TRIGGER_STATE, WAIT_STATE, WAIT_DONE_ACK_STATE);
    signal test_pulse_fsm_state     : test_pulse_fsm_state_type := IdleState;
    
    -- configuration
    signal Trigger_duration         : integer range 0 to 31:= 0;
    signal Trigger_timer            : integer range 0 to 31:= 0;
    signal Delay_after_fast_reset   : natural := 40;
    signal Delay_after_AutoZero     : natural := 40;
    signal Delay_after_init_prime   : natural := 200;
    signal Delay_after_prime_pulse  : natural := 200;
    signal Delay_after_inject_pulse : natural := 200;
    signal Delay_before_next_pulse  : natural := 400;
    signal Cal_data_prime           : std_logic_vector(19 downto 0):=(others=>'0');
    signal Cal_data_inject          : std_logic_vector(19 downto 0):=(others=>'0');
    signal test_pulse_fsm_signals_enable : test_pulse_fsm_signals_enable_type;
    
    -- timer and counters
    signal test_pulse_timer         : natural := 0;
    signal test_pulse_counter       : natural := 0;
    -- fast signals
    signal test_pulse_fast_reset    : std_logic := '0';
    signal TestFSM_Glb_pulse        : std_logic := '0';
    signal test_pulse_l1a           : std_logic := '0';
    signal test_pulse_req           : std_logic := '0';
    
    -- done state
    signal test_pulse_done          : std_logic := '0';
    
    -- 
    signal clk_10MHz_toggle : std_logic:='0';
    signal clk_10MHz_prev   : std_logic:='0';
    signal clk10_edge       : std_logic:='0'; 
    signal clk10_edge_dly_1 : std_logic:='0'; 
    signal clk10_edge_dly_2 : std_logic:='0'; 
    signal clk10_edge_dly_3 : std_logic:='0'; 
    -- john debug --
    signal state : std_logic_vector(2 downto 0);
    ----------------
    
--======================================================================
-- End of Signal Declaration
--======================================================================

------------------------------------------------------------------------
-- DEBUG CORE --
------------------------------------------------------------------------
COMPONENT TP_FMS_DEBUG

PORT (
	clk : IN STD_LOGIC;
	probe0 : IN STD_LOGIC_VECTOR(2 DOWNTO 0); 
	probe1 : IN STD_LOGIC_VECTOR(11 DOWNTO 0); 
	probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
	probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
	probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
	probe5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
	probe6 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
	probe7 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
	probe8 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
	probe9 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
	probe10 : IN STD_LOGIC_VECTOR(4 DOWNTO 0)
);
END COMPONENT  ;

signal trigger_state_debug : std_logic_vector(1 downto 0) := "00";
----------------------------------------------------------------------

begin


------------------------------------------------------------------------
trigger_state_debug <= "00" when (trigger_state_i = Idle) else "01" when (trigger_state_i = WaitAfterReset) else "10" when (trigger_state_i = Running) else "11";

--DEBUG_TP_FSM : TP_FMS_DEBUG
--PORT MAP (
--	clk       => clk_40MHz_i,
--	probe0    => state, 
--	probe1    => std_logic_vector(to_unsigned(test_pulse_timer,12)), 
--	probe2(0) => trigger_state_debug(0), 
--	probe3(0) => trigger_state_debug(1), 
--	probe4(0) => Test_pulse_fsm_signals_enable.l1a_en, 
--	probe5(0) => Test_pulse_fsm_signals_enable.inject_pulse_en, 
--	probe6(0) => test_pulse_req, 
--	probe7(0) => test_pulse_fast_reset, 
--	probe8(0) => test_pulse_l1a, 
--	probe9(0) => test_pulse_done,
--	probe10   => std_logic_vector(to_unsigned(trigger_timer,5))
--);
------------------------------------------------------------------------
-- END DEBUG CORE --
------------------------------------------------------------------------



--======================================================================
-- Start of Main Body
--======================================================================
------------------------------------------------------------------------
-- Configure Outputs 
------------------------------------------------------------------------
    test_pulse_done_o       <= test_pulse_done;
    test_pulse_fast_reset_o <= test_pulse_fast_reset; 
    test_pulse_l1a_o        <= test_pulse_l1a;        
    Cal_cmd_o               <= test_pulse_req; 
    Global_pulse_cmd_o      <= TestFSM_Glb_pulse;         
------------------------------------------------------------------------
------------------------------------------------------------------------


------------------------------------------------------------------------
-- Read signals from IPBus registers
------------------------------------------------------------------------
    process (clk_40MHz_i)
    begin
        if rising_edge(clk_40MHz_i) then
            if (trigger_state_i = Idle) then
                Delay_after_fast_reset        <= ((cnfg_fastblock_i.delay_after_fast_reset + 1) * 4) - 1;
                Delay_after_AutoZero          <= ((cnfg_fastblock_i.delay_after_autozero +1) * 4) + 3;
                Delay_after_init_prime        <= ((cnfg_fastblock_i.delay_after_first_prime + 1) * 4) + 7;
                Delay_after_prime_pulse       <= ((cnfg_fastblock_i.delay_after_prime_pulse + 1) * 4) + 7;
                Delay_after_inject_pulse      <= ((cnfg_fastblock_i.delay_after_inject_pulse +1) * 4) + 7;
                Delay_before_next_pulse       <= ((cnfg_fastblock_i.delay_before_next_pulse +1) * 4) - 1;
                Cal_data_prime                <= cnfg_fastblock_i.cal_data_prime;
                Cal_data_inject               <= cnfg_fastblock_i.cal_data_inject;
                Test_pulse_fsm_signals_enable <= cnfg_fastblock_i.test_pulse_fsm_signals_enable;
                Trigger_duration              <= trigger_duration_i;
            end if;            
        end if;
    end process;       
------------------------------------------------------------------------
------------------------------------------------------------------------

------------------------------------------------------------------------
-- Sync start of Test-Pulse FSM with 10MHz clock
------------------------------------------------------------------------
-- create a toggle flip-flop from 10MHz clock
process(reset_i, clk_10MHz_i)
begin
    if (reset_i = '1') then
        clk_10MHz_toggle <= '0';
    elsif rising_edge(clk_10MHz_i) then
        clk_10MHz_toggle <= not clk_10MHz_toggle;
    end if;
end process;

-- identify rising edge of 10MHz clock and start fsm 1 pulse after that
process(reset_i, clk_40MHz_i)
begin
    if (reset_i = '1') then
        clk_10MHz_prev <= '0';
        clk10_edge_dly_1 <= '0';
        clk10_edge_dly_2 <= '0';
        clk10_edge_dly_3 <= '0';
        clk10_edge       <= '0';
    elsif rising_edge(clk_40MHz_i) then
        clk_10MHz_prev <= clk_10MHz_toggle;
        if (clk_10MHz_prev /= clk_10MHz_toggle) then
            clk10_edge_dly_1 <= '1';
        else
            clk10_edge_dly_1 <= '0';
        end if;
        clk10_edge_dly_2 <= clk10_edge_dly_1;
        clk10_edge_dly_3 <= clk10_edge_dly_2;
        clk10_edge       <= clk10_edge_dly_3;
    end if;
end process;
------------------------------------------------------------------------
------------------------------------------------------------------------

------------------------------------------------------------------------
-- Main FSM
------------------------------------------------------------------------
    -- send test pulses
    process (clk_40MHz_i)
    begin
        if rising_edge(clk_40MHz_i) then
            if reset_i = '1' or ctrl_fastblock_i.reset_test_pulse = '1' then
                test_pulse_req        <= '0';
                test_pulse_fast_reset <= '0';
                test_pulse_l1a        <= '0';
                testFSM_glb_pulse     <= '0';
                test_pulse_timer      <= 0;
                test_pulse_counter    <= 0;
                test_pulse_done       <= '0';
                test_pulse_fsm_state  <= IdleState;
            else        
                test_pulse_req        <= '0';
                test_pulse_fast_reset <= '0';
                test_pulse_l1a        <= '0';
                testFSM_glb_pulse     <= '0';
                
                case test_pulse_fsm_state is
                
                    when IdleState =>
                    state <= "000";
                        test_pulse_counter <= 0;
                        test_pulse_timer   <= 0;
                        test_pulse_done    <= '0';
                        trigger_timer      <= trigger_duration;
                        
                        if (trigger_state_i = Running) and (trigger_source_i = SourceTestPulse) then
                            if (clk10_edge = '1') then -- synchronize with 10MHz clock
                                -- else go to the test pulse init-prime state if it is enabled
                                if (Test_pulse_fsm_signals_enable.test_pulse_en = '1') then
                                    test_pulse_fsm_state <= TestPulseInitPrime;
                                -- else go to the auto-zero state if it is enabled
                                elsif (AutoZero_Source = TestFSM_AutoZero) then
                                    test_pulse_fsm_state <= AUTO_ZERO_STATE;
                                -- go to the fast reset state if it is enabled
                                elsif (Test_pulse_fsm_signals_enable.fast_reset_en = '1') then
                                    test_pulse_fsm_state <= FAST_RESET_STATE;
                                -- go to the inject pulse state, if it is enabled
                                elsif (Test_pulse_fsm_signals_enable.inject_pulse_en = '1') then
                                    test_pulse_fsm_state <= TEST_PULSE_INJECT_STATE;
                                -- else go to the l1a, if it is enabled
                                elsif (Test_pulse_fsm_signals_enable.l1a_en = '1') then
                                    test_pulse_fsm_state <= L1_TRIGGER_STATE;
                                -- do nothing if all the signals are disabled
                                else
                                    null;
                                end if;
                            else
                                test_pulse_fsm_state <= IdleState;
                            end if;        
                        end if;
                    
                    -- Executed only once at the beggining    
                    when TestPulseInitPrime =>
                        state <= "001";
                            test_pulse_timer <= DELAY_AFTER_INIT_PRIME;
                            test_pulse_req   <= '1';
                            Cal_data_o       <= Cal_data_prime;
                            -- go to the autozero state, if it is enabled
                            if (AutoZero_Source = TestFSM_AutoZero) then
                                test_pulse_fsm_state <= AUTO_ZERO_STATE;
                            -- go to the reset state, if it is enabled
                            elsif (Test_pulse_fsm_signals_enable.fast_reset_en = '1') then
                                test_pulse_fsm_state <= FAST_RESET_STATE;
                            -- go to the inject pulse state, if it is enabled
                            elsif (Test_pulse_fsm_signals_enable.inject_pulse_en = '1') then
                                test_pulse_fsm_state <= TEST_PULSE_INJECT_STATE;
                            -- else go to the l1a, if it is enabled
                            elsif (Test_pulse_fsm_signals_enable.l1a_en = '1') then
                                test_pulse_fsm_state <= L1_TRIGGER_STATE;
                            -- else modify the wait length and wait till the next test pulse
                            else
                                test_pulse_timer     <= Delay_before_next_pulse;
                                test_pulse_fsm_state <= WAIT_STATE;
                                test_pulse_counter   <= test_pulse_counter + 1;
                            end if;
                            
                    
                    -- 1st State of FSM loop
                    when AUTO_ZERO_STATE =>
                    state <= "010";
                        if (test_pulse_timer = 0) then 
                            TestFSM_Glb_pulse <= '1';
                            test_pulse_timer  <= Delay_after_AutoZero;
                            -- go to the reset state, if it is enabled
                            if (Test_pulse_fsm_signals_enable.fast_reset_en = '1') then
                                test_pulse_fsm_state <= FAST_RESET_STATE;
                            elsif (test_pulse_fsm_signals_enable.inject_pulse_en = '1') then
                                test_pulse_fsm_state <= TEST_PULSE_INJECT_STATE;
                            elsif (test_pulse_fsm_signals_enable.l1a_en = '1') then
                                test_pulse_fsm_state <= L1_TRIGGER_STATE;
                            else
                               test_pulse_timer     <= Delay_before_next_pulse;
                               test_pulse_fsm_state <= WAIT_STATE;
                               test_pulse_counter   <= test_pulse_counter + 1;
                            end if;
                        else
                            test_pulse_timer <= test_pulse_timer - 1;
                        end if;
                        
                    -- 2nd State of FSM loop
                    when FAST_RESET_STATE =>  
                    state <= "011";
                        if (test_pulse_timer = 0) then 
                            test_pulse_fast_reset <= '1';
                            test_pulse_timer      <= DELAY_AFTER_FAST_RESET;
                            -- go to the test pulse state, if it is enabled
                            if (Test_pulse_fsm_signals_enable.inject_pulse_en = '1') then
                                test_pulse_fsm_state <= TEST_PULSE_INJECT_STATE;
                            -- else go to the l1a, if it is enabled
                            elsif (Test_pulse_fsm_signals_enable.l1a_en = '1') then
                                test_pulse_fsm_state <= L1_TRIGGER_STATE;
                            -- else modify the wait length and wait till the next test pulse
                            else
                                test_pulse_timer     <= Delay_before_next_pulse;
                                test_pulse_fsm_state <= WAIT_STATE;
                                test_pulse_counter   <= test_pulse_counter + 1;
                            end if;
                        else
                            test_pulse_timer <= test_pulse_timer - 1;
                        end if;
                    
                    -- 3rd State of FSM loop                            
                    when TEST_PULSE_INJECT_STATE =>
                    state <= "100";
                        if (test_pulse_timer = 0) then
                            test_pulse_req   <= '1';
                            Cal_data_o       <= Cal_data_inject;
                            test_pulse_timer <= Delay_after_inject_pulse;
                            -- go to the l1a, if it is enabled
                            if (test_pulse_fsm_signals_enable.l1a_en = '1') then
                                test_pulse_fsm_state <= L1_TRIGGER_STATE;
                            elsif (test_pulse_fsm_signals_enable.test_pulse_en = '1') then
                                test_pulse_fsm_state <= TEST_PULSE_PRIME_STATE;
                            -- else modify the wait length and wait till the next test pulse
                            else
                                test_pulse_timer     <= Delay_before_next_pulse;
                                test_pulse_fsm_state <= WAIT_STATE;
                                test_pulse_counter   <= test_pulse_counter + 1;
                            end if;
                        else
                            test_pulse_timer <= test_pulse_timer - 1;
                        end if;
                    
                    -- 4th State of FSM loop    
                    when L1_TRIGGER_STATE =>
                    state <= "101";
                        if (test_pulse_timer = 0) then
                            test_pulse_l1a   <= '1';                        
                            test_pulse_timer <= Delay_before_next_pulse;
                            -- go to prime pulse state, if enabled
                            if (test_pulse_fsm_signals_enable.test_pulse_en = '1') then
                                test_pulse_fsm_state <= TEST_PULSE_PRIME_STATE;
                            else
                                test_pulse_fsm_state <= WAIT_STATE;
                                test_pulse_counter   <= test_pulse_counter + 1;
                            end if;
                        else
                            test_pulse_timer <= test_pulse_timer - 1;
                        end if;
                    
                    -- 5th State of FSM loop    
                    when TEST_PULSE_PRIME_STATE =>
                        state <= "110";
                            if (test_pulse_timer = 0) then
                                test_pulse_req       <= '1';
                                Cal_data_o           <= Cal_data_prime;
                                test_pulse_timer     <= Delay_after_prime_pulse;
--                                test_pulse_counter   <= test_pulse_counter + 1;
                                test_pulse_fsm_state <= WAIT_STATE;
                                test_pulse_counter   <= test_pulse_counter + 1;
                            else
                                test_pulse_timer <= test_pulse_timer - 1;
                            end if;
                        
                    when WAIT_STATE =>
                    state <= "111";
                        if (test_pulse_timer = 0) then
--                            test_pulse_counter <= test_pulse_counter + 1;
                            if ((triggers_to_accept_i /= 0) and (test_pulse_counter >= triggers_to_accept_i)) or (trigger_state_i /= Running) then
                                test_pulse_done      <= '1';
                                test_pulse_fsm_state <= WAIT_DONE_ACK_STATE;
                            -- here the conditions are the same as for the IdleState
                            else
                                if (clk10_edge = '1') then -- synchronize with 10MHz clock
                                    -- go to the auto-zero state if it is enabled
                                    if (AutoZero_Source = TestFSM_AutoZero) then
                                        test_pulse_fsm_state <= AUTO_ZERO_STATE;
                                    -- else go to the fast reset state if it is enabled
                                    elsif (Test_pulse_fsm_signals_enable.fast_reset_en = '1') then
                                        test_pulse_fsm_state <= FAST_RESET_STATE;
                                    -- else go to the test pulse state if it is enabled
                                    elsif (Test_pulse_fsm_signals_enable.inject_pulse_en = '1') then
                                        test_pulse_fsm_state <= TEST_PULSE_INJECT_STATE;
                                    -- else go to the l1a, if it is enabled
                                    elsif (Test_pulse_fsm_signals_enable.l1a_en = '1') then
                                        test_pulse_fsm_state <= L1_TRIGGER_STATE;
                                    -- do nothing if all the signals are disabled
                                    else
                                        test_pulse_fsm_state <= IdleState;
--                                        null;
                                    end if;
                                else
                                    test_pulse_fsm_state <= WAIT_STATE;
                                end if;        
                            end if;
                        else
                            test_pulse_timer <= test_pulse_timer - 1;
                        end if;
                        
                    when WAIT_DONE_ACK_STATE =>
                    state <= "010";
                        if (trigger_state_i /= Running) then
                            test_pulse_done      <= '0';
                            test_pulse_fsm_state <= IdleState;
                        end if;
                        
                    when others =>
                        test_pulse_fsm_state <= IdleState;
                end case;
            end if;
        end if;
    end process;
------------------------------------------------------------------------
------------------------------------------------------------------------
--======================================================================
-- End of Main Body
--======================================================================

end rtl;
