# Fast Commands Block
Fast Commands block receives (or generates internally) Trigger, ECR, BCR, Global Pulse & Calibration Injection commands.
They are packed into groups of 4 BX cycles and forwarded to the CMD Controller.
Commands are sampled from several sources on the rising edge of clk_40MHz and stored in 4-bit buffers on the falling edge of clk_40MHz.
Outputs to the CMD Controller are set on the falling edge of clk_10MHz and sampled by the CMD Controller on the rising edge of clk_10MHz.

## Ports

**INPUTS**
* **clk_10MHz, clk_40MHz, clk_320MHz, rst:** Clock and reset signals.
* **Ctrl_fastblock_i**(Ctrl_fastblock_type): Control signals from IPBus. 
* **Cnfg_fastblock_i**(Cnfg_fastblock_type): Configuration signals from IPBus .
* **TTC_sig_i:** Decoded signals from TTC.
* **TLU_data_i:** Signals from TLU.
* **Ext_trigger_i:** External trigger signal.
* **Backpressure_i:** Backpressure signal from readout.
* **Cmd_done, Cal_done, Global_pulse_done:** Handshake signals from CMD controller for fast commands. 
* **HitOr_i(3:0):** HitOr lanes from chip.

**OUTPUTS**
* **Stat_fastblock_type** (stat_fastblock_type): Status signals to IPBus.
* **Trigger_cmd_o** (integer range 0 to 15): Trigger command.
* **Trigger_tag** (integer range 0 to 31): Trigger tag.
* **ECR_cmd_o, BCR_cmd_o:** ECR & BCR commands.
* **Cal_data_o** std_logic_vector(19 downto 0): Data word for calibration injection command.
* **Global_pulse_data_o** std_logic_vector(9 downto 0): Data word for Global Pulse command.
* **Triggers_lost_o** std_logic_vector(3 downto 0): Buffer showing which triggers have been vetoed in the last 10MHz clock cycle.


## List of IPBus Register

**CTRL_FASTBLOCK_TYPE,** Control signals from IPBus
* **cmd_strobe:**         Command strobe 
* **reset:**              Reset signal for fast commands block.
* **load_config:**        Signal used with command strobe to load configuration settings from IPBus registers.
* **start_trigger:**      Start triggering process.
* **stop_trigger:**       Stop triggering process.
* **reset_test_pulse:**   Reset Test-pulse FSM.
* **ipb_fast_reset:**     ECR command from IPBus.
* **ipb_orbit_reset:**	  BCR command from IPBus.
* **ipb_test_pulse:**     Cal command from IPBus.
* **ipb_trigger:**        Trigger command from IPBus.
* **ipb_glb_pulse:**      Global Pulse command from IPBus.
* **ipb_fast_duration:**  Duration of the fast commands (specified in 40MHz clk cycles).

**CNFG_FASTBLOCK_TYPE,** Configuration signals from IPBus
* **trigger_source:**            1=IPBus, 2=Test-Pulse FSM, 3=TTC, 4=TLU, 5=External trigger, 6=Hit-Or, 7=At a user-defined frequency, 0=Undefined.
* **autozero_source:**           1=IPBus, 2=Test-Pulse FSM, 3=At user-defined frequency, 0=Auto-zero disabled. 
* **triggers_to_accept:**        Number of triggers to be accepted (0--> unlimited triggers until triggering process is stopped by the user).
* **initial_fast_reset_enable:** Enable ECR command at the beginning of the triggering process.
* **ext_trigger_delay_value:**   Delay for the external trigger source (when trigger source is 4 or 5).
* **delay_after_fast_reset:**    Test Pulse FSM, delay after ECR command (trigger source = 2).
* **delay_after_autozero:**      Test Pulse FSM, delay after auto-zero command (trigger source = 2).
* **delay_after_prime_pulse:**   Test Pulse FSM, delay after first Calibration injection command (trigger source = 2).
* **delay_after_inject_pulse:**  Test Pulse FSM, delay after second calibration injection command. (trigger source = 2).
* **delay_before_next_pulse:**   Test Pulse FSM, delay before next test sequence (trigger source = 2).
* **tp_fsm_fast_reset_en:**      Test Pulse FSM, enable ECR command at the beginning of the test sequence (trigger source = 2).
* **tp_fsm_test_pulse_en:**      Test Pulse FSM, enable first calibration injection command on the test sequence (trigger source = 2).
* **tp_fsm_inject_pulse_en:**    Test Pulse FSM, enable second calibration injection command on the test sequence. If first cal command is disabled, second cal command is also disabled (trigger source = 2).
* **tp_l1a_en:**                 Test Pulse FSM, enable trigger command on the test sequence (trigger source = 2).
* **cal_data_prime:**            Data word for calibration injection command.
* **cal_data_inject:**           Data word for calibration injection command, applies only to Test Pulse FSM where 2 consequtive commands are sent.
* **backpressure_enable:**       Enables backpressure check from readout.
* **veto_en:**                   Enables vetoing triggers when auto-zero process is active.
* **global_pulse_data:**         Data word for for Global Pulse command.
* **autozero_freq:**             Auto-zero frequency, specified in 10MHz clock cycles (autozero_source = 3).
* **veto_after_autozero:**       Duration of trigger-veto after the execution of global pulse command (autozero_source = 3).


**STAT_FASTBLOCK_TYPE** Status signals to IPBus
* **trigger_source:**     Trigger source.
* **trigger_state:**      Status of the triggering FSM.
* **if_configured:**      '1' when configuration settings have been loaded.
* **trigger_in_counter:** Received triggers. 


## User Instructions
This section is intended to be a short guide on the usage of the fast commands block and contains instructions how to be treated by the software.
Additional info about this block can be found here https://indico.cern.ch/event/759741/contributions/3151120/attachments/1720441/2777234/fast_commands_block_overview.pdf

**Control Commands**
There are 4 commands which control the operation of this block: *reset*, *load_config*, *start_trigger*, *stop_trigger*. 
In order to send them, the signal *cmd_strobe* has to be set high first and then flip the apropriate bit for the relevant command.

**Initial Configuration**
The basic configuration for the correct operation of this block includes:
- Set the correct trigger source (register *trigger_source*, sets also the source for the Calibration injection command which is disabled when a value other than 1 or 2 is selected).
- Set the correct source for the Auto-zero process (register *autozero_source*).
- Set the desired amount of triggers to accept (register *triggers_to_accept*).
- Set various signals (*initial_fast_reset_enable*, *backpressure_enable*, *veto_en*, described above).
- Set enable signals and delays between each state of the FSM, when Test-Pulse FSM is selected as trigger source.
- Set signals associated with auto-zero process (frequency etc), when continuous auto-zeroing at a user-defined frequency has been selected as source (register *autozero_source* = 3).
- Then send *"load_config"* command.

**Send Triggers**
There are two operation modes: continuous triggering and limited amount of triggers mode, defined by *"triggers_to_accept"* register. 0 = continuous triggering, the other values set the desired amount of triggers to be sent (accepted). Maximum amount of triggers is 1,000,000 (continuous mode is not limited).
The algorithm of operation is the following:
* Software sends *"Start_trigger"* signal
* Firmware issues *"FAST_Reset"* (i.e. ECR command) signal, if enabled by the *"initial_fast_reset_enable"* register.
* Firmware waits for a few clock cycles.
* Firmware switches to the Running state and starts issuing triggers from the selected trigger source.
* Upon receival of the *"Stop_trigger"* signal from the software (or when *"triggers_to_accept"* amount is reached), firmware goes back to the Idle state.
Triggers may be sent to the chip __ONLY__ when the triggering process is active (i.e. firmware is in Running state), otherwise triggers are ignored.
All other fast commands may be sent at any time without any restrictions.

**Fast Commands from IPBus**
When IPBus is selected as trigger source (and as auto-zero source), in order to send fast commands through the IPBus, one has to flip the appropriate bit in the IPBus registers.
- *ipb_trigger*:     A trigger commandd is sent, trigger tag value is generated internally by a counter in the firmware.
- *ipb_fast_reset*:  ECR command is sent and also resets the contents of the trigger tag value counter.
- *ipb_orbit_reset*: BCR command is sent and also resets the contents of the trigger tag value counter.
- *ipb_test_pulse*:  Cal command is sent, along with the relevant data (contents of the *cal_data_prime* register).
- *ipb_glb_pulse*:   Global pulse command is sent, along with the relevant data (contents of the *global_pulse_data* register).












