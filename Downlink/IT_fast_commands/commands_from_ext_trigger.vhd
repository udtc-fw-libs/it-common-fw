------------------------------------------------------------------------------------
---- Engineer: Mykyta Haranko,
---- Create Date: 02/22/2018 12:21:12 PM
------------------------------------------------------------------------------------
---- external trigger from any source like dio5, fmc 2xcbc3, fermi trigger board
------------------------------------------------------------------------------------
---- Modified for IT, Engineer: Yiannis Kazas
---- Last Change: 20-May-2019
------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use IEEE.NUMERIC_STD.ALL;
use work.user_package.all;
library unisim;
use unisim.vcomponents.all;

entity commands_from_ext_trigger is
Port (
    reset_i                         : in std_logic;
    clk_40MHz_i                     : in std_logic;
    clk_320MHz_i                    : in std_logic;
    -- cnfg
    ext_trigger_delay_value_i       : in std_logic_vector(8 downto 0);
    -- ext trigger
    ext_trigger_i                   : in std_logic;
    --
    trigger_duration_i              : in integer range 0 to 32;
    -- trigger state
    trigger_state_i                 : in trigger_state_type;
    -- fast signals
    tdc_value_o                     : out std_logic_vector(7 downto 0);
    external_l1a_o                  : out std_logic
);
end commands_from_ext_trigger;

architecture rtl of commands_from_ext_trigger is


    -- delayed ext select
    signal ext_trigger_delay_value  : std_logic_vector(8 downto 0);
    --  trigger signal (rising edge)
    signal ext_trigger_rising_edge  : std_logic := '0';
    -- tdc value
    signal tdc_value_int            : std_logic_vector(7 downto 0) := (others => '0');
    signal tdc_value_delayed        : std_logic_vector(7 downto 0) := (others => '0');

begin

    -- configuration sync
    process (clk_40MHz_i)
    begin
        if rising_edge(clk_40MHz_i) then            
            if trigger_state_i = Idle then
                ext_trigger_delay_value <= ext_trigger_delay_value_i; 
            end if;            
        end if;
    end process;

    --======================================--
    -- ext trigger rising edge detector (with noise filtering)
    --======================================--
        rising_edge_detector_inst: entity work.rising_edge_detector
        generic map
        ( 
            CLK_DIV     => 8
        )
        port map 
        ( 
            reset_i     => reset_i,
            clk_ref_i   => clk_320MHz_i,
            clk_latch_i => clk_40MHz_i,
            signal_i    => ext_trigger_i,
--            trigger_duration_i => trigger_duration_i,
            --
            signal_o    => ext_trigger_rising_edge,
            tdc_o       => tdc_value_int
        );
    --======================================--
    
    --======================================--
    -- Ext Trigger delay line
    --======================================--
        ext_trigger_shifter_inst: entity work.shift_stub_req
        port map (
            A       => ext_trigger_delay_value,
            D(0)    => ext_trigger_rising_edge,
            CLK     => clk_40MHz_i,
            Q(0)    => external_l1a_o
        );
        
        tdc_shifter_inst: tdc_shift_ip
        port map (
            A       => ext_trigger_delay_value,
            D       => tdc_value_int,
            CLK     => clk_40MHz_i,
            Q       => tdc_value_delayed
        );
        tdc_value_o <= tdc_value_delayed;
    --======================================--


end rtl;
