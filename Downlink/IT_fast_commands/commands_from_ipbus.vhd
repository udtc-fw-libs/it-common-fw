----------------------------------------------------------------------------------
-- Engineer: Yiannis Kazas (original file by Mykyta Haranko)
-- Create Date: 02/22/2018 11:12:46 AM
----------------------------------------------------------------------------------
-- this block sends the fast commands from the ipbus with configurable duration
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use IEEE.NUMERIC_STD.ALL;
use work.user_package.all;

entity commands_from_ipbus is
Port (
    reset_i             : in std_logic;
    clk_40MHz_i         : in std_logic;
    -- ctrl fast block
    ctrl_fastblock_i    : in ctrl_fastblock_type;
    -- output of fast commands from the ipbus
    ipb_fast_reset_o    : out std_logic; 
    ipb_l1a_o           : out std_logic;    
    ipb_test_pulse_o    : out std_logic;    
    ipb_orbit_reset_o   : out std_logic;
    ipb_glb_pulse_o     : out std_logic    
);
end commands_from_ipbus;

architecture rtl of commands_from_ipbus is

-- enable signal for all those
signal ipb_fast_reset_en        : std_logic := '0'; 
signal ipb_l1a_en               : std_logic := '0';    
signal ipb_test_pulse_en        : std_logic := '0';    
signal ipb_orbit_reset_en       : std_logic := '0'; 
signal ipb_glb_pulse_en         : std_logic := '0'; 
-- counter
signal ipb_shift_counter        : natural range 0 to 15 := 0;   
-- shift register state
type ipb_fast_command_composer_state_type is (Idle, Sending); 
signal ipb_fast_command_composer_state : ipb_fast_command_composer_state_type := Idle;
    

begin

--===================================--
-- BEGIN IPBUS SENDER
--===================================--
ipb_fast_process: process (clk_40MHz_i)
begin
    if rising_edge(clk_40MHz_i) then
        if (reset_i = '1') then        
            -- signals
            ipb_fast_reset_o   <= '0'; 
            ipb_l1a_o          <= '0';    
            ipb_test_pulse_o   <= '0';    
            ipb_orbit_reset_o  <= '0';
            ipb_glb_pulse_o    <= '0'; 
            -- enable signal for all those
            ipb_fast_reset_en  <= '0'; 
            ipb_l1a_en         <= '0';    
            ipb_test_pulse_en  <= '0';    
            ipb_orbit_reset_en <= '0';
            ipb_glb_pulse_en   <= '0';  
            -- counter
            ipb_shift_counter  <= 0;   
            -- state
            ipb_fast_command_composer_state <= Idle;            
        else 
            -- reset every clock cycle
            ipb_fast_reset_o   <= '0'; 
            ipb_l1a_o          <= '0';    
            ipb_test_pulse_o   <= '0';    
            ipb_orbit_reset_o  <= '0';
            ipb_glb_pulse_o    <= '0'; 
            
            -- state machine 
            case ipb_fast_command_composer_state is
            
                when Idle =>
                    if (ctrl_fastblock_i.ipb_ecr = '1') or (ctrl_fastblock_i.ipb_trigger = '1') or (ctrl_fastblock_i.ipb_test_pulse = '1') or (ctrl_fastblock_i.ipb_bcr = '1') or (ctrl_fastblock_i.ipb_glb_pulse = '1') then
                        -- setting enable flags
                        ipb_fast_reset_en  <= ctrl_fastblock_i.ipb_ecr; 
                        ipb_l1a_en         <= ctrl_fastblock_i.ipb_trigger;   
                        ipb_test_pulse_en  <= ctrl_fastblock_i.ipb_test_pulse;    
                        ipb_orbit_reset_en <= ctrl_fastblock_i.ipb_bcr;
                        ipb_glb_pulse_en   <= ctrl_fastblock_i.ipb_glb_pulse;                      
                        -- set the counter to the desired duration
                        ipb_shift_counter  <= to_integer(unsigned(ctrl_fastblock_i.ipb_fast_duration));
                        -- state
                        ipb_fast_command_composer_state <= Sending;
                    end if;
                when Sending =>
                    -- output the commands
                    ipb_fast_reset_o  <= ipb_fast_reset_en;   
                    ipb_l1a_o         <= ipb_l1a_en;   
                    ipb_test_pulse_o  <= ipb_test_pulse_en;   
                    ipb_orbit_reset_o <= ipb_orbit_reset_en;
                    ipb_glb_pulse_o   <= ipb_glb_pulse_en;   

                    -- check the counter
                    if ipb_shift_counter > 0 then
                        -- decrement
                        ipb_shift_counter <= ipb_shift_counter - 1;
                    else
                        -- then go back to the idle
                        ipb_fast_command_composer_state <= Idle;
                    end if;
                when others =>
                    ipb_fast_command_composer_state <= Idle;
            end case;
        end if;
    end if;
end process;
--===================================--
-- END IPBUS SENDER
--===================================--

end rtl;
