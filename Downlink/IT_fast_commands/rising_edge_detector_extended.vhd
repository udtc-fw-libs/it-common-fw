----------------------------------------------------------------------------------
-- Create Date: 17/May/2019 
----------------------------------------------------------------------------------
-- Last Change: 20-May-2019
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity rising_edge_detector_extended is
    Generic
    ( 
        CLK_DIV     : natural := 8 
    );
    Port 
    ( 
        reset_i            : in std_logic;
        clk_ref_i          : in std_logic;
        clk_latch_i        : in std_logic;
        signal_i           : in std_logic;
        
        trigger_duration_i : in integer range 0 to 32;
        --
        signal_o           : out std_logic;
        tdc_o              : out std_logic_vector(7 downto 0)  
    );
end rising_edge_detector_extended;
    
architecture rtl of rising_edge_detector_extended is

    signal signal_buf       : std_logic_vector(CLK_DIV-1 downto 0) := (others => '0');
    signal phase_counter    : natural range 0 to CLK_DIV-1 := 0;
    signal signal_without_noise : std_logic := '0';
    -- edge detectors
    signal signal_prev      : std_logic := '0';
    signal signal_rising    : std_logic := '0';
    signal signal_falling   : std_logic := '0';
    --
    signal sync_latched     : std_logic := '0';
    signal sync_latched_buf : std_logic_vector(3 downto 0) := "0000";
    --
    signal tdc_value_fast       : natural range 0 to CLK_DIV-1 := 0;
    subtype tdc_value_slow_type is natural range 0 to CLK_DIV-1;   
    type tdc_value_slow_buf_type is array (0 to 3) of tdc_value_slow_type;
    signal tdc_value_slow_buf   : tdc_value_slow_buf_type;
    
    type trigger_state_type is (trigger_idle, trigger_sending);
    signal trigger_state : trigger_state_type;
    signal trigger_duration_counter : integer range 0 to 31;

begin

    -- tdc output
    tdc_o <= std_logic_vector(to_unsigned(tdc_value_slow_buf(3),8));

    -- oversampling buffer
    process(clk_ref_i)
    begin
        if rising_edge(clk_ref_i) then
            if reset_i = '1' then
                signal_buf <= (others => '0');
            else
                -- put the value into the shift register
                signal_buf(CLK_DIV-1 downto 0) <= signal_buf(CLK_DIV-2 downto 0) & signal_i;
            end if;
        end if;
    end process;
    
    -- noise filtering
    process(clk_ref_i)
    begin
        if rising_edge(clk_ref_i) then
            if reset_i = '1' then
                signal_without_noise <= '0';
            else                
                if signal_buf(3) = '1' and signal_buf(0) = '1' then
                    if signal_buf(2) = '1' or signal_buf(1) = '1' then
                        signal_without_noise <= '1';
                    end if;
                else
                    signal_without_noise <= '0';
                end if;
            end if;
        end if;
    end process;
    
    -- edge detection
    process(clk_ref_i)
    begin
        if rising_edge(clk_ref_i) then
            if reset_i = '1' then
                signal_prev <= '0';
            else                
                signal_prev <= signal_without_noise;
            end if;
        end if;
    end process;
    --
    signal_rising <= signal_without_noise and (not signal_prev);
    signal_falling <= (not signal_without_noise) and signal_prev;
    
    -- synch trigger relative to the latching clock
    process(clk_ref_i)
    begin
        if rising_edge(clk_ref_i) then
            if reset_i = '1' then
                sync_latched <= '0';
                tdc_value_fast <= 0;
            else
                if signal_rising = '1' then
                    tdc_value_fast <= phase_counter;                
                    sync_latched <= not sync_latched;
                end if;
            end if;
        end if;
    end process;   
    
    -- latch clock sync (finaly)
    process(clk_latch_i)
    begin
        if rising_edge(clk_latch_i) then
            if reset_i = '1' then
                signal_o <= '0';
                sync_latched_buf <= (others => '0');
                tdc_value_slow_buf <= (others => 0); 
                trigger_state <= trigger_idle;              
            else
                case trigger_state is
                    when trigger_idle =>
                        signal_o                     <= '0';
                        trigger_duration_counter     <= trigger_duration_i;
                        sync_latched_buf(3 downto 0) <= sync_latched_buf(2 downto 0) & sync_latched;  
                        tdc_value_slow_buf(3)        <= tdc_value_slow_buf(2);
                        tdc_value_slow_buf(2)        <= tdc_value_slow_buf(1);
                        tdc_value_slow_buf(1)        <= tdc_value_slow_buf(0);
                        tdc_value_slow_buf(0)        <= tdc_value_fast;
                        
                        -- rising edge of the sync latched in our clock domain
                        if (sync_latched_buf(3) /= sync_latched_buf(2)) then
                            trigger_state <= trigger_sending;
--                            signal_o <= '1';
--                            if (trigger_duration_i > 0) then
--                                trigger_state            <= trigger_sending;
--                                trigger_duration_counter <= trigger_duration_i;
--                            else
--                                trigger_state <= trigger_idle;
--                            end if;
                        end if;
                    
                    when trigger_sending =>
                        signal_o <= '1';
                        if (trigger_duration_counter <= 0) then
                            trigger_state <= trigger_idle;
                        else
                            trigger_state            <= trigger_sending;
                            trigger_duration_counter <= trigger_duration_counter -1;
                        end if;    
--                        trigger_duration_counter <= trigger_duration_counter -1;
--                        if (trigger_duration_counter <= 1) then
--                            trigger_state <= trigger_idle;
--                            signal_o      <= '0';
--                        else
--                            trigger_state <= trigger_sending;
--                            signal_o      <= '1';
--                        end if;
                    when others =>
                        trigger_state <= trigger_idle;
                end case;   
            end if;
        end if;
    end process;   

    -- phase counter
    process(clk_ref_i, reset_i)
    begin
        if reset_i = '1' then
            phase_counter <= 0;
        elsif rising_edge(clk_ref_i) then               
            -- phase counter incrementation
            if phase_counter = CLK_DIV-1 then
                phase_counter <= 0;
            else
                phase_counter <= phase_counter + 1;
            end if;
        end if;
    end process;

end rtl;
