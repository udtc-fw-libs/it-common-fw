----------------------------------------------------------------------------------
-- Engineer: Yiannis Kazas
-- Create Date: 07/13/2018 06:11:00 PM
-- Module Name: commands_from_HitOr - RTL
----------------------------------------------------------------------------------
-- Last Change: 20-May-2019
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use ieee.std_logic_unsigned.all;
use work.user_package.all;
use IEEE.std_logic_misc.all;

--------------------------------------
entity commands_from_HitOr is
    Port ( reset_i           : in STD_LOGIC;
           clk_40MHz_i       : in STD_LOGIC;
           clk_320MHz_i      : in STD_LOGIC;
           -- ctrl fast block
           ctrl_fastblock_i  : in ctrl_fastblock_type;
           cnfg_fastblock_i  : in cnfg_fastblock_type;
           -- trigger state and source
           trigger_state_i   : in trigger_state_type;
           trigger_source_i  : in trigger_source_type;
           -- Trigger lines from RD53A
           HitOr_i           : in STD_LOGIC;--_VECTOR (3 downto 0);
           --
           trigger_duration_i : in integer range 0 to 32;
           -- fast signals           
           HitOr_trigger_o   : out STD_LOGIC;
           HitOr_tdc_value_o : out STD_LOGIC_VECTOR (7 downto 0)
           );
end commands_from_HitOr;
--------------------------------------

architecture RTL of commands_from_HitOr is



--======================================================================
-- Start of Signal Declaration
--======================================================================
signal HitOr                    : std_logic;
-- delayed ext select
--signal ext_trigger_delayed      : std_logic := '0';
signal ext_trigger_delay_value  : std_logic_vector(8 downto 0);
-- tdc value
signal tdc_value_int            : std_logic_vector(7 downto 0) := (others => '0');

type HitOr_state_type is (HitOr_Idle, HitOr_sending);
signal HitOr_state           : HitOr_state_type;
signal trigger_duration_cntr : integer range 0 to 31;
--======================================================================
-- End of Signal Declaration
--======================================================================


begin
--======================================================================
-- Start of Main Body
--======================================================================

--------------------------------------------------------------------------
---- Define trigger-command duration when a HitOr signal is received
--------------------------------------------------------------------------
--process(reset_i, clk_40MHz_i)
--begin
--    if (reset_i = '1') then
--        HitOr_state <= HitOr_Idle;
--    elsif rising_edge(clk_40MHz_i) then
--        case HitOr_state is
--            when HitOr_Idle =>
--                HitOr_trigger_o <= '0';
--                if (HitOr_i = '1') then
--                    HitOr_state           <= HitOr_sending;
--                    trigger_duration_cntr <= trigger_duration_i;
--                else
--                    HitOr_state           <= HitOr_Idle;
--                    trigger_duration_cntr <= 0;
--                end if;
--            when HitOr_sending =>
--                HitOr_trigger_o <= '1';
--                if (trigger_duration_cntr <= 0) then
--                    HitOr_state <= HitOr_Idle;
--                else
--                    trigger_duration_cntr <= trigger_duration_cntr -1;
--                    HitOr_state           <= HitOr_sending;
--                end if;
            
--            when others =>
--                HitOr_state <= HitOr_Idle;
--        end case;
--    end if;
--end process;
--------------------------------------------------------------------------
--------------------------------------------------------------------------
                


-- -- configuration sync
--process (clk_40MHz_i)
--begin
--    if rising_edge(clk_40MHz_i) then            
--        if trigger_state_i = Idle then
--            ext_trigger_delay_value <= cnfg_fastblock_i.ext_trigger_delay_value; 
--        end if;            
--    end if;
--end process;

------ HitOr trigger is signaled if at least one of the 4 HitOr outputs fires
----HitOr <= or_reduce(HitOr_i);

--======================================--
-- ext trigger rising edge detector (with noise filtering)
--======================================--
rising_edge_detector_inst0: entity work.rising_edge_detector
generic map
( 
    CLK_DIV     => 8
)
port map 
( 
    reset_i     => reset_i,
    clk_ref_i   => clk_320MHz_i,
    clk_latch_i => clk_40MHz_i,
    signal_i    => HitOr_i,
--    trigger_duration_i => trigger_duration_i,
    --
    signal_o    => HitOr_trigger_o,
    tdc_o       => HitOr_tdc_value_o --tdc_value_int
);
--======================================--
--HitOr_tdc_value_o <= tdc_value_int;-- when (trigger_source_i = SourceExt or trigger_source_i = SourceTLU) else (others => '0');


--======================================================================
-- End of Signal Declaration
--======================================================================


end RTL;
