-- CMD_Block
-- written by Russell Taylor
-- Feed data to CMD_Block Core to an I2C bus


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.cmd_block_package.all;

entity CMD_Block_tb is
end CMD_Block_tb;

architecture behavior of CMD_Block_tb is

constant clk_period : time := 100 ns; -- 10 MHz

signal cmdClock160		: std_logic := '0';
signal cmdClock40		: std_logic := '0';
signal cmdClock10		: std_logic := '0';
signal cmdDone		: std_logic := '0';
signal cmd_serial_out_p		: std_logic := '0';
signal cmd_serial_out_n		: std_logic := '0';
signal cmdBlockReset		: std_logic := '0';
signal trigger				: integer range 0 to 15;
signal tag					: integer range 0 to 31;
signal cmd					: std_logic_vector (7 downto 0);
signal cmdFrame				: array22x5;
signal cmdLength			: integer range 0 to 22;
signal count 				: integer range 0 to 99 := 0;
signal BCR					: std_logic := '0';
signal ECR					: std_logic := '0';
signal GPFrame				: std_logic_vector (9 downto 0) := "0000000000";
signal GPDone				: std_logic;
signal calFrame				: std_logic_vector (19 downto 0) := x"00000";
signal calDone				: std_logic;
signal fifoRd				: std_logic;
signal fifoByte				: std_logic_vector (7 downto 0):= x"00";
signal fifoEmpty			: std_logic := '1';
-- signal outByte				: std_logic_vector(7 downto 0);
constant cword				: array24x8bit := (x"17", x"66", x"01", x"02", x"03", x"04", x"05", x"06",
		x"07", x"08", x"09", x"0a", x"0b", x"0c", x"0d", x"0e", x"0f", x"10", x"11", x"12", x"13",
		x"14", x"15", x"16");
signal fifoCount			: integer range 0 to 5 := 0;

begin

reset_process : process
begin
    cmdBlockReset <= '1';
    wait for 3*clk_period;
    cmdBlockReset <= '0';
    wait;
end process;


clk10_process : process begin cmdClock10 <= not cmdClock10; wait for clk_period/2; end process;
clk40_process : process begin cmdClock40 <= not cmdClock40; wait for clk_period/8; end process;
clk160_process : process begin cmdClock160 <= not cmdClock160; wait for clk_period/32; end process;

--Trigger <= 1;
--tag <= 0;
--cmd <= x"00";
--cmdFrame <= (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
--cmdLength <= 0;

fifoprocess : process
begin
fifoEmpty <= '0';
fifocount <= 1;
fifoByte <= cWord(0);
L1: loop
	wait for clk_period / 16;
	if fifoRd = '1' then
		fifoByte <= cWord(fifoCount);
		if fifoCount < 23 then
			fifoCount <= fifoCount + 1;
		else
			fifoCount <= 0;
		end if;
	end if;
end loop;
end process;

cmdcycle: process (cmdBlockReset, cmdClock10)
	begin
	if cmdBlockReset = '1' then
		count <= 0;
		trigger <= 0;
		tag <= 0;
		gpFrame <= "0000000000";
		calFrame <= x"00000";
		cmd <= x"00";
		cmdFrame <= (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		cmdLength <= 0;
	elsif cmdClock10'event and cmdClock10 = '0' then
		if count = 99 then
			count <= 0;
		else
			count <= count + 1;
		end if;
		case count is
			when 1 =>
				trigger <= 1;
				tag <= 5;
				if gpDone = '1' then
					gpFrame <= "0000000000";
				end if;
				if calDone = '1' then
					calFrame <= x"00000";
				end if;
				BCR <= '0';
				ECR <= '0';
			when 2 =>
				trigger <= 0;
				tag <= 5;
				if gpDone = '1' then
					gpFrame <= "0000000000";
				end if;
				if calDone = '1' then
					calFrame <= x"00000";
				end if;
				BCR <= '1';
				ECR <= '0';
			when 3 =>
				trigger <= 0;
				tag <= 5;
				if gpDone = '1' then
					gpFrame <= "0000000000";
				end if;
				if calDone = '1' then
					calFrame <= x"00000";
				end if;
				BCR <= '0';
				ECR <= '1';
			when 5 =>
				trigger <= 0;
				tag <= 5;
				gpFrame <= "0000100011";
				if calDone = '1' then
					calFrame <= x"00000";
				end if;
				BCR <= '0';
				ECR <= '0';
			when 10 =>
				trigger <= 0;
				tag <= 5;
				if gpDone = '1' then
					gpFrame <= "0000000000";
				end if;
				calFrame <= x"20861"; -- 4 2 3 1
				BCR <= '0';
				ECR <= '0';
			when 15 =>
				trigger <= 1;
				tag <= 5;
				if gpDone = '1' then
					gpFrame <= "0000000000";
				end if;
				if calDone = '1' then
					calFrame <= x"00000";
				end if;
				BCR <= '0';
				ECR <= '0';
			when others =>
				trigger <= 0;
				tag <= 5;
				if gpDone = '1' then
					gpFrame <= "0000000000";
				end if;
				if calDone = '1' then
					calFrame <= x"00000";
				end if;
				BCR <= '0';
				ECR <= '0';
		end case;
	end if;
end process;


DUT: entity work.CMD_Block_core
port map(
  cmdClock160			=> cmdClock160,
  cmdClock40            => cmdClock40,
  cmdClock10            => cmdClock10,
  cmdDone             	=> cmdDone,
  out_p				    => cmd_serial_out_p,
  out_n      			=> cmd_serial_out_n,
  cmdBlockReset         => cmdBlockReset,
  trigger              	=> trigger,
  tag             		=> tag,
  -- cmd             		=> cmd,
  -- cmdFrame             	=> cmdFrame,
  -- cmdLength				=> cmdLength,
  BCR					=> BCR,
  ECR					=> ECR,
  GPFrame				=> GPFrame,
  GPDone				=> GPDone,
  calFrame				=> calFrame,
  calDone				=> calDone,
  fifoRd				=> fifoRd,
  fifoByte				=> fifoByte,
  fifoEmpty				=> fifoEmpty
);
end behavior;
