--==================================================================================================
-- CMD_Block_core
-- written by Russell Taylor, modified for module support by Yiannis Kazas
----------------------------------------------------------------------------------
-- 10, 40 and 160 MHz clocks sould be phase aligned.
-- load word with next command on 10 mhz rising edge. which will be the last half byte of previous word transmitted using 40 MHz clock
-- start sending new word on next 40 MHz edge
-- cmdDone signals next rising 10 MHz edge will need to be a new cmd or cmd set to "00000000".  trigger always executes next!
----------------------------------------------------------------------------------
-- Last Changes : 13-June-2019
--==================================================================================================

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.cmd_block_package.all;
use work.user_package.all;
use IEEE.std_logic_misc.all;

library unisim;
use unisim.vcomponents.all;

entity CMD_Block_core is
port(
	cmdClock160           	: in    std_logic;  -- fast serial clock
	cmdClock40            	: in    std_logic;
	cmdClock10            	: in    std_logic;
	out_p                   : out   std_logic_vector (NUM_HYBRIDS-1 downto 0); -- transaction data latched
	out_n	            	: out   std_logic_vector (NUM_HYBRIDS-1 downto 0); -- transaction data latched
	cmd_to_optical_o        : out   lpgbtfpga_downlink_chipframe;
	cmdBlockReset        	: in    std_logic; -- fsm_clock
	trigger               	: in    integer range 0 to 15; -- active high reset
	tag						: in	integer range 0 to 31;
	BCR						: in 	std_logic;
	ECR						: in 	std_logic;
	GPFrame					: in 	std_logic_vector (9 downto 0);
	GPDone					: out 	std_logic;
	calFrame				: in 	std_logic_vector (19 downto 0);
	calDone					: out 	std_logic;
	--
--	enable_sync_word_i      : in std_logic;
	ctrl_global_i           : in ctrl_global_type;
	cmd_fifo_data_valid_i   : in std_logic;
	cmd_fifo_dout_i         : in std_logic_vector(15 downto 0);
	cmd_module_address_i    : in std_logic_vector(NUM_HYBRIDS-1 downto 0);
	cmd_controller_busy_o   : out std_logic;--_vector(NUM_HYBRIDS-1 downto 0);
	-- john debug
	init_in_progress_o      : out std_logic;
	resync_i                : in    std_logic;
	command_out             : out   std_logic_vector(15 downto 0)
	--
);
end CMD_Block_core;

architecture behavioral of CMD_Block_core is

--==================================================================================================
-- Start of Signal Declaration
--==================================================================================================
--    constant RESET_TIME       : integer := 1000;
--    constant PLL_LOCK_TIME    : integer := RESET_TIME + 50_000;
--    constant FRAME_ALIGN_TIME : integer := PLL_LOCK_TIME + 1000;
    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- LUT for trigger symbols
    constant tLUT : array16x8bit := (x"00", x"2b", x"2d", x"2e", x"33", x"35", x"36", x"39", 
                                     x"3a", x"3c", x"4b", x"4d", x"4e", x"53", x"55", x"56");
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- Command Encoding for RD53A chip ----
        -- LUT for data symbols
    constant dLUT_rd53a     : array32x8bit := (x"6a", x"6c", x"71", x"72", x"74", x"8b", x"8d", x"8e", 
                                               x"93", x"95", x"96", x"99", x"9a", x"9c", x"a3", x"a5", 
                                               x"a6", x"a9", x"aa", x"ac", x"b1", x"b2", x"b4", x"c3", 
                                               x"c5", x"c6", x"c9", x"ca", x"cc", x"d1", x"d2", x"d4");
    constant BCRword_rd53a  : std_logic_vector (15 downto 0) := x"5959";
    constant ECRWord_rd53a  : std_logic_vector (15 downto 0) := x"5a5a";
    constant GPWord_rd53a   : std_logic_vector (15 downto 0) := x"5c5c";
    constant CalWord_rd53a  : std_logic_vector (15 downto 0) := x"6363";
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- Command Encoding for RD53B chip ----
        -- LUT for data symbols
    constant dLUT_rd53b     : array32x8bit := (x"6a", x"6c", x"71", x"72", x"74", x"8b", x"8d", x"8e", 
                                               x"93", x"95", x"96", x"99", x"9a", x"9c", x"a3", x"a5", 
                                               x"a6", x"a9", x"59", x"ac", x"b1", x"b2", x"b4", x"c3", 
                                               x"c5", x"c6", x"c9", x"ca", x"cc", x"d1", x"d2", x"d4");
    -- "BCR" & "ECR" Commands both correspond to "Clear" command
    constant BCRword_rd53b  : std_logic_vector (15 downto 0) := x"5AD4";
    constant ECRWord_rd53b  : std_logic_vector (15 downto 0) := x"5AD4";
    constant GPWord_rd53b   : std_logic_vector (15 downto 0) := x"5CD4";
    constant CalWord_rd53b  : std_logic_vector (15 downto 0) := x"63D4";
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    signal dLUT     : array32x8bit;
    signal BCRword  : std_logic_vector (15 downto 0);
    signal ECRWord  : std_logic_vector (15 downto 0);
    signal GPWord   : std_logic_vector (15 downto 0);
    signal CalWord  : std_logic_vector (15 downto 0);
    
    type outtype is array (15 downto 0) of std_logic_vector (0 downto 0);
    signal cmd_serial_out_p : outtype;
    signal cmd_serial_out_n : outtype;
    signal prev10      : std_logic := '0';
    signal edge10      : std_logic := '0';
    signal done        : std_logic := '0';
    signal toggle      : std_logic := '0';
    signal BCRSave     : std_logic := '0';
    signal ECRSave     : std_logic := '0';
    signal calDoneTemp : std_logic := '1';
    signal gpDoneTemp  : std_logic := '1';
    signal nGPPending  : std_logic := '1';
    signal nCalPending : std_logic := '1';
    
    signal init_done : std_logic:='0';
    signal Fast_cmd : std_logic_vector (15 downto 0);
    signal calCount : integer range 0 to 2;
    
    signal clk10_toggle  : std_logic:= '0';
    signal edge10_o      : std_logic:= '0';
    signal halfByteCount : integer range 0 to 3 := 0;
    
    -- Communication Initialization
    type Init_state_type is (Init_Idle, Initialization_sequence);
    signal Initialization_state : Init_state_type;
    signal sync_pattern     : std_logic_vector(15 downto 0):=(others=>'0');
    signal init_in_progress : std_logic :='0';
    signal init_counter     : integer range 0 to 10_000_000:= 0;
    signal toggle_rst       : std_logic:='0';
    signal toggle_rst_prev  : std_logic:='0';
    signal rst_issued       : std_logic:='0';
    
    signal clk_divided       : std_logic:='0';
    signal clk_div_cntr      : integer range 0 to 1000:= 0; 
    signal frequency_divider : integer range 0 to 50:= 4;--39;
    
    signal data_valid : std_logic_vector(NUM_HYBRIDS-1 downto 0);
    
    
    signal nb_of_pll_words  : natural range 0 to 1023 := 31;
    signal nb_of_sync_words : natural range 0 to 1023 := 1;
    signal set_sync_cmds_pre_pre : std_logic := '0';
    signal set_sync_cmds_pre     : std_logic := '0';
    signal set_sync_cmds         : std_logic := '0';
    
    signal reset_time       : integer := 100;--1600;
    signal PLL_LOCK_TIME    : integer := 51_000;--RESET_TIME + 50_000;
    signal FRAME_ALIGN_TIME : integer := 52_000;--PLL_LOCK_TIME + 1000;

    
    signal cmd_controller_busy   : std_logic_vector(NUM_HYBRIDS-1 downto 0);
    
--==================================================================================================
-- END of Signal Declaration
--==================================================================================================

    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
--    -- Debug Core--
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    
--    COMPONENT cmd_fast_interface_debug
--    PORT (
--        clk : IN STD_LOGIC;
--        probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
--        probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
--        probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
--        probe3 : IN STD_LOGIC_VECTOR(9 DOWNTO 0); 
--        probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
--        probe5 : IN STD_LOGIC_VECTOR(19 DOWNTO 0); 
--        probe6 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
--        probe7 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
--        probe8 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
--        probe9 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
--        probe10 : IN STD_LOGIC_VECTOR(1 DOWNTO 0); 
--        probe11 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
--        probe12 : IN STD_LOGIC_VECTOR(15 DOWNTO 0); 
--        probe13 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--        probe14 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--        probe15 : in std_logic_vector(15 downto 0)
--    );
--    END COMPONENT  ;
	        
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   
begin

    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    
            
--debug_cmd_fast : cmd_fast_interface_debug
--PORT MAP (
--	clk => cmdClock160,
--	probe0(0) => set_sync_cmds,--ecr,--'0' when trigger = 0 else '1', 
--	probe1(0) => ecr, 
--	probe2(0) => bcr, 
--	probe3 => std_logic_vector(to_unsigned(reset_time,10)),--GPFrame, 
--	probe4(0) => rst_issued, 
--	probe5 => std_logic_vector(to_unsigned(init_counter,20)),--CalFrame, 
--	probe6(0) => ecr, 
--	probe7(0) => toggle_rst, 
--	probe8(0) => toggle_rst_prev, 
--	probe9(0) => caldonetemp, 
--	probe10 => ecr & bcr,--"00" when calCount = 0 else "01" when calCount = 1 else "10" when calcount = 2 else "11", 
--	probe11(0) => ncalPending, 
--	probe12 => Fast_cmd, 
--	probe13(0) => gpDoneTemp,
--	probe14(0) => nGPPending,
--	probe15 => cmd_fifo_dout_i
--);
    init_in_progress_o <= init_in_progress;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
---- END Debug Core--
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    pll_lock_time    <= reset_time + 50_000;--*16;--50_000;
    FRAME_ALIGN_TIME <= pll_lock_time + 1000;--*16;--1000;
    
--==================================================================================================
-- Start of Main Body
--==================================================================================================


	gen_rd53a_cmds: if (FRONT_END = RD53A) generate
	   dLUT    <= dLUT_rd53a;
       BCRword <= BCRword_rd53a;
       ECRWord <= ECRWord_rd53a;
       GPWord  <= GPWord_rd53a; 
       CalWord <= CalWord_rd53a; 
    end generate gen_rd53a_cmds;
    
	gen_rd53b_cmds: if (FRONT_END = RD53B) generate
       dLUT    <= dLUT_rd53b;
       BCRword <= BCRword_rd53b;
       ECRWord <= ECRWord_rd53b;
       GPWord  <= GPWord_rd53b; 
       CalWord <= CalWord_rd53b; 
    end generate gen_rd53b_cmds;
    
    -- debug_jk
	command_out <= Fast_cmd;
	--
	GPDone  <= GPDoneTemp and nGPPending;
	calDone <= calDoneTemp and ncalPending;
    

    -- process commands and triggers
    -- done, gpDone and calDone get cleared on the 10 MHz clk edge their commands appear
    -- done, gpDone and calDone get set in the 10 MHz period that their command's last word is sent to the serializer
    -- precidence is trigger BCR, ECR, GP, cal, eommand processor cmds
    -- trigger, BCR and ECR cna supersede a started GP, Cal and command proccessor command
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Fast Commands Interface --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- Fast Commands are global, common for all modules
	processCmd : process (cmdClock10, cmdBlockReset)
	begin
		if cmdBlockReset = '1' then
	    	Fast_cmd    <= sync_pattern;--syncWord;
			done        <= '1';
			gpDoneTemp  <= '1';
			calDoneTemp <= '1';
			nCalPending <= '1';
			nGPPending  <= '1';
			toggle_rst  <= not toggle_rst;
			
--			--*******************************-
--        elsif resync_i = '1' then
--			trig_cntr  <= 0;
--			tag_buffer <= (others=>(others=>'0'));
--			--*******************************-
			
		elsif rising_edge(cmdClock10) then
		    -- identify when a reset has been issued to start communication initialization
		    toggle_rst_prev <= toggle_rst;
		    if (toggle_rst /= toggle_rst_prev and toggle_rst = '0') then
		       rst_issued <= '1';
		    else
		       rst_issued <= '0';
		    end if;
            -- Configure Handshakes with fast cmd block	
            if (GPFrame /= "0000000000") then
                nGPPending <= '0';
            end if;
            if (calFrame /= x"00000") then
                nCalPending <= '0';
            end if;
            
--            cmd_controller_busy_o <= '1';
            
        -- Priority Encoder for Fast commands
            if (init_in_progress = '1') then
                Fast_cmd <= sync_pattern;
            -- Trigger Command    
            elsif (trigger /= 0) then
                Fast_cmd <= tLUT(trigger) & dLUT(tag);
                BCRSave  <= BCR or BCRSave;
                ECRSave  <= ECR or ECRSave;
                
--			--*******************************-
--                tag_buffer(trig_cntr) <= std_logic_vector(to_unsigned(tag,5));
--                trig_cntr <= trig_cntr +1;
--			--*******************************-
			
            -- ECR Command
            elsif (ECR = '1' or ECRSave = '1') then
                BCRSave <= BCR or BCRSave;
                ECRSave <= '0';
                Fast_cmd <= ECRWord;
            -- BCR Command    
            elsif (BCR = '1' or BCRSave = '1') then
                BCRSave  <= '0';
                Fast_cmd <= BCRWord;
            -- Global Pulse Command
            elsif ((GPFrame /= "0000000000") and (calDoneTemp = '1')) then -- fast GP command
                if (GPDoneTemp = '0') then
                    GPDoneTemp <= '1';
                    nGPPending <= '1';
                    Fast_cmd   <= dLUT(conv_integer(GPFrame(9 downto 5))) & dLUT(conv_integer(GPFrame(4 downto 0)));
                else
                    GPDoneTemp <= '0';
                    Fast_cmd   <= GPWord;
                end if;
            -- Calibration Injection Command    
            elsif (calFrame /= x"00000") and (gpDoneTemp = '1') then -- fast cal command
                if (calDoneTemp = '0') then
                    if (calCount = 0) then
                        calCount    <= 1;
                        calDoneTemp <= '0';
                        Fast_cmd    <= dLUT(conv_integer(calFrame(19 downto 15))) & dLUT(conv_integer(calFrame(14 downto 10)));
                    else
                        calCount    <= 2;
                        calDoneTemp <= '1';
                        nCalPending <= '1';
                        Fast_cmd    <= dLUT(conv_integer(calFrame(9 downto 5))) & dLUT(conv_integer(calFrame(4 downto 0)));
                    end if;
                else
                    calCount    <= 0; -- alwasy increase by 2, up to 22 5 bit words (2 per frame)
                    calDoneTemp <= '0';
                    Fast_cmd    <= calWord;
                end if;
            else
                Fast_cmd <= x"0000"; 
--                cmd_controller_busy_o <= '0';
            end if;
        end if;
    end process;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Find edge of 40MHz clock alligned with rising edge of 10MHz clock --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- Toggle flip-flop with 10MHz clock
    process(cmdBlockReset,cmdClock10)
    begin
        if (cmdBlockReset = '1') then
            clk10_toggle <= '0';
        elsif falling_edge(cmdClock10) then
            clk10_toggle <= not clk10_toggle;
        end if;
    end process;
    -- Sample flip-flop's output with 40MHz clock to find 10MHz clock rising edge
    process(cmdBlockReset, cmdClock40)
    begin
        if (cmdBlockReset = '1') then
            prev10 <= '0';
            edge10 <= '0';
        elsif rising_edge(cmdClock40) then
            prev10 <= clk10_toggle;
            if (prev10 /= clk10_toggle) then
                edge10 <= '1';
            else
                edge10 <= '0';
            end if;
            -- one more clk cycle delay to allign with rising edge
            edge10_o <= edge10;
            -- count clk40 edges starting on the rising edge of 10MHz clock
            if (edge10_o = '1') then
                halfByteCount <= 0;
            elsif (halfByteCount = 3) then
                halfByteCount <= 0;
            else
                halfByteCount <= halfByteCount +1;
            end if;
        end if;
    end process;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Communication Initialization procedure --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    process(CmdClock10)
    begin
        if rising_edge(CmdClock10) then
            case Initialization_state is
                when Init_Idle =>
                    init_counter     <= 0;
                    init_in_progress <= '0';
                    clk_div_cntr     <= 0;
                    clk_divided      <= '1';
                    sync_pattern     <= x"FFFF";
                    if (rst_issued = '1') or (resync_i = '1') then
                        Initialization_state <= Initialization_sequence;
                    end if;
                
                when Initialization_sequence =>
                    init_in_progress <= '1';
                    init_counter     <= init_counter + 1;
                    
                    -- First send a low frequency signal to reset the chip
                    if (init_counter < reset_time) then
                        if (clk_div_cntr >= frequency_divider) then
                            sync_pattern <= not sync_pattern;
                            clk_div_cntr <= 0;
                        else
                            clk_div_cntr <= clk_div_cntr +1;
                        end if;
                    
                    -- Then a clock pattern (x"AAAA") to help the PLL lock    
                    elsif (init_counter < PLL_LOCK_TIME) then
                        sync_pattern <= x"5555";
                    -- And finally Sync Words (x"817E") to align the frame
                    elsif (init_counter < FRAME_ALIGN_TIME) then
                        sync_pattern <= x"817e";
                    else
                        Initialization_state <= Init_Idle;
                    end if;
                
                when others =>
                    Initialization_state <= Init_Idle;
            end case;
        end if;
    end process;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        -- Set frequency of pll words and sync words --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    process(cmdClock10,cmdBlockReset)
    begin
        if (cmdBlockReset = '1') then
            set_sync_cmds_pre_pre <= '0';
            set_sync_cmds_pre     <= '0';
            set_sync_cmds         <= '0';
            nb_of_pll_words       <= 31;
            nb_of_sync_words      <= 1;
            reset_time            <= 100;--1600;
            
        elsif rising_edge(cmdClock10) then
            set_sync_cmds_pre_pre <= ctrl_global_i.set_sync_cmds;
            set_sync_cmds_pre     <= set_sync_cmds_pre_pre;
            set_sync_cmds         <= set_sync_cmds_pre;
            if (set_sync_cmds = '1') then
                nb_of_pll_words   <= to_integer(unsigned(ctrl_global_i.nb_of_pll_words));
                nb_of_sync_words  <= to_integer(unsigned(ctrl_global_i.nb_of_sync_words));
                reset_time        <= to_integer(unsigned(ctrl_global_i.reset_time))*10;--*160; -- reset duration during initialization
                frequency_divider <= to_integer(unsigned(ctrl_global_i.idle_signal_frequency));
            end if;
        end if;
    end process;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- CMD Priority Encoder and serializer --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- Prioretizes Fast & Slow Commands
    CMD_Hybrid: for h in 0 to NUM_HYBRIDS-1 generate
        out_p(h)      <= cmd_serial_out_p(h)(0);
        out_n(h)      <= cmd_serial_out_n(h)(0);
        data_valid(h) <= cmd_fifo_data_valid_i and cmd_module_address_i(h);
        CMD_ser: entity work.cmd_serializer
            Port map( 
                clk_10MHZ_i         => cmdClock10,
                clk_40MHz_i         => cmdClock40,
                clk_160MHz_i        => cmdClock160,
                reset_i             => cmdBlockReset,
                init_in_progress_i  => init_in_progress,
                halfByteCount       => halfByteCount,
                data_valid_i        => data_valid(h),
                Fast_cmd_i          => Fast_cmd,
                slow_cmd_i          => cmd_fifo_dout_i,
                --
                enable_sync_word_i  => ctrl_global_i.enable_sync_word,
                nb_of_pll_words_i   => nb_of_pll_words,
                nb_of_sync_words_i  => nb_of_sync_words,
                
                cmd_controller_busy_o => cmd_controller_busy(h),
                cmd_nibble_o        => cmd_to_optical_o(h),
                cmd_serial_out_p    => cmd_serial_out_p(h),
                cmd_serial_out_n    => cmd_serial_out_n(h)
                );
    end generate;
    cmd_controller_busy_o <= or_reduce(cmd_controller_busy);
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
--==================================================================================================
end behavioral;
