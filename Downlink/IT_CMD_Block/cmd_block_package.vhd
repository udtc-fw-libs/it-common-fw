-- CMD_Block
-- written by Russell Taylor
-- Feed data to CMD_Block Core to an I2C bus


library ieee;
use ieee.std_logic_1164.all;


package cmd_block_package is

type array22x5 is array (0 to 21) of integer range 0 to 31;
type array16xi is array (0 to 15) of integer;
type array16x8bit is array (0 to 15) of std_logic_vector(7 downto 0);
type array32x8bit is array (0 to 31) of std_logic_vector(7 downto 0);
type array16x12bit is array (0 to 15) of std_logic_vector (11 downto 0);
type array5x8bit is array (0 to 4) of std_logic_vector(7 downto 0);
type array24x8bit is array (0 to 23) of std_logic_vector(7 downto 0);

end cmd_block_package;

package body cmd_block_package is
end cmd_block_package;
