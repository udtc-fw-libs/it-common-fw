--==================================================================================================
-- Company     : NCSR Demokritos / CERN
-- Engineer    : Yiannis Kazas
-- Module Name : CMD_serializer - RTL
--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- Description : 
--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- Last change : 26-Nov-2020
--==================================================================================================

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;
use work.user_package.all;

--==================================================================================================
entity CMD_serializer is
    Port ( 
        -- Clocks and Reset --
        clk_10MHZ_i         : in std_logic;
        clk_40MHz_i         : in std_logic;
        clk_160MHz_i        : in std_logic;
        reset_i             : in std_logic;
        init_in_progress_i  : in std_logic;
        -- Frame alignment --
        halfByteCount       : in integer range 0 to 3;
        -- Input signals
        data_valid_i        : in std_logic;
        Fast_cmd_i          : in std_logic_vector(15 downto 0);
        slow_cmd_i          : in std_logic_vector(15 downto 0);
        -- busy signal
        cmd_controller_busy_o : out std_logic;
        --
--        ctrl_global_i       : in ctrl_global_type;
        enable_sync_word_i  : in std_logic;
        nb_of_pll_words_i   : in natural range 0 to 1023;
        nb_of_sync_words_i  : in natural range 0 to 1023;
        
        -- command output --
        cmd_nibble_o        : out std_logic_vector (3 downto 0); -- data to lpgbt (optical implementation)
        cmd_serial_out_p    : out std_logic_vector(0 downto 0);  -- serialized data (electrical implementation)
        cmd_serial_out_n    : out std_logic_vector(0 downto 0)   -- serialized data (electrical implementation)
        );
end CMD_serializer;
--==================================================================================================


architecture RTL of CMD_serializer is

--==================================================================================================
-- Start of Component Declaration
--==================================================================================================
    -- Serializer
	component cmdSerDes
    generic(
        SYS_W : integer := 1;
        DEV_W : integer := 4
    );
    port(
        data_out_from_device : in    std_logic_vector (DEV_W - 1 downto 0);
        data_out_to_pins_p   : out    std_logic_vector(SYS_W - 1 downto 0);
        data_out_to_pins_n   : out    std_logic_vector(SYS_W - 1 downto 0);
        clk_in               : in     std_logic;
        clk_div_in           : in     std_logic;
        io_reset             : in     std_logic
    );
    end component;
--==================================================================================================
-- End of Component Declaration
--==================================================================================================


--==================================================================================================
-- Start of Signal Declaration
--==================================================================================================
	constant SYNC_WORD     : std_logic_vector (15 downto 0) := x"817e";
    constant PLL_LOCK_WORD : std_logic_vector(15 downto 0) := x"AAAA";--x"5555";

	
	signal nextWord        : std_logic_vector(15 downto 0);
	signal word            : std_logic_vector (15 downto 0);
    signal nibbleOut       : std_logic_vector (3 downto 0);
    signal nibbleOut_new   : std_logic_vector (3 downto 0);
    signal sync_counter    : integer range 0 to 2000 := 0;
    signal word_count      : integer range 0 to 10 := 0;
    
--==================================================================================================
-- End of Signal Declaration
--==================================================================================================

    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- Debug Core --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    COMPONENT SERIALIZER_DEBUG
    PORT (
    	clk : IN STD_LOGIC;
    	probe0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0); 
    	probe1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0); 
    	probe2 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    	probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
    );
    END COMPONENT  ;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
begin

    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
--    DEBUG_SERIALIZER : SERIALIZER_DEBUG
--    PORT MAP (
--    	clk => clk_160MHz_i,
--    	probe0 => nextWord, 
--    	probe1 => Fast_cmd_i, 
--    	probe2 => Slow_cmd_i,
--    	probe3(0) => data_valid_i
--    );
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- END Debug Core --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--==================================================================================================
-- Start of Main Body
--==================================================================================================

    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- Priority Encoder --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    process(reset_i, clk_10MHz_i)
    begin
        if (reset_i = '1') then
            nextWord     <= PLL_LOCK_WORD;
            sync_counter <= 0;
            word_count   <= 0;
--            cmd_controller_busy_o <= '0';
        elsif rising_edge(clk_10MHz_i) then
            sync_counter <= sync_counter +1;
            -- Fast Commands have the highest priority
            if (Fast_cmd_i /= x"0000") or (init_in_progress_i = '1') then
                nextWord <= Fast_cmd_i;
            -- Then the slow commands
            elsif (data_valid_i = '1') then
                nextWord <= Slow_cmd_i;
            -- Then Idles 
            elsif (sync_counter >= nb_of_pll_words_i) and (enable_sync_word_i = '1') then
                --Send at least 2 sync word every ~32 frames            
                nextWord   <= SYNC_WORD;
                word_count <= word_count +1;
                if (word_count >= nb_of_sync_words_i) then
                    sync_counter <= 0;
                    word_count   <= 0;
                end if;
            else
                -- If there is nothing else to be sent, send "PLL Lock" words
                nextWord <= PLL_LOCK_WORD;
--                cmd_controller_busy_o <= '0';
            end if;
        end if;
    end process;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- 16-bit frame to nibbles --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	--make sure word is not updated until fully used
    with halfByteCount select word <=
            nextWord when 0,
            word when 1,
            word when 2,
            word when 3;

	-- start of frame (as sent to serdes) first 40 mhz clock after 10 MHz rising edge
	with halfByteCount select nibbleOut <=
			word(3 downto 0) when 3,
			word(7 downto 4) when 2,
			word(11 downto 8) when 1,
			word(15 downto 12) when 0;
			
    --<-- CHANGE Alex -->--
    nibbleOut_new(0) <= nibbleOut(3);
    nibbleOut_new(1) <= nibbleOut(2);
    nibbleOut_new(2) <= nibbleOut(1);
    nibbleOut_new(3) <= nibbleOut(0);
    
    
    process(clk_40MHz_i)    
    begin
        if rising_edge(clk_40MHz_i) then
            cmd_nibble_o <= nibbleOut;
        end if;
    end process;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- Serializer (Electrical Implementation) --
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Electrical_serializer: if (Link = ELECTRICAL) generate
    -- send half bytes to the serial deserializer
        SerData : cmdSerDes
        port map(
            data_out_from_device     => nibbleOut_new, --<-- CHANGE Alex
            data_out_to_pins_p        => cmd_serial_out_p,
            data_out_to_pins_n        => cmd_serial_out_n,
            clk_in                    => clk_160MHz_i,
            clk_div_in                => clk_40MHz_i,
            io_reset                => reset_i
        );
    end generate Electrical_serializer;
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    --~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


end RTL;
