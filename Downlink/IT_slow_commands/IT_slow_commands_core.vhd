----------------------------------------------------------------------------------
-- Company     : NCSR Demokritos / CERN
-- Engineer    : Yiannis Kazas
-- Module Name : IT_slow_commands - Behavioral
----------------------------------------------------------------------------------
-- Description : 
----------------------------------------------------------------------------------
-- Last change : 6-May-2021
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.user_package.all;
use IEEE.std_logic_misc.all;
library UNISIM;
use UNISIM.VComponents.all;

------------------------------------------------------------------------
entity IT_slow_commands_core is
    Port( 
        clk_10MHz_i           : in std_logic;
        ipb_clk_i             : in std_logic;
        reset_i               : in std_logic;
        cmd_controller_busy_i : in std_logic;--_vector(NUM_HYBRIDS-1 downto 0);
        ctrl_slow_cmd_i       : in ctrl_slow_cmd_type;
        stat_slow_cmd_o       : out stat_slow_cmd_type;
        cmd_fifo_data_valid_o : out std_logic;
        cmd_fifo_dout_o       : out std_logic_vector(15 downto 0);
        module_address_o      : out std_logic_vector(NUM_HYBRIDS-1 downto 0)        
        ); 
end IT_slow_commands_core;
------------------------------------------------------------------------


architecture RTL of IT_slow_commands_core is

--======================================================================
-- Start of Component Declaration
--======================================================================

-- fifo
COMPONENT fifo_32to16x4096
  PORT (
    rst : IN STD_LOGIC;
    wr_clk : IN STD_LOGIC;
    rd_clk : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    prog_empty_thresh : IN STD_LOGIC_VECTOR(16 DOWNTO 0);
    dout : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    almost_empty : OUT STD_LOGIC;
    prog_empty : OUT STD_LOGIC;
    wr_rst_busy : OUT STD_LOGIC;
    rd_rst_busy : OUT STD_LOGIC
  );
END COMPONENT;

--======================================================================
-- End of Component Declaration
--======================================================================


--======================================================================
-- Start of Signal Declaration
--======================================================================
type cmd_fifo_state_type is (Idle, Read_Cmd_Header, Send_Commands);
signal Cmd_Fifo_state : cmd_fifo_state_type;
signal module_address    : std_logic_vector(NUM_HYBRIDS-1 downto 0):=(others=>'0');
signal cmd_error_flag    : std_logic;
signal header_word_count : integer range 0 to 2:=0;
signal word_counter      : integer range 0 to 200_000:=0;
signal cmd_packet_done   : std_logic:='0';

signal cmd_fifo_rst         : std_logic:='0';
signal cmd_fifo_wr_rst_busy : std_logic;
signal cmd_fifo_rd_rst_busy : std_logic;
signal cmd_fifo_prog_empty  : std_logic;
signal cmd_fifo_empty       : std_logic;
signal cmd_fifo_almost_empty : std_logic;
signal cmd_fifo_full        : std_logic;
signal cmd_fifo_rd_en       : std_logic:='0';
--signal cmd_fifo_wr_en       : std_logic:='0';
signal cmd_fifo_dout        : std_logic_vector(15 downto 0);
signal cmd_fifo_prog_empty_thr : std_logic_vector(16 downto 0);


signal reset_int : std_logic:='0';

signal cmd_fifo_dispatch_arm : std_logic:='1';
signal cmd_fifo_dispatch     : std_logic:='0';
signal cmd_fifo_dispatch_pre_pre_sync : std_logic:='0';
signal cmd_fifo_dispatch_pre_sync     : std_logic:='0';
--signal cmd_fifo_dispatch_pre_syn     : std_logic:='0';
signal cmd_fifo_dispatch_flag : std_logic := '0';
signal clear_dispatch_flag    : std_logic := '0';

signal cmd_fifo_wr_clk : std_logic;
signal rd_en : std_logic;

--======================================================================
-- End of Signal Declaration
--======================================================================

----------------------------------------
-- Debug Core --
----------------------------------------
COMPONENT Slow_cmds_Debug
PORT (
	clk : IN STD_LOGIC;
	probe0 : IN STD_LOGIC_VECTOR(1 DOWNTO 0); 
	probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
	probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
	probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
	probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
	probe5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
	probe6 : IN STD_LOGIC_VECTOR(1 DOWNTO 0); 
	probe7 : IN STD_LOGIC_VECTOR(15 DOWNTO 0); 
	probe8 : IN STD_LOGIC_VECTOR(9 DOWNTO 0); 
	probe9 : IN STD_LOGIC_VECTOR(10 DOWNTO 0); 
	probe10 : IN STD_LOGIC_VECTOR(31 DOWNTO 0); 
	probe11 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
	probe12 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
);
END COMPONENT  ;

signal fifo_state : std_logic_vector(1 downto 0);

begin

--Debug_Slow_cmds : Slow_cmds_Debug
--PORT MAP (
--	clk        => ipb_clk_i,
--	probe0     => fifo_state, 
--	probe1(0)  => cmd_fifo_empty, 
--	probe2(0)  => cmd_fifo_rd_en, 
--	probe3(0)  => cmd_fifo_dispatch, 
--	probe4(0)  => cmd_error_flag, 
--	probe5(0)  => cmd_controller_busy_i, 
--	probe6     => std_logic_vector(to_unsigned(header_word_count, 2)), 
--	probe7     => cmd_fifo_dout, 
--	probe8     => "000000000" & cmd_packet_done,--(others=>'0'),--"000000" & module_address, 
--	probe9     => std_logic_vector(to_unsigned(word_counter, 11)), 
--	probe10    => ctrl_slow_cmd_i.cmd_fifo_din, 
--	probe11(0) => ctrl_slow_cmd_i.cmd_fifo_wen,
--	probe12(0) => cmd_fifo_dispatch_flag
--);
--------------------------------------
--------------------------------------


--======================================================================
-- Start of Main Body
--======================================================================
module_address_o <= module_address;
cmd_fifo_dout_o  <= cmd_fifo_dout;

-- status signals to ipbus
stat_slow_cmd_o.cmd_fifo_error_flag  <= cmd_error_flag;
stat_slow_cmd_o.cmd_fifo_empty       <= cmd_fifo_empty;
stat_slow_cmd_o.cmd_fifo_prog_empty  <= cmd_fifo_prog_empty;
stat_slow_cmd_o.cmd_fifo_full        <= cmd_fifo_full;
stat_slow_cmd_o.cmd_fifo_packet_done <= cmd_packet_done;

------------------------------------------------------------------------
-- Reset for fifo 
------------------------------------------------------------------------
process(ipb_clk_i)
    variable cnt : integer := 0;    
begin
    if rising_edge(ipb_clk_i) then
       if (reset_i = '1') or (ctrl_slow_cmd_i.cmd_fifo_rst = '1')then
           cnt := 0;
           reset_int <= '0';
       else
           if (cnt<20) then
               reset_int <= '1';
               cnt := cnt+1;
           else
               reset_int <= '0';
           end if;
       end if;
    end if;
end process;
------------------------------------------------------------------------

------------------------------------------------------------------------
-- Cmd fifo ready to dispatch signal 
------------------------------------------------------------------------
process(reset_int,clk_10MHz_i)
begin
    if (reset_int = '1') then
        cmd_fifo_dispatch_arm  <= '1';
        cmd_fifo_dispatch      <= '0';
        cmd_fifo_dispatch_flag <= '0';
    elsif rising_edge(clk_10MHz_i) then
        if (cmd_fifo_dispatch_flag = '1') then--(ctrl_slow_cmd_i.cmd_fifo_dispatch = '1') then
            cmd_fifo_dispatch_arm <= '0';
        else
            cmd_fifo_dispatch_arm <= '1';
        end if;
        
        if (ctrl_slow_cmd_i.cmd_fifo_dispatch = '1') and (cmd_fifo_dispatch_arm <= '1') then
            cmd_fifo_dispatch_pre_pre_sync <= '1';
        else
            cmd_fifo_dispatch_pre_pre_sync <= '0';
        end if;
        
        cmd_fifo_dispatch_pre_sync <= cmd_fifo_dispatch_pre_pre_sync;
        cmd_fifo_dispatch          <= cmd_fifo_dispatch_pre_sync;

        if (cmd_fifo_dispatch = '1') then
            cmd_fifo_dispatch_flag <= '1';
        elsif (clear_dispatch_flag = '1') then
            cmd_fifo_dispatch_flag <= '0';
        end if;
        
    end if;
end process;


------------------------------------------------------------------------


------------------------------------------------------------------------
-- Dispatch Commands to CMD Controller
------------------------------------------------------------------------
process(reset_int, clk_10MHz_i)
begin
    if (reset_int = '1') then
        Cmd_Fifo_State        <= Idle;
        cmd_error_flag        <= '0';
        cmd_fifo_data_valid_o <= '0';
        cmd_packet_done     <= '0';
    elsif rising_edge(clk_10MHz_i) then
        
        case Cmd_Fifo_State is
            when Idle =>
            fifo_state <= "00";
                clear_dispatch_flag   <= '0';
                header_word_count     <= 0;
                cmd_fifo_rd_en        <= '0';
                cmd_fifo_data_valid_o <= '0';
                module_address        <= (others=>'0');
                if (cmd_error_flag = '0') and (cmd_fifo_dispatch_flag = '1') and (cmd_fifo_empty = '0') then
                    Cmd_Fifo_State    <= Read_Cmd_Header;
                    cmd_fifo_rd_en    <= '1';
                    cmd_packet_done   <= '0';
                    clear_dispatch_flag   <= '1';
                    end if;
            
            -- Read the header of the packet    
            when Read_Cmd_Header =>
            fifo_state <= "01";
            
                -- First 16-bit word of the packet, must begin with header identifier x"F"
                if (header_word_count = 0) then
                    -- Read the module adresses for this transaction
                    if (cmd_fifo_dout(15 downto 12) = "1111") then
                        module_address    <= cmd_fifo_dout(NUM_HYBRIDS-1 downto 0);
                        header_word_count <= 1;
                        Cmd_Fifo_State    <= Read_Cmd_Header;
                    -- header identifier not found, raise error flag and return to idle    
                    else 
                        cmd_error_flag <= '1';
                        CMD_Fifo_State <= Idle;
                    end if;
                -- Second 16-bit word which specifies the number of words this packet contains    
                elsif (cmd_fifo_empty = '0') then
                    word_counter          <= ((to_integer(unsigned(cmd_fifo_dout)))*2)-1;
                    Cmd_Fifo_State        <= Send_Commands;
                    cmd_fifo_data_valid_o <= '1';
                    header_word_count     <= 0;
                -- Less data than expected - fifo is empty, raise error flag and go back to idle    
                else
                    cmd_error_flag <= '1';
                    Cmd_Fifo_State <= Idle; 
                end if;
                
            when Send_Commands =>
            fifo_state <= "10";
            
                if (word_counter > 0) then
                    cmd_fifo_data_valid_o <= '1';
                    if (cmd_fifo_empty = '0') then
                        cmd_fifo_data_valid_o <= '1';
                        if (cmd_controller_busy_i = '0') then
                            word_counter <= word_counter -1;
                        end if;
                    else
                        cmd_fifo_data_valid_o <= '0';
                        cmd_error_flag        <= '1';
                    end if;
                else
                    cmd_packet_done       <= '1';
                    cmd_fifo_data_valid_o <= '0';
                    if (cmd_fifo_almost_empty = '0') then
                        Cmd_fifo_state <= Read_Cmd_Header;
                    else
                        Cmd_Fifo_State <= Idle;
                    end if;
                end if;
                
            when others =>
                cmd_error_flag <= '1';
                Cmd_Fifo_state <= Idle;
        end case;
    end if;
end process;
------------------------------------------------------------------------
------------------------------------------------------------------------
--======================================================================
-- End of Main Body
--======================================================================

--======================================================================
-- Start of Component Instantiation
--======================================================================
cmd_fifo_wr_clk <= not ipb_clk_i; --only for simulation of the user core
rd_en           <= not cmd_controller_busy_i and cmd_fifo_rd_en;
cmd_fifo_prog_empty_thr  <= "00000" & ctrl_slow_cmd_i.cmd_fifo_prog_empty_thr;
SLow_cmd_fifo : fifo_32to16x4096
  PORT MAP (
    rst    => reset_int,
    wr_clk => ipb_clk_i,--cmd_fifo_wr_clk,
    rd_clk => clk_10MHz_i,
    din    => ctrl_slow_cmd_i.cmd_fifo_din,
    wr_en  => ctrl_slow_cmd_i.cmd_fifo_wen,
    rd_en  => rd_en,--cmd_fifo_rd_en,
    prog_empty_thresh => cmd_fifo_prog_empty_thr,
    dout         => cmd_fifo_dout,
    full         => cmd_fifo_full,
    empty        => cmd_fifo_empty,
    almost_empty => cmd_fifo_almost_empty,
    prog_empty   => cmd_fifo_prog_empty,
    wr_rst_busy  => cmd_fifo_wr_rst_busy,
    rd_rst_busy  => cmd_fifo_rd_rst_busy
  );
--======================================================================
-- End of Component Instantiation
--======================================================================


end RTL;
